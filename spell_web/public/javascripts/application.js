// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

// In case console.log and console.debug are not defined:
if (typeof console == "undefined") {
    console = { log: function(msg){}, 
		debug: function(msg) {} }
}

if (typeof(princeton) == "undefined") { princeton = {}; }

// If state is undefined, then flip checkboxes in the class specified by selector, otherwise:
// if state is boolean, check or uncheck these checkboxes accordingly, otherwise
// if state is a String, then check only the checkboxes in the specified CSS selector.
// Example: toggle(nil, ".previous")
// The default for selector is ".toggleable"
princeton.toggle = function(source, state, selector) {
    if (typeof(selector) == "undefined" || !selector) selector= ".toggleable";
    switch (typeof state) {
    case "undefined":
	jQuery(selector).each( function() { this.checked = ! this.checked; });
	break;
    case "string":
	princeton.toggle(source, false, selector);
	jQuery(selector + state).each( function() { this.checked = true });
	break;
    default:
	jQuery(selector).each( function() { this.checked = state } );
    }
    return source; // allow chaining
}

// Has the selection changed?
princeton.changedp = function(selector) {
    if (typeof(selector) == "undefined" || !selector) selector= ".toggleable";
    var changed = false;
    jQuery(selector).each( function() { if ( this.hasClassName("previous") != this.checked ) { console.log(this); changed = true; return false; } } );
    return changed;
}

// Has anything been checked?
princeton.checkedp = function(selector) {
    if (typeof(selector) == "undefined" || !selector) selector= ".toggleable";
    var checked = false;
    jQuery(selector).each( function() { if (this.checked) { checked = true; return false; } } );
    return checked;
}

// Check jQuery(x)[0].value
princeton.validate = function(x, msg) {
  var txt = jQuery(x);
  if (txt && txt[0] && txt[0].value && txt[0].value.trim().length > 0) { 
    return true; 
  }
  if (msg) alert(msg);
  return false;
}
