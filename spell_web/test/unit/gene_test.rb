require File.dirname(__FILE__) + '/../test_helper'

class GeneTest < Test::Unit::TestCase
  fixtures :genes

  # Replace this with your real tests.
  def test_truth
    assert_kind_of Gene, genes(:first)
  end
end
