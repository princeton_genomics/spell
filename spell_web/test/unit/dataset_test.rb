require File.dirname(__FILE__) + '/../test_helper'

class DatasetTest < Test::Unit::TestCase
  fixtures :datasets

  # Replace this with your real tests.
  def test_truth
    assert_kind_of Dataset, datasets(:first)
  end
end
