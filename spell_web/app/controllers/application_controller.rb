# Filters added to this controller will be run for all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
class ApplicationController < ActionController::Base

  def authorize
    unless session[:user_id]
      flash[:notice] = "Please log in"
      redirect_to(:controller => "login", :action => "login")
    end
  end

  # type := :message :warning :error
  # Use flash[:array_of_notices] to ensure duplicate messages do not appear
  def append_flash(type, text)
    if flash[:notice]
      sofar = flash[:array_of_notices] ||= []  # set to [] in case it is nil
      return if sofar.include? text
      flash[:array_of_notices] << text
      case type
      when :warning
        text = "<br>- WARNING: " + text
      when :error
        text = "<br><font color=#F00>- ERROR: " + text + "</font>"
      else
        text = "<br>" + text
      end
      flash[:notice] += text
    else
      flash[:array_of_notices] = [text]
      case type
      when :warning
        text = "- WARNING: " + text
      when :error
        text = "<font color=#F00>- ERROR: " + text + "</font>"
      end
      flash[:notice] = text
    end
  end

  def append_message(text)
    append_flash :message, text
  end

  def append_warning(text)
    append_flash :warning, text
  end

  def append_error(text)
    append_flash :error, text
  end

  # A wrapper function for SPELL_OPTIONS[]
  # Always allow nil, and generally allow String, but do not define defaults here.
  def spell_options option
    begin
      so = SPELL_OPTIONS[option] 
    rescue
      return nil
    end

    return nil if so == nil
    
    # Special cases:
    case option
    when :number_of_columns_for_tags
      ok = (1..100)
      so = so.to_i
      if ok.include?(so)
        return so
      else
        logger.error "#{SPELL_OPTIONS}[:number_of_columns_for_tags] is being ignored as it should be an integer in #{ok}"
        return nil
      end
    
    when :relevantTags
      case so
      when Array, Boolean
        return so
      else
        logger.error "#{SPELL_OPTIONS}[:relevantTags] is being ignored as it should be nil, Boolean, or an Array of String"
        return nil
      end
    end

    # General case
    case so
    when String, Number
      so.to_s  # String#to_s is a no-op
    else
      nil
    end
  end
  
  def selectedTagsAreKnownToBeExhaustive
    # assert { Array === spell_options(:relevantTags) }
    # logger.debug "#{__method__}: SPELL_OPTIONS[:relevantTagsAreKnownToBeExhaustive] = #{SPELL_OPTIONS[:relevantTagsAreKnownToBeExhaustive]}"
    # logger.debug "#{__method__}: sts.length=#{session[:tags_searched].length}"
    # logger.debug "#{__method__}: spell_options(:relevantTags).length =  #{spell_options(:relevantTags).length}"
    stsl = session[:tags_searched].length
    sorl = spell_options(:relevantTags).length
    return (SPELL_OPTIONS[:relevantTagsAreKnownToBeExhaustive] and ( stsl == sorl ) )
  end


  filter_parameter_logging :password

  helper_method :spell_options, :append_warning, :append_error,  :selectedTagsAreKnownToBeExhaustive

end
