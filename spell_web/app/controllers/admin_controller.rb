class AdminController < ApplicationController
  before_filter :authorize, :except => :add_user

  def index
  end

  def list_datasets
    if params[:order] == nil
      params[:order] = 'author'
    end
    @datasets = Dataset.paginate :page => params[:page], :order => params[:order], :per_page => 10
  end

  def list_genes
    if params[:order] == nil
      params[:order] = 'name'
    end
    @genes = Gene.paginate :page => params[:page], :order => params[:order], :per_page => 100
  end

  def list_exprs
  end

  def new_dataset
    @dataset = Dataset.new
  end

  def add_flexible_link
    if params[:flexible_link].nil?
       flash[:notice]="Internal Error: flexible_link was nil"
      redirect_to :action => 'new_flexible_link'
      return
    end

    if params[:flexible_link][:filename].nil? || 
       params[:flexible_link][:filename].length == 0
      flash[:notice]="You must specify a file name"
      redirect_to :action => 'new_flexible_link'
      return
    end

    if params[:flexible_link][:label].nil? ||
       params[:flexible_link][:label].length == 0
      flash[:notice]="You must specify text to display in the link (the label)"
      redirect_to :action => 'new_flexible_link'
      return
    end

    if params[:flexible_link][:url].nil? ||
       params[:flexible_link][:url].length == 0
      flash[:notice]="You must specify a URL to link to"
      redirect_to :action => 'new_flexible_link'
      return
    end

    filename = params[:flexible_link][:filename]
    label    = params[:flexible_link][:label]
    url      = params[:flexible_link][:url]

    @flexible_link = Flexible_dataset_link.new(:filename => filename, :label => label, :url => url)
    if @flexible_link.save
      flash[:notice] = "Flexible link (#{filename}, #{label}, #{url}) was successfully added"
      redirect_to :action => 'list_flexible_links'
    else
      flash[:notice] = "Unknown error adding flexible link (#{filename}, #{label}, #{url}) to the database."
      render :action => 'new_flexible_link'
    end
  end

  def add_list_flexible_links
    @filename = params[:filename]
    error = update_flexible_links_from_list(@filename)
    if (error != nil)
      append_error(error)
    end
    redirect_to :action => 'list_flexible_links'
  end

  def list_flexible_links
    if params[:order] == nil
      params[:order] = 'filename'
    end
    @flexible_links = Flexible_dataset_link.paginate :page => params[:page], :order => params[:order], :per_page => 20
  end

  def edit_flexible_link
   @flexible_link = Flexible_dataset_link.find(params[:id])
  end

  def update_flexible_link
    @flexible_link = Flexible_dataset_link.find(params[:id])
    if @flexible_link.update_attributes(params[:flexible_link])
      flash[:notice] = "Flexible link (#{@flexible_link[:filename]}, #{@flexible_link[:label]}, #{@flexible_link[:url]}) was successfully updated."
      redirect_to :action => 'list_flexible_links'
    else
      render :action => 'edit_flexible_link'
    end
  end

  def update_flexible_links_from_list(filename)
    errors = nil
    lines = ""
    if filename.class == lines.class
      lines = filename.split(/[\n\r]/)
    else
      lines = filename.read.split(/[\n\r]/)
    end
    for line in lines
      line.strip!
      parts = line.chomp.split("\t")
      # parts[0] is the filename
      # parts[1] is the label
      # parts[2] is the url

      flexible_link = Flexible_dataset_link.new
      partnum = 0
      for column in Flexible_dataset_link.content_columns
        flexible_link[column.name] = parts[partnum]
        partnum += 1
      end

      if !flexible_link.save
        logger.warn("****Unable to save: " + line)
        append_warning("Unable to save: " + line)
      end
    end
    errors
  end

  def destroy_flexible_link
    Flexible_dataset_link.find(params[:id]).destroy
    redirect_to :action => 'list_flexible_links'
  end

  def add_user
    # add_user is not subject to the authorize filter. We have
    # to do it here, if we have users at this point.
    @users = User.find(:all);
    if (@users.length != 0)
       authorize
    else
       flash[:notice] = "Please add an initial user"
    end

    if request.get?
      @user = User.new
    else
      @user = User.new(params[:user])
      if @user.save
        flash[:notice] = "User #{@user.name} created"
        redirect_to(:controller => "admin", :action => "index")
      end
    end
  end

  def list_users
    if params[:order] == nil
      params[:order] = 'name'
    end
    @users = User.paginate :page => params[:page], :order => params[:order], :per_page => 20
  end

  def edit_user
    @user = User.find(params[:id])
  end

  def update_user
    @user = User.find(params[:id])
    params[:user][:name].upcase!
    if @user.update_attributes(params[:user])
      flash[:notice] = "User #{params[:user][:name]} was successfully updated."
      redirect_to :action => 'list_users'
    else
# QUESTION: More retries with no message. Does a message come out some other way?
      render :action => 'edit_user'
    end
  end

  def remove_user
    User.find(params[:id]).destroy
    redirect_to :action => 'list_users'
  end


  def add_dataset
    @dataset = Dataset.new(params[:dataset])
    if @dataset.save
      flash[:notice] = 'Dataset successfully added'
      redirect_to :action => 'list_datasets'
    else
      render :action => 'new_dataset'
    end
  end

  def new_gene
    @gene = Gene.new
  end

  def add_gene
    params[:gene][:name].upcase!
    @gene = Gene.new(params[:gene])
    if @gene.save
      flash[:notice] = 'Gene successfully added'
      redirect_to :action => 'list_genes'
    else
      flash[:notice] = "Error adding gene #{params[:gene][:name]} to the database:<br />" +
        @gene.errors
      render :action => 'new_gene'
    end
  end

  def add_common_gene
    if params[:common_gene].nil?
       flash[:notice]="Internal Error: common_gene was nil"
      redirect_to :action => 'new_common_gene'
      return
    end

    if params[:common_gene][:common_name].nil? || 
        params[:common_gene][:common_name].length == 0
      flash[:notice]="You must specify a common gene name"
      redirect_to :action => 'new_common_gene'
      return
    end

    if params[:common_gene][:systematic_name].nil? ||
        params[:common_gene][:systematic_name].length == 0
      flash[:notice]="You must specify a systematic name"
      redirect_to :action => 'new_common_gene'
      return
    end

    params[:common_gene][:common_name].upcase!
    params[:common_gene][:systematic_name].upcase!
    common = params[:common_gene][:common_name]
    systematic = params[:common_gene][:systematic_name]

    @common_gene = Common_gene.new(:common_name => common, :systematic_name => systematic)
    if @common_gene.save
      flash[:notice] = 'Common gene successfully added'
      redirect_to :action => 'list_common_genes'
    else
      flash[:notice] = "Error adding common gene name #{common} (#{systematic}) to the database:<br />" +
        @common_gene.errors
      render :action => 'new_common_gene'
    end
  end

  def add_gene_alias
    if params[:gene_alias].nil?
       flash[:notice]="Internal Error: gene_alias was nil"
      redirect_to :action => 'new_gene_alias'
      return
    end

    if params[:gene_alias][:alias_name].nil? || 
        params[:gene_alias][:alias_name].length == 0
      flash[:notice]="You must specify a gene alias"
      redirect_to :action => 'new_gene_alias'
      return
    end

    if params[:gene_alias][:systematic_name].nil? ||
        params[:gene_alias][:systematic_name].length == 0
      flash[:notice]="You must specify a systematic name"
      redirect_to :action => 'new_gene_alias'
      return
    end

    params[:gene_alias][:alias_name].upcase!
    params[:gene_alias][:systematic_name].upcase!

    alias_name = params[:gene_alias][:alias_name]
    systematic = params[:gene_alias][:systematic_name]

    @gene_alias = Gene_alias.new(:alias_name => alias_name, :systematic_name => systematic)
    if @gene_alias.save
      flash[:notice] = "Gene alias #{alias_name} (#{systematic}) was successfully added"
      redirect_to :action => 'list_gene_aliases'
    else
      flash[:notice] = "Unknown error adding gene alias name #{alias_name} (#{systematic}) to the database."
      render :action => 'new_gene_alias'
    end
  end

  def show_dataset
    @dataset = Dataset.find(params[:id])
  end

  def edit_dataset
    @dataset = Dataset.find(params[:id])
  end

  def update_dataset
    @dataset = Dataset.find(params[:id])
    if @dataset.update_attributes(params[:dataset])
      flash[:notice] = "Dataset was successfully updated."
      redirect_to :action => 'show_dataset', :id => @dataset
    else
      render :action => 'edit_dataset'
    end
  end

  def edit_gene
    @gene = Gene.find(params[:id])
  end

  def edit_common_gene
    @common_gene = Common_gene.find(params[:id])
  end

  def edit_gene_alias
    @gene_alias = Gene_alias.find(params[:id])
  end

  def update_gene
    @gene = Gene.find(params[:id])
    params[:gene][:name] = params[:gene][:name].upcase
    if @gene.update_attributes(params[:gene])
      flash[:notice] = "Gene was successfully updated."
      redirect_to :action => 'list_genes'
    else
# QUESTION: More retries with no message. Does a message come out some other way?
      render :action => 'edit_gene'
    end
  end

  def update_common_gene
    @common_gene = Common_gene.find(params[:id])
    params[:common_gene][:common_name].upcase!
    params[:common_gene][:systematic_name].upcase!
    if @common_gene.update_attributes(params[:common_gene])
      flash[:notice] = "Common gene was successfully updated."
      redirect_to :action => 'list_common_genes'
    else
      render :action => 'edit_common_gene'
    end
  end

  def update_gene_alias
    @gene_alias = Gene_alias.find(params[:id])
    params[:gene_alias][:alias_name].upcase!
    params[:gene_alias][:systematic_name].upcase!
    if @gene_alias.update_attributes(params[:gene_alias])
      flash[:notice] = "Gene alias was successfully updated."
      redirect_to :action => 'list_gene_aliases'
    else
      render :action => 'edit_gene_alias'
    end
  end

  def destroy_dataset
    Dataset.find(params[:id]).destroy
    redirect_to :action => 'list_datasets'
  end

  def destroy_all_datasets
    @datasets = Dataset.find(:all)
    for dataset in @datasets
      dataset.destroy
    end
    redirect_to :action => 'list_datasets'
  end

  def destroy_gene
    Gene.find(params[:id]).destroy
    redirect_to :action => 'list_genes'
  end

  def destroy_all_genes
    @genes = Gene.find(:all)
    for gene in @genes
      gene.destroy
    end
    redirect_to :action => 'list_genes'
  end

  def destroy_common_gene
    Common_gene.find(params[:id]).destroy
    redirect_to :action => 'list_common_genes'
  end

  def destroy_all_common_genes
    @common_genes = Common_gene.find(:all)
    for common_gene in @common_genes
      common_gene.destroy
    end
    redirect_to :action => 'list_common_genes'
  end

  def destroy_gene_alias
    Gene_alias.find(params[:id]).destroy
    redirect_to :action => 'list_gene_aliases'
  end

  def destroy_all_gene_aliases
    @gene_aliases = Gene_alias.find(:all)
    for gene_alias in @gene_aliases
      gene_alias.destroy
    end
    redirect_to :action => 'list_gene_aliases'
  end

  def destroy_all_exprs
    @exprs = Expr.find(:all)
    for expr in @exprs
      expr.destroy
    end
    redirect_to :action => 'list_exprs'
  end

  def new_list_datasets
  end

  def add_list_datasets
    @filename = params[:filename]
    error = update_datasets_from_list(@filename)
    if (error != nil)
      flash[:notice] = error
    end
    redirect_to :action => 'list_datasets'
  end

  def new_list_genes
  end

  def add_list_genes
# QUESTION: Why instance variable for filename?  Is it used elsewhere? Don't think so.
#  Could this be just as easily
#     update_genes_from_list(params[:filename])
# ?
    @filename = params[:filename]
    error = update_genes_from_list(@filename)
    if (error != nil)
      append_error(error)
    end
    redirect_to :action => 'list_genes'
  end

  def new_list_common_genes
  end

  def add_list_common_genes
    @filename = params[:filename]
    error = update_common_genes_from_list(@filename)
    if (error != nil)
      append_error(error)
    end
    redirect_to :action => 'list_common_genes'
  end

  def new_list_gene_aliases
  end

  def add_list_gene_aliases
    @filename = params[:filename]
    error = update_gene_aliases_from_list(@filename)
    if (error != nil)
      append_error(error)
    end
    redirect_to :action => 'list_gene_aliases'
  end

  def new_list_exprs
  end

  def add_list_exprs
    @filename = params[:filename]
    update_exprs_from_list(@filename)
    if (error != nil)
      append_error(error)
    end
    redirect_to :action => 'list_exprs'
  end

  def make_info_files
    make_gene_info_file
    make_dataset_info_file
    make_config_file
    flash[:notice] = 'Info files recreated'
    redirect_to :action => 'index'
  end

  def list_gene_aliases
    if params[:order] == nil
      params[:order] = 'alias_name'
    end
    @gene_aliases = Gene_alias.paginate :page => params[:page], :order => params[:order], :per_page => 100
  end

  def list_common_genes
    if params[:order] == nil
      params[:order] = 'common_name'
    end
    @common_genes = Common_gene.paginate :page => params[:page], :order => params[:order], :per_page => 100
  end

  def make_gene_info_file
    @genes = Gene.find(:all, :order => 'name')
    fout = File.open(File.expand_path("config/gene_list.txt"), "w")
    for gene in @genes
      fout.puts(gene.id.to_s + "\t" + gene.name)
    end
    fout.flush
    fout.close
  end

  def make_dataset_info_file
    @datasets = Dataset.find(:all, :order => 'author')
    fout = File.open("config/dataset_list.txt", "w")
    for dataset in @datasets
      fout.puts(dataset.id.to_s + "\t"  + dataset.filename)
    end
    fout.flush
    fout.close
  end

  def make_config_file
    fout = File.open("config/config.txt","w")
    fout.print "CEL\t" + Dir.pwd + "/config/gene_list.txt\t"
    fout.print Dir.pwd + "/config/dataset_list.txt\t"
    fout.print PATH_TO_DATASETS
    fout.puts
    fout.flush
    fout.close
  end

  private
  def update_datasets_from_list(filename)
    errors = nil
    lines = ""
    if filename.class == lines.class
      lines = filename.split(/[\n\r]/)
    else
      lines = filename.read.split(/[\n\r]/)
    end
    for line in lines
      parts = line.chomp.split("\t")
      dataset = Dataset.new
      partnum = 0
      for column in Dataset.content_columns
        dataset[column.name] = parts[partnum]
        partnum = partnum + 1
      end
      if !dataset.save
        logger.warn("Unable to save: " + line)
        append_warning("Unable to save: " + line)
      end
    end
    errors
  end

  def update_genes_from_list(filename)
    errors = nil
    lines = ""
    if filename.class == lines.class
      lines = filename.split(/[\n\r]/)
    else
      lines = filename.read.split(/[\n\r]/)
    end
    for line in lines
      line.strip!
      parts = line.chomp.split("\t")
      gene = Gene.new
      partnum = 0
      for column in Gene.content_columns
        gene[column.name] = parts[partnum].upcase
        partnum += 1
      end
      if !gene.save
        logger.warn("****Unable to save: " + line)
        append_warning("Unable to save: " + line)
      end
    end
    errors
  end

  def update_common_genes_from_list(filename)
#QUESTION: How is errors set, or is it?
    errors = nil
    lines = ""
    if filename.class == lines.class
      lines = filename.split(/[\n\r]/)
    else
      lines = filename.read.split(/[\n\r]/)
    end
    for line in lines
      line.strip!
      parts = line.chomp.split("\t")
      # parts[0] is the Systematic name
      # parts[1] is the common name

      common_gene = Common_gene.new
      partnum = 0
      for column in Common_gene.content_columns
        common_gene[column.name] = parts[partnum].upcase
        partnum += 1
      end
      if !common_gene.save
        logger.warn("****Unable to save: " + line)
        append_warning("Unable to save: " + line)
      end
    end
    errors
  end

  def update_gene_aliases_from_list(filename)
    errors = nil
    lines = ""
    if filename.class == lines.class
      lines = filename.split(/[\n\r]/)
    else
      lines = filename.read.split(/[\n\r]/)
    end
    for line in lines
      line.strip!
      parts = line.chomp.split("\t")
      # parts[0] is the alias
      # parts[0] is the systematic name

      gene_alias = Gene_alias.new
      partnum = 0
      for column in Gene_alias.content_columns
        gene_alias[column.name] = parts[partnum].upcase
        partnum += 1
      end

      if !gene_alias.save
        logger.warn("****Unable to save: " + line)
        append_warning("Unable to save: " + line)
      end
    end
    errors
  end

  def update_exprs_from_list(filename)
    IO.foreach(filename.path) do |line|
      line.strip!
      parts = line.split("\t")
      expr = Expr.new
      partnum = 0
      for column in Expr.content_columns
        expr[column.name] = parts[partnum]
        partnum += 1
      end
      if !expr.save
        logger.warn("****Unable to save: " + line)
        append_warning("Unable to save: " + line)
      end
    end
  end
end
