require 'set'
require 'translate_names.rb'

# NOTE: Set@intersect? is defined below.

class SearchController < ApplicationController

  def index
    initialize_relevant_tags
  end

  def about
  end

  # render tag_details.rhtml or else  default_tag_details.rhtml
  def tag_details
    begin
      render
    rescue
      render "default_tag_details"  # does not change the URL
      # redirect_to :action => "default_tag_details" # changes the URL
    end
  end

  def gene_listing
    initialize_relevant_tags
    logger.debug "#{__method__}: params[:tags]=#{params[:tags].inspect}"
    persist_tags
  end

  # TODO: this is adhoc, and maybe we want to remember the previous selection
  def reset_session_tags
    ensure_session __method__
    session[:params][:tags] = nil
    session[:tags_searched] = nil
    session[:datasetIds]    = nil

    session[:tags_found] = false
  end

  def help
    render(:layout => 'layouts/popup')
  end

  # If params[:page] is set on entry then do not clear session[:datasetIds]
  #
  def dataset_listing

    initialize_relevant_tags
    logger.debug "#{__method__}: params[:tags]=#{params[:tags].inspect}"
    persist_tags
      
    params[:order]    ||= 'author'
    params[:per_page] ||= 30

    # If we have arrived here via a page number (params[:page]) then do nothing [TODO]
    filter_all_datasets unless (session[:tags_found] and session[:datasetIds] and params[:page])

    if session[:datasetIds] and session[:tags_found]
      setOfIds = session[:datasetIds].to_set
      @datasets = Dataset.find(:all, :order => params[:order])
      datasets = @datasets.select { |dset| setOfIds.include?(dset.id) }
      @datasets = datasets if datasets.length > 0 
      @datasets = @datasets.paginate :page => params[:page], :order => params[:order], :per_page => params[:per_page]
    else
      @datasets = Dataset.paginate :page => params[:page], :order => params[:order], :per_page => params[:per_page]
    end
  end

  def dataset_details
    begin
      @dataset = Dataset.find(params[:id])
    rescue
      append_error "A valid dataset id must be specified when requesting dataset details"
      redirect_to :action => index
      return
    end
    @flexible_dataset_links = Flexible_dataset_link.find(:all, :conditions => ["filename = '__ANY__' OR filename = ?", @dataset.filename])
    if request.xhr?
      render(:layout => 'layouts/popup')
    else
      render
    end
  end

  def submit_search
    logger.debug "#{__method__}: params[:query] = #{params[:query].inspect}"

    # R2.0.3: If :num_genes has not been specified then copy the value from session:
    num_genes = params[:query][:num_genes] || session[:num_genes]

    new_params = Hash.new
    new_params[:search_string] = params[:query][:search_genes]
    new_params[:num_genes]     = num_genes

    if params[:tags] and params[:tags].length > 0
      new_params[:tags]  = params[:tags].join(',')
      logger.debug  "#{__method__} = #{new_params[:tags]}"
    end

    update_session(new_params)
    redirect_to(session[:params]) # ==> show_results || index if there were errors
  end

  # Formerly "refine_search"
  def update_search
    pass_params = Hash.new
    for key in params.keys
      case key
      when "action", "controller", "commit", "query", "form", "tags"
        # nop
      else
        if pass_params[:search_string] == nil
          # Use concat to force a temp string; avoids an exception for an
          # in-place operation on a frozen string.
          pass_params[:search_string] = "" + key
        else
          pass_params[:search_string] += " " + key
        end
      end
    end
    logger.info "New Search String: " + pass_params[:search_string]
    if params[:query] && params[:query][:num_genes]
      pass_params[:num_genes] = params[:query][:num_genes]
    end

    # Inherit tags:
    reconcile_session_params_tags  # set params[:tags] if possible
    pass_params[:tags] = params[:tags]

    update_session(pass_params)
    redirect_to(session[:params]) # ==> show_results || index if there were errors
  end

  def log_search
    # Don't log unless configured to allow it.
    return if NIH_LOG_PATH.nil? || NIH_LOG_PATH.length==0

    if !File.directory?(NIH_LOG_PATH)
      append_error("NIH_LOG_PATH (#{NIH_LOG_PATH}) is not a directory.")
      return
    end

    if !File.writable?(NIH_LOG_PATH)
      append_error("NIH_LOG_PATH (#{NIH_LOG_PATH}) is not writable.")
      return
    end

    t = Time.now
    fname = "#{NIH_LOG_PATH}SPELL_NIH_Log_#{t.strftime '%Y_%m'}.log"
    if File.exist?(fname) && !File.writable?(fname)
      append_error("NIH log file \"#{fname}\" is not writable.")
      return
    end

    nowstr = t.strftime "%Y-%m-%d\t%H:%M"

    outstr = nowstr + "\t"
    one_out = false
    session[:query_genes].each do |g|
      if one_out
        outstr += ", "
      end
      outstr += g
      one_out = true
    end

    File.open fname, "a" do |f|
      f.puts outstr
    end
  end

  def log_failed_search_gene(gene)
    return if GENE_LOOKUP_FAIL_LOG.nil? || GENE_LOOKUP_FAIL_LOG.length==0

    if !File.exist?(GENE_LOOKUP_FAIL_LOG)
      dir = File.dirname(GENE_LOOKUP_FAIL_LOG)
      if !File.directory?(dir)
        append_error("Gene lookup failure file (#{GENE_LOOKUP_FAIL_LOG}) does not exist and its directory (#{dir}) is not a directory.")
              return
      end
      if !File.writable?(dir)
        append_error("Gene lookup failure file (#{GENE_LOOKUP_FAIL_LOG}) does not exist and its directory (#{dir}) is not writable.")
        return
      end
    else
      if !File.writable?(GENE_LOOKUP_FAIL_LOG)
        append_error("GENE_LOOKUP_FAIL_LOG (#{GENE_LOOKUP_FAIL_LOG}) is not writable.")
        return
      end
    end
    nowstr = Time.now.strftime "%Y-%m-%d\t%H:%M"
    File.open GENE_LOOKUP_FAIL_LOG, "a" do |f|
      f.puts "#{nowstr}\t#{gene}"
    end
  end

  def show_results
    # NOTES: In SPELL 2.0.0 this method consisted of these lines:
    # # Populate the session from the params.
    # session[:embedded] = nil
    # update_session(params)
    # perform_search

    # In SPELL 2.0.2.0, "update_sessions(params)" was removed to address a problem
    # of redundant error messages, but that prevents URL-pasting,
    # so we now perform the call conditionally:

    session[:embedded] = nil  # retained from SPELL 2.0.2

    initialize_relevant_tags
    reconcile_session_params_tags

    # peak 2011.09.27: paste URL problem
    if params[:search_string] or not session[:query_ids]
      # Populate the session from the params:
      update_session(params)
    end

    # peak: handle invalid pasted "show_results"
    if session[:search_string]
      perform_search
    else
      # FYI: session[:params] is now {action => index}
      # FYI  params           is now {controller=>search, action=>show_results}
      redirect_to(session[:params])
    end
  end

  def show_embedded_results
    session[:embedded] = 1
    @num_datasets =  params[:num_sets].nil? ? 5 : params[:num_sets]
    logger.info @num_datasets
    # Populate the session from the params.
    update_session(params)
    perform_search
    render :layout => false
  end

  # This method ensures, amongst other things, that session[:params] is a Hash.
  def update_session(pass_params)

    # reset the search parameters in the session
    session[:params] = Hash.new
    session[:params][:action] = 'show_results'

    case pass_params[:search_string]
    when nil, ""
      if session[:embedded]
        append_error("Embedded search error: You must supply at least one gene to search.")
      else
        session[:params][:action] = 'index'
        append_error("Your query seemed to be empty. Please enter at least one gene name or alias.")
      end
    else
      reinit_session pass_params

      #Given the basic information from the parameters, fill in the query genes information
      session[:query_genes] = Array.new
      session[:query_ids] = Array.new
      map_all_genes(session[:search_string])
      session[:dset_page] = 1
      if session[:query_genes].length == 0
        session[:params][:action] = 'index'
      end
    end # case
  end


  def reinit_session(pass_params)
      session[:search_string] = pass_params[:search_string]
      if !pass_params[:num_genes].nil?
        session[:num_genes] = pass_params[:num_genes]
      else
        if !pass_params[:embedded].nil? && pass_params[:embedded]
          session[:embedded] = 1
          session[:num_genes] = 0
        else
          session[:embedded] = nil
          session[:num_genes] = 20
        end
      end
      ensure_session __method__
      session[:params][:search_string] = pass_params[:search_string]
      session[:params][:tags] = pass_params[:tags] if pass_params[:tags]
      session[:params][:num_genes] = session[:num_genes]

      session[:tags_searched] = nil
      session[:tags_found] = false
      session[:datasetIds] = nil

      if session[:single_display] == nil
              session[:single_display] = 0
      end
      if session[:dual_display] == nil
              session[:dual_display] = 1
      end
      if session[:single_color] == nil
              session[:single_color] = 0
      end
      if session[:dual_color] == nil
              session[:dual_color] = 0
      end
  end

  def perform_search
    if session[:query_ids].length < 1
      append_error("No valid genes in query - please try again.")

      # SPELL 2.0.2.0 used the following two lines, which leave a dangling query string:
      # session[:params][:action] = 'index'
      # redirect_to(session[:params]) # Get another query
      # so:
      redirect_to :action => index
      return

    else
      # Log the search, to support future grant requests:
      log_search

      send_search_query(nil)

      if (session[:set_order].nil? || @gene_order.nil?)
        append_message("Query returned no results.")
	session[:params][:action] = 'index'
	redirect_to(session[:params]) # We're done here...
      else
        # Stash the gene_order for the views.
        session[:gene_order] = @gene_order
        # Check for significant datasets
        setParts = session[:set_order].split(",")

        # logger.info "Number of datasets returned: #{setParts.length}"
        # logger.info "session[:set_order]: #{session[:set_order]}"

        issue_warning_if_too_few_datasets setParts
        filter_search
      end
    end
  end

  def send_search_query(options)
    # Do the searching here
    require 'socket'
    sock = TCPSocket.new('localhost', SPELL_SOCKET_NUMBER)
    # Message format is:
    # <dsetIDs, comma delim>\t<query geneIDs, comma delim>[\tMAX_GENES=n]\n
    sendingMessage = get_all_dataset_ids
    sendingMessage += "\t"
    for gID in session[:query_ids]
      sendingMessage = sendingMessage + gID.to_s + ","
    end
    sendingMessage.chop!

    if !options.nil?
      sendingMessage = sendingMessage + options
    end

    sendingMessage += "\n"
    sock.send(sendingMessage, 0)

    # We may have to receive multiple fragments from the net.
    # A well formed message will have two newlines.  Loop until
    # we have seen both, accumulating the fragments.
    msg = ""
    num_new_lines = 0
    error_message = ""
    while num_new_lines < 2
      part_msg = sock.recv(50000)
      num_new_lines += part_msg.count "\n"
      msg += part_msg 
    end
    lines = msg.split("\n")
    # logger.debug "#{__method__}: session[:set_order]=#{lines[0]}"
    # logger.debug "#{__method__}: gene_order=#{lines[1]}"
    session[:set_order] = lines[0]
    @gene_order = lines[1]
  end

  def ds_results_as_text
    # Perform the search for textual output.  This reiterates code that
    # for web display is spread across several files.
    if session[:query_ids].length < 1
      append_error("No valid genes in query, Unable to perform search.")
      session[:params][:action] = 'index'
    elsif session[:query_ids].length == 1
      append_message "You must choose more than one gene to get a textual output of database relevance."
      # peak 2011.09.27: let the user try again (was: redirect_to :action => index)
      redirect_to :back
    else
      # Send the query, and have the results saved in the session.
      send_search_query("\tMAX_GENES=0")

      # Check for significant datasets
      setParts = session[:set_order].split(",")
      issue_warning_if_too_few_datasets setParts

      # R2.0.3: !!!
      original_number=setParts.length
      logger.debug "#{__method__}: BEFORE: # = #{setParts.length}"
      filter_search
      logger.debug "#{__method__}: AFTER: # = #{setParts.length}"
      logger.debug "#{__method__}: session[:set_order] = #{session[:set_order]}"
      # logger.debug "#{__method__}: session[:datasetIds] = #{session[:datasetIds]}"
      setParts = session[:set_order].split(",")

      require 'translate_names.rb'
      require 'stringio'

      os = StringIO.open("", "w")
      # Output query genes
      query_gene_string = "Query genes: "
      query_gene_out = false
      query_genes = {}
      session[:query_ids].each do |qg|
        name = gene_id_to_name(qg)
        if name.start_with?("Unfound")
          append_error("Internal error. Could not find returned query gene id in the database")
          redirect_to :action => index
        end
        query_genes.merge!({name => 1})
      end
      query_genes.keys.sort!.each do |name|
        if query_gene_out
          query_gene_string += ", "
        else
          query_gene_out = true
        end
        query_gene_string += name
      end

      os.puts "SPELL Version #{SPELL_VERSION} for #{COMMON_NAME} at #{Socket.gethostname} with #{original_number} datasets on #{Time.now.localtime.strftime("%Y-%m-%d")}"
      os.puts  ""  # A blank line to separate sections
      os.puts query_gene_string

      if session[:tags_found] 
        # os.puts "The SPELL search algorithm found #{original_number} datasets."
        os.puts  ""  # A blank line to separate sections
        os.puts "The following listing only includes those datasets that have been tagged with at least one of the following tags:"
        # assert { session[:tags_searched].sort_by {|x| x.downcase } = session[:tags_searched] }
        os.puts session[:tags_searched].join(", ") 
      end

      rank = 0
      os.puts  ""  # A blank line to separate sections
      # Output the dataset weights for those with a non-zero weight, _if_ the weights are
      # meaningful.
      if session[:query_ids].length > 1
        titleOut = false;
        # setParts contains an array of the returned datasets, in descending order of weight.
        setParts.each do |rds|
          # rds is <dataset_id>:<score>
          dsParts = rds.split(":")
          # If our score is "--" or or 0.0% or 0.0, we're done.
          break if (dsParts[1] == "--") or (dsParts[1].to_f == 0.0)
          if !titleOut
            os.puts "Rank\tWeight\tPubMedID\tArticle"
            titleOut = true
          end

          dataset = Dataset.find(dsParts[0])
          if dataset == nil
            os.puts dsParts[1] + "\t" + "Internal error: could not find dataset id #{dsParts[0]} in the database."
          else
            rank += 1
            os.puts rank.to_s + "\t" +
                        dsParts[1] + "\t" +
                      dataset.pubmedID.to_s + "\t" +
                    dataset.author + ", " +
                    dataset.title + ", " +
                    dataset.journal + ", " +
                    dataset.pub_year.to_s
          end
        end
      end
      render :text  => os.string, :content_type => "text/plain"
    end
  end

  def gene_results_as_text
    # Perform the search for textual output.  This reiterates code that
    # for web display is spread across several files.
    if session[:query_ids].length < 1
      append_error("No valid genes in query, Unable to perform search.")
      session[:params][:action] = 'index'
    else
      # Send the query, and have the results saved in the session.
      send_search_query("\tMAX_GENES=0")

      # Check for significant datasets
      setParts = session[:set_order].split(",")
      issue_warning_if_too_few_datasets setParts

      require 'translate_names.rb'
      require 'stringio'

      os = StringIO.open("", "w")
      # Output query genes
      query_gene_string = "Query genes: "
      query_gene_out = false
      query_genes = {}
      session[:query_ids].each do |qg|
        name = gene_id_to_name(qg)
        if name.slice(0,"Unfound".length) == "Unfound"
          append_error("Internal error. Could not find returned query gene id in the database")
          redirect_to :action => index
        end
        query_genes.merge!({name => 1})
      end
      query_genes.keys.sort!.each do |name|
        if query_gene_out
          query_gene_string += ", "
        else
          query_gene_out = true
        end
        query_gene_string += name
      end

      os.puts "SPELL Version #{SPELL_VERSION} for #{COMMON_NAME} at #{Socket.gethostname} on #{Time.now.localtime.strftime("%Y-%m-%d")}"
      os.puts ""  # A blank line to separate sections.
      os.puts query_gene_string
      os.puts ""  # A blank line to separate sections.

      # Output the returned scored genes whose scores are > 0.0
      os.puts "Rank\tGene\tCommon\tScore"

      geneParts = @gene_order.split(",")
      # geneParts contains an array of the returned genes, in descending order of score.
      rank = 0
      geneParts.each do |rg|
        # rg is <gene_id>:<score>
         scoreParts = rg.split(":")
         # If our score is 0.0, we're done.
         break if scoreParts[1] == "0.0"

         sys_name = gene_id_to_systematic(scoreParts[0])
         name = systematic_to_common(sys_name)
         was_query = (query_genes[name]  == 1)

         if was_query
           rank_string = "Q"
           score_string = ""
         else
           rank += 1
           rank_string = rank.to_s
           score_string = scoreParts[1]
         end
         os.puts rank_string + "\t" + sys_name + "\t" + name + "\t" + score_string
      end
      render :text  => os.string, :content_type => "text/plain"
   end
  end

  def expr_results
    if params[:num_genes] != nil
      session[:num_genes] = params[:num_genes]
    end
    if params[:dset_page] != nil
      session[:dset_page] = params[:dset_page]
    end
    if params[:single_display] != nil
      session[:single_display] = params[:single_display]
    end
    if params[:single_color] != nil
      session[:single_color] = params[:single_color]
    end
    if params[:dual_display] != nil
      session[:dual_display] = params[:dual_display]
    end
    if params[:dual_color] != nil
      session[:dual_color] = params[:dual_color]
    end
    render(:layout => false)
  end

  def go_results
    if params[:num_genes] != nil
      session[:num_genes] = params[:num_genes]
    end
    render(:layout => false)
  end

  def new_listing
    pass_params = Hash.new
    pass_params[:search_string] = params[:query][:search_genes]

    # Update the session based on the parameters
    session[:params] = Hash.new
    session[:params][:action] = 'show_listing'

    if pass_params[:search_string] == nil || pass_params[:search_string] == ""
      session[:params][:action] = 'gene_listing'
      append_error("Your search seemed to be empty. Please repeat your search below:")
    else
      session[:search_string] = pass_params[:search_string]
      session[:params][:search_string] = pass_params[:search_string]

      if session[:single_display] == nil
              session[:single_display] = 0
      end
      if session[:dual_display] == nil
              session[:dual_display] = 1
      end
      if session[:single_color] == nil
              session[:single_color] = 0
      end
      if session[:dual_color] == nil
              session[:dual_color] = 0
      end

      #Given the basic information from the parameters, fill in the query genes information
      session[:query_genes] = Array.new
      session[:query_ids] = Array.new
      map_all_genes(session[:search_string])
      session[:dset_page] = 1

      #peak 20111022 filter by tags
      # Set session[:params][:tags] based on params[:tags] whether it is a string or an array:
      reconcile_session_params_tags

    end

    redirect_to(session[:params]) # ==> show_listing || index if there were errors
  end

  def expr_listing
    if params[:dset_page] != nil
      session[:dset_page] = params[:dset_page]
    end
    if params[:single_display] != nil
      session[:single_display] = params[:single_display]
    end
    if params[:single_color] != nil
      session[:single_color] = params[:single_color]
    end
    if params[:dual_display] != nil
      session[:dual_display] = params[:dual_display]
    end
    if params[:dual_color] != nil
      session[:dual_color] = params[:dual_color]
    end
    render(:layout => false)
  end

  def show_listing
    initialize_relevant_tags   #R2.0.3

    # peak 2011.09.27:
    if params[:search_string] or not session[:query_ids]
      # Populate the session from the params:
      update_session(params)
    end
    # peak 2011.09.27: handle invalid pasted "show_listing"
    if not session[:query_ids]
      redirect_to(session[:params])
    end

    # If filtering by tags, then populate the relevant session[] variables:
    filter_all_datasets

    if session[:datasetIds].nil?
      session[:datasetIds] = Dataset.find(:all, :select => 'id').map! { |dset| dset.id }
    end

    logger.debug "#{__method__}: #datasets before checking expression levels: #{session[:datasetIds].length}"
    datasetIds = relevant_datasets( session[:datasetIds], session[:query_ids])
    logger.debug "#{__method__}: #datasets after checking expression levels : #{datasetIds.length}"

    if datasetIds.empty?
      append_warning ( session[:tags_found] ?
                       "No datasets with the specified tags have matching data." :
                       "No datasets have matching data." )
      redirect_to request.referer
    else
      session[:datasetIds] = datasetIds 
    end
  end

  def show_single_expression
    render(:layout => 'layouts/popup')
  end

  #########
  protected
  #########

  def ensure_session info
    session[:params]    ||= Hash.new
  end

  def persist_tags
    if params[:tags]
      ensure_session __method__
      session[:params][:tags] = params[:tags]
    elsif session[:tags_found] and session[:datasetIds] and params[:page]
      logger.debug "retaining datasetIds: tags_found and params[:page] = #{params[:page]}"
    # allow persistence
    elsif session[:tags_searched]
      logger.debug __method__, "retaining session information"
    else
      reset_session_tags 
    end
  end

  # peak 2011.09.30 based on prototype by lparsons.
  # In general, this method assumes session[:set_order] has been set on entry
  # to a string of the form: <dataset_id>:<score>,...
  # It then filters out items that do not match any of the dataset tags that are
  # currently being used for filtering.
  # On entry, params[:tags] should be nil, or a properly constructed array, or a string of tags
  # On return:
  #  session[:tags_searched] is set to nil or an array of tags (sorted for display);
  #  session[:tags_found] is set to true or false, and if true:
  #     session[:set_order] is reset to be a comma-delimited list
  #     # Maybe it would be useful to set session[:datasetIds] to be an array of dataset ids
  def filter_search
    # Set params[:tags] if possible:
    reconcile_session_params_tags
    tags = params[:tags]
    session[:tags_found] = false
    session[:tags_searched] = nil
    return if tags.nil? or tags == ""
    logger.debug "#{__method__}: tags = #{tags}"

    if String === tags
      # Be careful of leading and trailing spaces:
      tags = tags.split(/ *, */).select { |tag| tag != "" }
    end
    return if tags.length == 0

    logger.debug "session[:tags_searched].class =1 #{session[:tags_searched].class}"
    setOfTags = tags.to_set  # precompute before looping
    datasets = Array.new
    # datasetIds = Array.new
    setParts = session[:set_order].split(",")
    setParts.each { |parts|
      entry = parts.split(":")
      id = entry[0].to_i
      dset = Dataset.find(id)
      # datasetIds << id
      datasets.push(parts) if keep?(dset, setOfTags)
    }
    if datasets.empty?
      session[:tags_found] = false 
    else
      session[:tags_found] = true
      session[:set_order] = datasets.join(",")
      # session[:datasetIds] = datasetIds  # array of dataset ids
    end
    logger.debug "session[:tags_searched].class =2 #{session[:tags_searched].class}"
    session[:tags_searched] = tags.sort_by {|x| x.downcase } # an array
  end

  # peak 2011.10.24 
  # Which datasets have any of the specified tags?
  # q.v. SearchHelper#filter_datasets
  # Set these session[] variables:
  #    session[:datasetIds]    -- an array of ids of matching datasets, else nil
  #    session[:tags_found]    -- boolean
  #    session[:tags_searched] -- an array of sorted tags, else nil
  #    session[:params][:tags] (conditionally) -- a copy of params[:tags]
  #
  def filter_all_datasets
    # Set params[:tags] if possible:
    reconcile_session_params_tags
    tags = params[:tags]

    if String === tags
      # Be careful of leading and trailing spaces:
      tags = tags.split(/ *, */).select { |tag| tag != "" }
    end
    
    if tags.nil? or tags.empty?
      reset_session_tags
      # logger.debug "#{__method__}: session[:datasetIds]  = #{session[:datasetIds].inspect}"
      return
    end

    setOfTags = tags.to_set  # precompute before looping

    datasetIds = Dataset.find(:all, :select => 'id, tags').
      select { |dset| keep?(dset, setOfTags) }.
      map! {|dset| dset.id }

    if datasetIds.empty?
      session[:tags_found] = false
      session[:datasetIds] = nil
    else
      session[:tags_found] = true
      session[:datasetIds] = datasetIds  # array of dataset ids
    end
    session[:tags_searched] = tags.sort_by {|x| x.downcase }
  end


  # Minimize the work required to determine if this dataset should be kept,
  # on the assumption that tags is a Set on entry.
  def keep?( dset, tags )
    return false unless dset
    tags.intersect?(dset.tags.split("|"))
  end

  def get_mapped_names(gName)
    nameStr = alias_to_systematic(gName.upcase)
    result = Set.new
    if nameStr != nil
      parts = nameStr.split("|")
      result.merge(parts)
    end
    result
  end

  def map_all_genes(query_string)
    query_string.upcase!
    queries = query_string.split(/[;,\s]+/)
    for query in queries
      q_id = get_gene_id(query)
      if q_id != -1
        session[:query_genes].push(query)
        session[:query_ids].push(q_id)
      else
          mapSet = get_mapped_names(query)
          if mapSet == nil || mapSet.size == 0
            append_warning("<b><i>" + query + "</b></i> was not recognized as a valid gene name.")
          elsif mapSet.size == 1
            map_q = mapSet.to_a[0]
            session[:query_genes].push(map_q)
            q_id = get_gene_id(map_q)
            if q_id == -1
              log_failed_search_gene(map_q)
              append_warning("<b><i>" + map_q + "</b></i> was not recognized as a valid gene name.")
              session[:query_genes].delete(map_q)
            else
              session[:query_ids].push(q_id)
            end
          else
            gList = ""
            for map_gene in mapSet
              gList += "<b><i>" + map_gene + "</b></i>,"
              map_id = get_gene_id(map_gene)
              if map_id == -1
                log_failed_search_gene(map_gene)
                append_warning("<b><i>" + map_gene + "</b></i> was not recognized as a valid gene name.")
              else
                session[:query_genes].push(map_gene)
                session[:query_ids].push(map_id)
              end
            end
            gList.chop!
            append_message("Query gene <b><i>" + query + "</b></i> maps to several gene symbols.  The following symbols were included in this search: " + gList)
        end
      end
    end
  end

  def get_gene_id(gName)
    g = Gene.find(:first, :conditions => [ "name = ?", gName])
    id = -1
    id = g.id unless g.nil?
    id
  end

  def get_all_dataset_ids
    dsets = Dataset.find(:all)
    str = ""
    for dset in dsets
      str += dset.id.to_s + ","
    end
    return str.chop
  end

  def get_expression(d, g)
    e = Expr.find(:first, :conditions => ["geneID = :gID AND dsetID = :dID", {:gID => g, :dID => d}])
    if e != nil
      return e.data_table
    else
      return ""
    end
  end

  # After: SPELL_OPTIONS[:relevantTags] is nil or an array
  #        SPELL_OPTIONS[:relevantTagsAreKnownToBeExhaustive] is boolean
  def initialize_relevant_tags

    # In case this method is called several times, the following assignment is conditional:
    SPELL_OPTIONS[:relevantTagsAreKnownToBeExhaustive] ||= false

    case SPELL_OPTIONS[:relevantTags]
    when Array
      logger.debug "#{__method__}: SPELL_OPTIONS[:relevant_tags] is an array: " + SPELL_OPTIONS[:relevantTags].join(',')      
      return
    when nil
      return
    end

    # Treat a missing tag as equivalent to the string in nyc:
    nyc = "not yet curated"

    # Canonical value for "Other":
    other = "other"

    # Since a null value is treated as a value (nyc):
    SPELL_OPTIONS[:relevantTagsAreKnownToBeExhaustive] = true
    logger.debug "#{__method__}: exhaustive"
    tags = Set.new
    rows = Dataset.find(:all, :select => "tags")

    nycLabel = nil
    otherLabel = nil

    rows.each { |row|
      c = row.tags.split('|')
      if c.any?
        tags.merge(c)
      else 
        nycLabel = nyc
      end
    }
    # Ensure that the 'other' and 'not yet curated' tags, if present, are last
    ["Not Yet Curated", "Not yet curated", nyc].each { |label|
      nycLabel = nyc if tags.delete?(label)
    }

    ["Other", other].each { |label|
      otherLabel = other if tags.delete?(label)
    }

    tags = tags.to_a.sort_by {|x| x.downcase }

    tags = tags << otherLabel if otherLabel == other
    tags = tags << nycLabel   if nycLabel == nyc

    SPELL_OPTIONS[:relevantTags] = tags
    logger.debug "#{__method__}: " + SPELL_OPTIONS[:relevantTags].join(',')
  end

  # Set params[:tags] and session[:params][:tags] so that they are consistent, giving priority to params[:tags].
  # If params[:tags] is an Array or a String, then populate session[:params][:tags] accordingly;
  # otherwise, ensure session[:params][:tags] is nil or a string, and set params[:tags] accordingly.
  # NOTE: in general, session[:params][:tags] should be nil or a string.
  def reconcile_session_params_tags

    ensure_session __method__ # TODO: needed?
    logger.debug "#{__method__}: ON ENTRY: session[:params][:tags]=#{session[:params][:tags]}"

    # If params[:form] then pay attention to params[:tags]
    if params[:form] and not params[:tags]
      logger.debug "#{__method__} :: params[:form] = #{params[:form]}"
      ptString = spt = ""
    else
      # If params[:tags] is set, try to use it:
      case params[:tags]
      when Array
        ptString = spt = params[:tags].join(',')
      when String
        ptString = spt = params[:tags]
      else
        spt = session[:params][:tags]
        ptString  = ""
      end
    end

    case session[:params][:tags]
    when String
      if spt and spt != session[:params][:tags]
        logger.debug "#{__method__}: warning: resetting session[:params][:tags] as it differs from params[:tags] = " + ptString
      end
      session[:params][:tags] = spt
    when nil
      session[:params][:tags] = spt
    when Array
      session[:params][:tags] = session[:params][:tags].join(',')
    else
      logger.debug "#{__method__}: error: ignoring session[:params][:tags] as it is a #{session[:params][:tags].class}"
    end

    # Ensure params[:tags] is set if at all possible
    if String === session[:params][:tags]
      case params[:tags]
      when nil, ""
        logger.debug "#{__method__}: note: setting params[:tags]=#{session[:params][:tags]}"
        params[:tags]=session[:params][:tags]
      end
    end
  end

  # setParts should contain the array of returned datasets, in descending order of weight.
  def issue_warning_if_too_few_datasets setParts
    n = setParts.length
    return if n == 0
    firstPart = setParts[0].split(":")
    score = firstPart[1]

    if score != "--"
      # score has a trailing "%", which to_f mercifully ignores:
      score = score.to_f
      # Per Matt Hibbs (20111112):
      # For the original SPELL compendium, the following condition would be true with fewer than 3 datasets:
      #  ((score / 100.0) < (2.0 / setParts.length.to_f))
      # In R2.0.2, the criterion for issuing a warning was: n < 200.0 / score
      # Notes on implementation in R2.0.3:
      #    In R2.0.2, if there are insufficiently many datasets, the weights are equal, 
      #    so all we have to do here is test whether the weights of the first and last datasets are equal.
      # (For robustness we also compare score with 100/n, allowing for rounding.)

      s2 = setParts[n-1].split(":")[1].to_f

      # n = 0; setParts.each { |x| s = x.split(":")[1]; break if s == "--" or s.to_f == 0.0 ; n += 1 }
      # For robustness, also compare score with 100/n after allowing for rounding: i.e. score <= (100/n * 10).ceil / 10.0
      if (n > 1 && score == s2) or score <= ( (1000.0 / n).ceil / 10.0)
        logger.debug "#{__method__}: first and last scores are #{score}, #{s2}; n=#{n}"
        append_warning "The number of datasets that are directly relevant to this query falls below the computed threshold of acceptability, and so all datasets have been weighted equally. This may lead to unreliable results."
      end
    end
  end

  # R2.0.3
  # Which datasets have some data for at least one of the genes?
  # Parameters: datasets : an array of integers, or an array of objects for which dataset.id is an integer
  #                 gIDs : array (or comma-separated string) of numeric ids
  # Return a subarray of the input array, datasets
  def relevant_datasets(datasets, gIDs)
    # logger.debug "#{__method__}: # datasets=#{datasets.length}"
    # logger.debug "#{__method__}: gIDs=#{gIDs.inspect}"
    datasets.select { |dataset|
      id = (Integer === dataset) ? dataset : dataset.id
      # scan for a non-empty expression level
      gIDs = gIDs.split(",") if String === gIDs
      gIDs.any? { |gID|
        parts = get_expression(id, gID)
        parts = parts.split(",") if String === parts
        parts.any? { |part| part != "NA" }
      }
    }
  end

  # Expose to view:
  # helper_method :selectedTagsAreKnownToBeExhaustive
  helper_method :relevant_datasets, :get_expression

end  # class SearchController

class Set
  # Fast intersection assuming Set#include? is trivial to compute
  # and that we should in general avoid computing enum.length.
  # enum should in general respond to each and include?
  def intersect? enum
    if self.length == 0
      return false
    elsif (Set === enum or Range === enum) and self.length < enum.length
      self.each { |x| return true if enum.include?(x) }
    else
      enum.each { |x| return true if self.include?(x) }
    end
    false
  end
end # class Set
