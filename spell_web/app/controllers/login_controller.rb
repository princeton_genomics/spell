class LoginController < ApplicationController
	before_filter :authorize, :except => :login
	
	layout "admin"
	
	def login
		#if no users exist, add the admin/nimda user
		@users = User.find(:all)
		if @users.length == 0
                   redirect_to(:controller => 'admin', :action => 'add_user')
		end
		if request.get?
			session[:user_id] = nil
			@user = User.new
		else
			@user = User.new(params[:user])
			logged_in_user = @user.try_to_login
			if logged_in_user
				session[:user_id] = logged_in_user.id
				redirect_to(:action => "index")
			else
				flash[:notice] = "Invalid user/password combination"
			end
		end
	end

	def index
		redirect_to(:controller => 'admin', :action => "index")
	end

  def logout
  		session[:user_id] = nil
  		flash[:notice] = "Logged out"
  		redirect_to(:action => "login")
  end

  def list_users
  end
end
