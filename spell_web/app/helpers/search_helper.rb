module SearchHelper

  # if 'link' is truthy then the help icon will itself be a hyperlink
  def help_link (type, link = nil)
    options = { :action => 'help', :type => type, :width => 300 }
    if link
      options[:link] = url_for(options = { :action => 'help', :type => type })
    end
    link_to(  image_tag("/images/1316625150_help.png"),
            # image_tag("/images/help.jpg"),
            options = options,
            html_options = { :class => 'jTip', :id => type } )
  end

  def flexible_link_format(pattern, file, cond)
    ret = pattern.gsub('#{FILE}', file)
    ret.gsub!('#{CONDITION}', cond)
    ret
  end

  def flexible_link_to_anchor(flexible_link, file, cond)
    ret = '<a href="' + flexible_link_format(flexible_link[:url], file, cond) + '">'
    ret += flexible_link_format(flexible_link[:label], file, cond) + '</a>'
    ret
  end

  def flexible_link_has_condition(flexible_link)
    !flexible_link[:label].index('#{CONDITION}').nil? ||
    !flexible_link[:url].index('#{CONDITION}').nil?
  end

  def default_num_genes ()
    if session[:num_genes] == nil
      return 20
    else
      return session[:num_genes].to_i
    end
  end

  def dset_page_choices (num_per_page)
    choice = Array.new
    if session[:set_order] == nil
      choice[0] = Array.new
      choice[0][0] = "--"
    else
      sets = session[:set_order].split(",")
      for i in 1..((sets.length-1)/num_per_page + 1) do
        choice[i-1] = Array.new
        choice[i-1][0] = "From " + ((i-1)*num_per_page + 1).to_s + " to " + 
                         [(i*num_per_page), sets.length].min.to_s
        choice[i-1][1] = i
      end
    end
    return choice
  end

  # This method filters datasets based on the ids in session[:datasetIds].
  # Specifically, if session[:tags_found] and session[:datasetIds] and datasets, then
  # this method computes the selection of datasets with obj.id in session[:datasetIds];
  # if there are any, then that selection is returned, otherwise tags are effectively ignored
  # and datasets is returned.
  # Otherwise this method returns datasets.
  # Q.v. SearchController#filter_search
  # Rationale for computing session[:datasetIds]: the GUI presents a global view of datasets.
  def filter_datasets datasets
    if datasets and session[:datasetIds] and session[:tags_found]
      selection = datasets.select {
        |dset| session[:datasetIds].include?(dset.id)
      }
      logger.debug "#{__method__}: number selection = #{selection.length}"
      if selection.any?
        datasets=selection
      else
        logger.debug "#{__method__}: ignoring the selected tags"
        session[:tags_searched] = nil
        session[:tags_found] = false
        # append_warning "No datasets match the specified tags, which have therefore been ignored."
      end
    end
    datasets
  end

  def alpha_dset_page_choices (num_per_page)
    choice = Array.new
    dsets = filter_datasets( Dataset.find(:all, :order => 'author') )
    dsets = relevant_datasets( dsets, session[:query_ids] )  # Issue 28
    if dsets == nil || dsets.length == 0
      choice[0] = Array.new
      choice[0][0] = "--"
    else
      for i in 1..((dsets.length-1)/num_per_page + 1) do
        choice[i-1] = Array.new
        choice[i-1][0] = "From " + ((i-1)*num_per_page + 1).to_s + 
                         " (" + dsets[((i-1)*num_per_page)].author + ") to "
        choice[i-1][0] += [(i*num_per_page), dsets.length].min.to_s + 
                          " (" + dsets[[i*num_per_page,dsets.length].min - 1].author + ")"
        choice[i-1][1] = i
      end
    end
    return choice
  end

  def single_expr_link(dsetID, geneID, levels, cutoff)
    link_to(format_expression(levels,cutoff,dsetID), 
      options = { :action => 'show_single_expression', :dsetID => dsetID, :geneID => geneID },
                  html_options = { :class => 'thickbox', :title => "" })
  end
  
  def preprocess_for_rank_representation(parts)
    # Up front processing for rank-based representation
    
    tmp  = Hash.new
    rank = Hash.new
    for part in parts
      rank[part] = 1   unless part == "N/A"
    end
    # Assign the ranks so that the condition (part)
    # with the lowest expression level is rank 1.
    # Yes, that's an unusual interpretation of "Rank"
    # We do this because we want the most expressed
    # (highest) value to have the brightest representation
    # and later code works out better this way.
    rank_array = rank.sort {|a,b| a[0].to_f <=> b[0].to_f}
    rank_num = 1
    for ra in rank_array
      rank[ra[0]] = rank_num
      rank_num += 1
    end

    rank
  end

  def format_expression(levels, cutoff, dsetID)
    must_be_positive = false
    by_rank = false
    rank = Array.new  # establishes the name in routine, not block, context

    parts = levels.split(/,/)
    result = "<table border=0 cellspacing=0 height=\"10px\" width=\"100%\"><tr>\n"
    if parts.length < 1
      result += "<td class=\"nodata_ribbon\">No Data</td>\n"
    else
      dset = Dataset.find(dsetID)
      avg = 0
      count = 0
      # For centered data, compute the average, so that we can bias
      # the values by it.  Also, set flag that we require positive values.
      if ((dset.channelCount == 1) && (session[:single_display].to_i == 0) ||
          (dset.channelCount == 2) && (session[:dual_display].to_i == 0))

        if dset.channelCount == 1
          must_be_positive = true
        end

        for part in parts
          if (part != "NA") && (!must_be_positive || part.to_f > 0.0)
            avg += part.to_f
            count += 1
          end
        end

        if count > 0
          avg /= count.to_f
        end
      end

      # Prepare for rank based "coloring" (actually, gray scale)
      if ((dset.channelCount == 1) && (session[:single_display].to_i == 2) ||
          (dset.channelCount == 2) && (session[:dual_display].to_i == 2))
        by_rank = true
        rank = preprocess_for_rank_representation(parts)          
      end  # rank-based preprocessing

      for part in parts
        if (part == "NA") || (must_be_positive && (part.to_f <= 0.0))    
          result += "<td bgcolor=\"#AAAAAA\"></td>\n"
        else
          # OK, we have data here.  Select how to process it.
          # We have a lot of nested ifs in the else parts, but they're 
          # intentionally not collapsed into elsif's.  They aren't parallel
          # conditions, and collapsing them would make it harder to understand
          # when maintaining in the future.
          if by_rank
            # Rank processing is the same for either single or dual channel data
            result += "<td bgcolor = \"#{convert_rank_to_gray_scale(
                                         rank[part], rank.length)}\"></td>\n"
          else
            # Not by rank.  Processing varies by channel count, method,
            # and color pair selection.
            if dset.channelCount == 1
              # Single channel data
              if session[:single_display].to_i == 0
                result += "<td bgcolor=\"#{convert_fold_to_hex_color(
                                           log2(part.to_f/avg), cutoff, 
                                           session[:single_color].to_i)}\"></td>\n"
              else
                result += "<td bgcolor=\"#{convert_transcript_to_hex_color(
                                           part.to_f, 16, 
                                           session[:single_color].to_i)}\"></td>\n"
              end
            else
              # Dual channel: now dset.channel_count == 2
              if session[:dual_display].to_i == 0
                result += "<td bgcolor=\"#{convert_fold_to_hex_color(
                                           part.to_f - avg, cutoff, 
                                           session[:dual_color].to_i)}\"></td>\n"
              else
                result += "<td bgcolor=\"#{convert_fold_to_hex_color(
                                         part.to_f, cutoff, 
                                         session[:dual_color].to_i)}\"></td>\n"
              end
            end
          end   # if by_rank
        end
      end  # For part in parts
    end # if parts.length < 2 else
    result += "</tr></table>"
  end

  def format_single_expression(gene, labels, levels, cutoff, dsetID)
    must_be_positive = false
    by_rank = false
    rank = Array.new  # establishes the name in routine, not block, context
    parts = levels.split(/,/)

    labelParts = labels.split(/~/)
    # peak 20110928 for SGD: "~" should be the delimiter but if too few are found, use "|":
    if labelParts.length < 2
      labelParts = labels.split('|')
    end

    result = "<table class=\"results\" width=\"100%\"><tr><td></td>\n"
    for label in labelParts
      result += "<td class=\"descr\">" + label + "</td>\n"
    end
    result += "</tr><tr><td>#{link_to_genepage(gene.name)}</td>\n"
    if parts.length == labelParts.length
      dset = Dataset.find(dsetID)
      avg = 0
      count = 0
      # For centered data, compute the average, so that we can bias 
      # the values by it.  Also, set flag that we require positive values.
      if ((dset.channelCount == 1) && (session[:single_display].to_i == 0) ||
          (dset.channelCount == 2) && (session[:dual_display].to_i == 0))

        if dset.channelCount == 1
          must_be_positive = true
        end

        for part in parts
          if (part != "NA") && (!must_be_positive || part.to_f > 0.0)
            avg += part.to_f
            count += 1
          end
        end
        if count > 0
          avg /= count.to_f
        end
      end

      # Prepare for rank based "coloring" (actually, gray scale)
      if ((dset.channelCount == 1) && (session[:single_display].to_i == 2) ||
          (dset.channelCount == 2) && (session[:dual_display].to_i == 2))
        rank = preprocess_for_rank_representation(parts)
        by_rank = true          
      end

      # CSS used for other than rank.  Rank display explicitly sets font
      # color based on the rank's background color.
      datum_class = 'class="single_expression_datum"'

      for part in parts
        if by_rank
          result += '<td bgcolor = "' +
            convert_rank_to_gray_scale(rank[part], rank.length) +
            %Q{"><font color="#{font_color_for_rank(rank[part], rank.length)}">#{part}</font></td>\n}
        elsif part == "NA"   
          result += "<td bgcolor=\"#AAAAAA\"></td>\n"
        else
          if dset.channelCount == 1
            if session[:single_display].to_i == 0
              val = log2(part.to_f / avg)
              result += "<td #{datum_class} bgcolor=\"#{convert_fold_to_hex_color(val, cutoff, 
                                         session[:single_color].to_i)}\">#{format("%.2f",val)}</td>\n"
            else
              result += "<td #{datum_class} bgcolor=\"#{convert_transcript_to_hex_color(part.to_f, 16, 
                                         session[:single_color].to_i)}\">#{part}</td>\n"
            end
          else # Dual channel: now dset.channel_count == 2
            if session[:dual_display].to_i == 0
              val = part.to_f - avg
              result += "<td #{datum_class} bgcolor=\"#{convert_fold_to_hex_color(val, cutoff, 
                                         session[:dual_color].to_i)}\">#{format("%.2f",val)}</td>\n"
            else
              result += "<td #{datum_class} bgcolor=\"#{convert_fold_to_hex_color(part.to_f, cutoff, 
                                               session[:dual_color].to_i)}\">#{part}</td>\n"
            end
          end
        end
      end  # For part in parts
    else
      result += "<td bgcolor=#333 colspan=" + labelParts.length.to_s + ">"
      result += "Gene not present in this dataset</td>"
    end
    result += "</tr></table>"
  end

  def font_color_for_rank(rank, num_ranks)
    color = ""
    if rank >= num_ranks/2
      color = "\#000000"
    else
      color = "\#FFFFFF"
    end
    color
  end

  def convert_rank_to_gray_scale(rank, num_ranks)
    # Note that the number of ranks may be less than the total number of 
    # conditions in the dataset, if there are N/A conditions, or 
    # conditions with the same expression level.

    color = "\#"
    # We're working to create a gray scale, so all three color segments are the same.
    if num_ranks ==1
      color += "FFFFFF"
    elsif num_ranks == 0
      color += "000000"
    else
      amount = (255.0 * (rank.to_f / num_ranks.to_f)).to_i
      channel = ""
      if amount < 16
        channel += "0"
      end
      channel += amount.to_s(16)
      color += channel * 3
    end
  end

  def convert_transcript_to_hex_color(lvl, cutoff, scheme)
    color = "\#"
    #Warm color
    if lvl >= 0
      if lvl < cutoff
        amount = (255 * (lvl / cutoff)).to_i
        if amount < 16
          color += "0"
        end
        color += amount.to_s(16)
      else
        color += "FF"
      end
      #Include the green channel if using blue/yellow
      if scheme == 1
        if lvl < cutoff
          amount = (255 * (lvl / cutoff)).to_i
          if amount < 16
            color += "0"
          end
          color += amount.to_s(16)
        else
          color += "FF"
        end
      else
        color += "00"
      end
      #Blue channel is always zero
      color += "00"
    else
      color += "AAAAAA"
    end
    return color
  end
  
  def convert_fold_to_hex_color(lvl, cutoff, scheme)
    color = "\#"
    if lvl > 0
      #Warm color
      if lvl < cutoff
        amount = (255 * (lvl / cutoff)).to_i
        if amount < 16
          color += "0"
        end
        color += amount.to_s(16)
      else
        color += "FF"
      end
      #Include the green channel if using blue/yellow
      if scheme == 1
        if lvl < cutoff
          amount = (255 * (lvl / cutoff)).to_i
          if amount < 16
            color += "0"
          end
          color += amount.to_s(16)
        else
          color += "FF"
        end
      else
        color += "00"
      end
      #Blue channel is always zero
      color += "00"
    else
      #Cool color
      #Red channel is always zero
      color += "00"
      if lvl > -cutoff
        amount = (255 * (-lvl / cutoff)).to_i
        if amount < 16
          color += "0"
        end
        color += amount.to_s(16)
      else
        color += "FF"
      end
      #Include blue channel if using blue/yellow
      if scheme == 1
        if lvl > -cutoff
          amount = (255 * (-lvl / cutoff)).to_i
          if amount < 16
            color += "0"
          end
          color += amount.to_s(16)
        else
          color += "FF"
        end
      end
    end
    return color
  end
    
  def convert_to_hex_color(lvl, cutoff)
    lvl = lvl.to_f
    color = "\#"
    if lvl > 0
      if lvl < cutoff
        amount = (255 * (lvl / cutoff)).to_i
        if amount < 16
          color += "0"
        end
        color += amount.to_s(16)
      else
        color += "FF"
      end
      color += "00"
    else
      color += "00"
      if lvl > -cutoff
        amount = (255 * (-lvl / cutoff)).to_i
        if amount < 16
          color += "0"
        end
        color += amount.to_s(16)
      else
        color += "FF"
      end
    end
    color += "00"
    color
  end
  
  def score_to_color(score, cutoff)
    score = score.to_f
    color = "#00"
    if score > cutoff
      color += "FF"
    else
      amount = (255 * (score / cutoff)).to_i
      if amount < 16
        color += "0"
      end
      color += amount.to_s(16)
    end
    color += "00"
  end
  
  def getGOrows(geneNames)
    rows = ""
    require 'socket'
    sock = TCPSocket.new('localhost', GO_SOCKET_NUMBER)
    sock.send("0.05\n" + geneNames + "\n",0)
    msg = sock.recv(10000)
    lines = msg.split("\n")
    for line in lines
      parts = line.split("\t")
      if parts.length > 8
        if parts[2] == "cellular_component"
          rows += '<tr class="cellular_component">'
        elsif parts[2] == "molecular_function"
          rows += '<tr class="molecular_function">'
        else #if parts[2] == "biological_process"
          rows += '<tr class="biological_process">'
        end
        rows += "<td>" + link_to_goterm(parts[1],parts[0])
        rows += " (" + parts[2] + ")</td>\n"
        rows += "<td class=\"fit\">" + ("%.2e" % parts[3].to_f).to_s + "</td>\n"
        rows += "<td class=\"fit\">" + parts[4] + " of " + parts[5] + "</td>\n"
        rows += "<td class=\"fit\">" + parts[6] + " of " + parts[7] + "</td>\n"
        rows += "<td>"
        genes = parts[8].split(/[,\s]+/)
        for i in 0...genes.length
          rows += link_to_genepage(genes[i])
          if i == genes.length-1
            rows += "</td>\n"
          else
            rows += ", "
          end
        end
        rows += "</tr>"
      end
    end
    rows
  end

end
