# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  # Variant of link_to for a link that opens in a new tab or window.
  # If SPELL_OPTIONS[:new_window_icon] is "" then do not use an icon.
  # Example: link_to_new_tab("Dataset Tags",
  #                          {:action => 'tag_details'},
  #                          {:title => "Open Dataset Tags page in new tab" } )
  # Notice the default values of :class and :title
  # (But for MSIE we would use CSS (:after) to specify the icon (to make it easy to customize).)
  def link_to_new_tab(title, url_options = {}, html_options = {} )
    defaults = {:target => "_blank", :class => "newtab", :title => "Open in new tab" }
    icon = SPELL_OPTIONS[:new_window_icon]
    icon = "green_new_window_icon.gif" unless String === icon
    if icon == ""
      ititle = title
    else
      ititle = title + image_tag( icon )
    end
    # Let html_options override defaults:
    link_to(ititle, url_options, defaults.merge(html_options) )
  end

  def link_to_pubmed(pubmedID)
    link_to("#{pubmedID}", PUBINDEX_PATH + "#{pubmedID}", :popup => true)
  end

  def link_to_genepage_notrans(geneID)
    link_to("#{geneID}", GENEPAGE_PATH + "#{geneID}", :popup => true)
  end

  #def link_to_genepage(geneID)
  #  link_to("#{format_gene(geneID)}", GENEPAGE_PATH + "#{geneID}", :popup => true)
  #end

  def link_to_genepage(geneID)
    link_to_genepage_notrans(format_gene(geneID))
  end

  def link_to_goterm(termName, goID)
  		link_to("#{termName}", GO_PATH + "#{goID}", :popup => true)
  end    

  def format_gene(gene)
    require 'translate_names'
    gene.upcase!
    # Ruby returns its last evaluated expression's value
    systematic_to_common(gene) || 
    systematic_to_common(alias_to_systematic(gene)) ||
    gene
  end

  def log2(val)
    if val < 0.00001
      return 0
    end
  	return Math.log(val) / Math.log(2)
  end

  # In case Rails 3.1's truncate with :separator is unavailable.
  # The maximum length of the result is (char_limit + omission.length)
  def truncate_at_whitespace(text, char_limit = 80, omission = " ...")
    return text if text.size <= char_limit
    size = 0
    result = []
    trail = ""
    # Specifying ' ' eliminates all whitespace
    text.split(' ').each { |token|
      size += token.size()
      if size > char_limit
        trail = omission
        break
      end
      result << token
      size += 1
    }
    result.join(" ") + trail
  end

  # td_prolog is a helper method for emit_td().
  # Specify checked=true to check the checkbox initially.
  # Specify cssclass=SOMECLASS (a String) to add SOMECLASS to the class
  def td_prolog(tag, checked = false, cssclass = false)

    cssclass = (String === cssclass) ? "toggleable #{cssclass}" : "toggleable"

    # Be careful with spaces!
    %Q{<td class="tag">
          <input name="tags[]" class="#{cssclass}" type="checkbox" value="} +
      tag + '"' +
      " #{checked ? "checked='checked'" : ""} />"
  end

  # emit_td(tags,n) is used to construct the table of checkboxes for selecting dataset tags;
  # it emits "<td />" if n >= tags.length, otherwise it emits a td element for tags[n],
  # paying special attention to the cases "other" and "not yet curated".
  # If prepopulate, then it should be an array of strings whose boxes are to be checked.
  def emit_td(tags, n, prepopulate = nil)
    if n >= tags.length
      return "<td />"
    end
    tag = tags[n]
    checked = prepopulate ? prepopulate.include?(tag) : nil

    chktag = tags[n]

    if session[:tags_searched]
      previous = session[:tags_searched].include?(chktag) 
      previous = previous ? "previous" : nil
    else
      previous = nil
    end
    if tag == "not yet curated"
      return td_prolog(tag, checked, previous) +  tag + help_link('NotYetCuratedTag') + "</td>"
    elsif  tag == "other"
      return td_prolog(tag, checked, previous) +  tag + help_link('OtherTag') + "</td>"
    end
    case tag
    when "", nil
      "<td />"
    else
      td_prolog(tag, checked, previous) + tag + "</td>"
    end
  end

end
