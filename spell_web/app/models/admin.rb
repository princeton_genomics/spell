class Admin < ActiveRecord::Base

  validates_format_of :content_type, :with => /^text/,
            :message => "--- you can only upload text files"
            
  def filename=(filename)
    self.name = filename_list_field.original_filename
    self.content_type = filename_list_field.content_type.chomp
    self.data = filename_list_field.read
  end
  
  def save
    @admin = Admin.new(params[:admin])
    if @admin.save
      redirect_to(:action => 'list_datasets', :id => @admin.id)
    else
      render(:action => 'new_list_datasets')
    end
  end

end
