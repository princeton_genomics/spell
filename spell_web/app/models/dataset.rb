class Dataset < ActiveRecord::Base
  validates_presence_of :name, :description, :author, :filename, :channelCount
  validates_numericality_of :num_conds, :pubmedID, :channelCount
  
  protected
  def validate
    errors.add(:num_conds, "must be greater than zero") unless num_conds.nil? || num_conds > 0
    #errors.add(:channelCount, "must be 1 or 2") unless channelCount == 0 || channelCount == 1 
  end
  
end
