# module TranslateNames
  def systematic_to_common(sys_name)
 #   return nil if sys_name.nil?
    return "systematic_to_common_called_with_nil!" if sys_name.nil?

    common_gene = Common_gene.find(:first, :conditions => [ "systematic_name = ?", sys_name])
    return sys_name if common_gene.nil?

    return common_gene.common_name
  end

  def alias_to_systematic(alias_name)
    return nil if alias_name.nil?;

    g_alias = Gene_alias.find(:first, :conditions => [ "alias_name = ?", alias_name])
    return alias_name if g_alias.nil?

    return g_alias.systematic_name
  end

  def gene_id_to_systematic(id)
    gene = Gene.find(id)
    return "Unfound gene id: #{id}" if gene.nil?

    return gene.name
  end

  def gene_id_to_name (id)
    gene = Gene.find(id)
    return "Unfound gene id: #{id}" if gene.nil?

    return systematic_to_common(gene.name);
  end

