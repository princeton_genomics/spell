# Copyright (C) 2011 Peter Koppstein pkoppstein@gmail.com
# License: Creative Commons Attribution-NonCommerical-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)
# See http://creativecommons.org/licenses/by-nc/3.0/
# Attribution shall include the copyright notice above.

# Version = 0.1.4

=begin
This file defines the Assert class and adds one function to the Kernel module.
That function is named "assert".

Synopsis:
   assert { ... } 
   or
   assert(msg, ...) { ... }
   or 
   assert(msg, ... )

   Assert.status
   Assert.on
   Assert.off
   Assert.stream
   Assert.stream= <STREAM>

Description:

 * Assert.on turns assertion checking on, and Assert.off turns assertion checking off.
 * Assert.status returns true or false as assertion checking is on or off.
 * Assertion violation messages are printed to Assert.stream.
 * "assert" always returns true.
 * To send assertion messages to Rails.logger:  Assert.stream = Rails.logger.
 * Assert.stream=(x) will be a no-op unless x is a stream or responds to 'debug'.

1. assert { ... } 
   assert( msg, ... ) { ... }
   If assertion checking is on and if the block does not return
   true, an assertion violation notice is printed together with each
   message except that if a message is a symbol, a string of the form
   "name = value" is printed, where 'value' is the value of the variable
   corresponding to the symbol.
2) assert(msg, ... )
   If assertion checking is on, the messages are printed to
   the assertion stream, one per line, without using eval,
   as the binding of the symbols is unavailable.

Illustration:

> require 'simple_assert'
=> true

> Assert.on
=> false   # the previous value of Assert.status

> x = 1
=> 1

> assert { x == 2 }
Assertion failed: (irb):4
=> true

> assert(:x) { x == 2 }
Assertion failed: (irb):5
x = 1
=> true

> assert("x==0", :x) { x == 0 }
Assertion failed: (irb):6
x==0
x = 1
=> true


Other examples:

 @x=1; assert("@x==0", :@x) 
 assert("assertion checking is on:", :x)

=end

# Define Assert as a class because Module "class variables" become
# class variables of classes that include the module, and module
# "instance variables" can give rise to name conflicts if two modules
# are "mixed in".  However, there is no need for Assert to have any
# instances, so we undefine :initialize

class Assert
  @@on = false
  @@stream = STDOUT

  # Hide the warning that undef_method :initialize elicits:
  def self.undefine_initialize
    original_verbosity = $VERBOSE
    $VERBOSE = nil
    # :new is not yet setup
    undef_method :initialize
    $VERBOSE = original_verbosity
  end

  Assert.undefine_initialize

  def self.status
    @@on  
  end

  def self.on
    was = @@on
    @@on = true
    was
  end

  def self.off
    was = @@on
    @@on = false
    was
  end

  def self.stream ; @@stream ; end

  # Support Logger
  # return the prior stream
  def self.stream=(stream)
    was=@@stream
    if IO === stream 
      @@stream = stream
      return was
    elsif stream.respond_to? :debug
      arity = stream.method(:debug).arity
      if arity < 0 or arity == 1
        @@stream = stream
        return was
      end
    end
    $stderr.print "Assertion messages can only be directed to an IO stream or to an object that responds to debug messages\n"
    was
  end

  # Add newline only if using print
  def self.println msg
    if @@stream.respond_to? :print
      @@stream.print msg, "\n"
    else
      @@stream.debug msg
    end
  end

  def self.print msg
    if @@stream.respond_to? :print
      @@stream.print msg
    else
      @@stream.debug msg
    end
  end
      
end


module Kernel
  def assert(*msgs, &block)
    if Assert.status
      begin
        if block_given?
          binding = block.send(:binding)
          unless yield
            Assert.print "Assertion failed: "
            begin
              raise Exception
            rescue Exception => e
              # $e = e.backtrace[1]
              Assert.println e.backtrace[1].sub( /:in `.*/, '')
            end
            msgs.each { |msg|
              if Symbol === msg
                Assert.println "#{msg} = #{eval(msg.to_s,binding)}"
              else
                Assert.println "#{msg}"
              end
            }
          end
        else
          msgs.each { |msg|
            Assert.println "#{msg}"
          }
        end
      rescue Exception => e
        $stderr.print "ERROR: assertion raised an error: "
        $stderr.print e.backtrace[1].sub( /:in `.*/, ''),"\n"
        $stderr.print e, "\n"
      end
    end
    true
  end
end

if __FILE__ == $0
  def tests 
    Assert.on
    assert("Four assertion failures should follow:")
    x=1; assert {x == 0}
    x=1; assert(2, :x) { x == 0 }
    x=1; assert(3, "x==0", :x) { x == 0 }
    @x=2; assert("@x==0", :@x) 
    @x=2; assert(4, "@x==0", :@x) {@x == 0}
    x=1; assert("assertion checking is on but we cannot see the value of :x", :x)
  end
  tests
  # require 'logger'
  # logger = Logger.new "/tmp/simple_assert.out"
  # Assert.stream = logger
  Assert.stream = STDERR
  tests
end
