#!/usr/bin/ruby

if ARGV.length < 2
	puts "ARGS:"
	puts "0 - old style dataset db config file"
	puts "1 - path to data files"
	exit(0)
end

breakChar = "|"

IO.foreach(ARGV[0]) do |line|
	line.strip!
	parts = line.split("\t")
	fname = parts[8]

	print parts[0]
	for i in 1..7
		print "\t" + parts[i]
	end

	fin = open(ARGV[1] + fname)
	condLine = fin.readline.strip!
	condParts = condLine.split("\t")
	if condLine.include?(breakChar)
		$stderr.puts "WARNING: " + fname + " contains the break string"
	end
	print "\t" + condParts[3]
	for i in 4...condParts.length
		print breakChar + condParts[i]
	end

	for i in 8..9
		print "\t" + parts[i]
	end

	puts
end
	
