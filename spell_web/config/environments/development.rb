# Settings specified here will take precedence over those in config/environment.rb

# In the development environment your application's code is reloaded on
# every request.  This slows down response time but is perfect for development
# since you don't have to restart the webserver when you make code changes.
config.cache_classes     = true # normally set to false in development

# Log error messages when you accidentally call methods on nil.
config.whiny_nils        = true

# Enable the breakpoint server that script/breakpointer connects to
#config.breakpoint_server = true #Unused in Rails v2+

# Show full error reports and disable caching
config.action_controller.consider_all_requests_local = true
config.action_controller.perform_caching             = false

# Don't care if the mailer can't send
config.action_mailer.raise_delivery_errors = false

# Start logger for the sake of assertions:
config.logger = ActiveSupport::BufferedLogger.new(Rails.root.join("log", Rails.env + ".log"))

# Either require simple_assert or:
# module Kernel ; def assert(*msg); true; end ; end
require 'simple_assert'
Assert.on

# Rails.logger and RAILS_DEFAULT_LOGGER are not yet defined
Assert.stream = config.logger
