drop table if exists datasets;
create table datasets (
  id                int             not null auto_increment,
  pubmedID          int             not null,
  filename          text            not null,
  geoID		    varchar(10)     not null,
  platformID        varchar(10)     not null,
  channelCount      int		    not null,
  name              text            not null,
  description       text            not null,
  num_conds         int             not null,
  num_genes         int             not null,
  author            varchar(100)    not null,
  all_authors	    text	    not null,
  title		    text	    not null,
  journal	    text	    not null,
  pub_year	    int		    not null,
  cond_descs        text	    not null,
  tags		    text	    not null,
  primary key (id)
);

drop table if exists genes;
create table genes (
  id                int             not null auto_increment,
  name              varchar(20)     not null,
  primary key (id)
);

drop table if exists exprs;
create table exprs (
  geneID	    int		    not null,
  dsetID	    int		    not null,
  data_table	    text	    not null,
  primary key (geneID, dsetID)
 );

drop table if exists users;
create table users (
  id          	   int			not null auto_increment,
  name		   varchar(100)		not null,
  hashed_password  char(40)		null,
  primary key (id)
);
 
drop table if exists flexible_dataset_links;
create table flexible_dataset_links (
  id   	           int                  not null auto_increment,
  filename         varchar(255)         not null,
  label            varchar(255)         not null,
  url		   varchar(255)         not null,
  primary key (id)
);

drop table if exists common_genes;
create table common_genes (
  id   	           int                  not null auto_increment,
  systematic_name  varchar(255)         not null,
  common_name      varchar(255)         not null,
  primary key (id)
);

drop table if exists gene_aliases;
create table gene_aliases (
  id   	           int                  not null auto_increment,
  alias_name       varchar(255)         not null,
  systematic_name  varchar(255)         not null,
  primary key (id)
);

drop table if exists sessions;
create table sessions (
  id               int(11)              not null auto_increment,
  session_id       varchar(255)         not null,
  data             longtext,
  created_at       datetime             default null,
  updated_at       datetime             default null,
  primary key  (id),
  key    index_sessions_on_session_id (session_id),
  key    index_sessions_on_updated_at (updated_at)
);

