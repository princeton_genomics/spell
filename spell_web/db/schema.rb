# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20100512130321) do

  create_table "common_genes", :force => true do |t|
    t.string "systematic_name", :null => false
    t.string "common_name",     :null => false
  end

  add_index "common_genes", ["systematic_name"], :name => "index_common_genes_on_systematic_name"

  create_table "datasets", :force => true do |t|
    t.integer "pubmedID",                    :null => false
    t.text    "filename",                    :null => false
    t.string  "geoID",        :limit => 10,  :null => false
    t.string  "platformID",   :limit => 10,  :null => false
    t.integer "channelCount",                :null => false
    t.text    "name",                        :null => false
    t.text    "description",                 :null => false
    t.integer "num_conds",                   :null => false
    t.integer "num_genes",                   :null => false
    t.string  "author",       :limit => 100, :null => false
    t.text    "all_authors",                 :null => false
    t.text    "title",                       :null => false
    t.text    "journal",                     :null => false
    t.integer "pub_year",                    :null => false
    t.text    "cond_descs",                  :null => false
    t.text    "tags",                        :null => false
  end

  create_table "exprs", :id => false, :force => true do |t|
    t.integer "geneID",     :null => false
    t.integer "dsetID",     :null => false
    t.text    "data_table", :null => false
  end

  create_table "flexible_dataset_links", :force => true do |t|
    t.string "filename", :null => false
    t.string "label",    :null => false
    t.string "url",      :null => false
  end

  create_table "gene_aliases", :force => true do |t|
    t.string "alias_name",      :null => false
    t.string "systematic_name", :null => false
  end

  add_index "gene_aliases", ["alias_name"], :name => "index_gene_aliases_on_alias_name", :unique => true

  create_table "genes", :force => true do |t|
    t.string "name", :limit => 20, :null => false
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id",                       :null => false
    t.text     "data",       :limit => 2147483647
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "users", :force => true do |t|
    t.string "name",            :limit => 100, :null => false
    t.string "hashed_password", :limit => 40
  end

end
