import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

/**
 * 
 * @author mhibbs
 *
 * This is essentially a wrapper class to keep track of multiple Organism classes, and to load and manage
 * homology and orthology information within and between organisms
 *
 */
public class Homology {

	/**
	 * Map from unique organism identifier (eg. HSA, SCE) to Organism object
	 */
	public HashMap<String, Organism> orgs;
	
	/**
	 * Vector (array) of all loaded gene families.  Note that only those families with members in at least
	 * one loaded organism are created and indexed.  The id field of each family object corresponds to its
	 * index in this vector.
	 */
	public Vector<GeneFamily> families;
	
	public boolean usingOrthology;	
	
	/**
	 * Default constructor
	 */
	public Homology () {
		orgs = new HashMap<String,Organism>();
		families = new Vector<GeneFamily>();
		usingOrthology = false;
	}
	
	/**
	 * Loads an organism file and adds it to the hash of loaded organisms
	 * @param id The unique string identifier of the organism (eg. HSA, SCE)
	 * @param geneFileName Path and file name of the organism's gene list file
	 */
	public void addOrganism (String id, String geneFileName) {
		orgs.put(id, new Organism(id,geneFileName));
	}
	
	public void addGeneFamilies (String familyFileName) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(familyFileName));
			String line;
			//For each family in the orthology file...
			while ((line = br.readLine()) != null) {
				String[] parts = line.split("\t");
				if (parts != null) {
					//Construct a new gene family
					GeneFamily family = new GeneFamily();
					//For each gene in the family...
					for (int i=0; i<parts.length; i++) {
						String[] subparts = parts[i].split("\\|");
						//If the gene is in an organism in use...
						if (orgs.get(subparts[0]) != null) {
							//Get the corresponding gene object
							Gene g = orgs.get(subparts[0]).genes.get(subparts[1]);
							if (g == null) g = orgs.get(subparts[0]).genes.get(subparts[1].toUpperCase());
							if (g == null) {
								System.err.println("WARNING: Unable to find match for " + parts[i]);
							}
							else {
								//Add the gene to the family, and link the family to the gene and organism
								family.genes.add(g);
								g.family = family;
								orgs.get(subparts[0]).families.add(family);
							}
						}
					}
					//If the family is not empty after adding all valid genes...
					if (family.genes.size() > 0) {
						//Give the family an index and store it
						family.id = families.size();
						families.add(family);
					}
				}
			}
			for (Iterator<Organism> it = orgs.values().iterator(); it.hasNext(); ) {
				Organism org = it.next();
				//Also indicate that this organism is using orthology information
				org.usingOrthology = true;
			}
			usingOrthology = true;
		}
		catch (FileNotFoundException e) {
			System.err.println("WARNING: orthology file not found: " + familyFileName);
			System.err.println("         Assuming that no gene family information is desired");
		}
		catch (IOException e) {
			System.err.println("ERROR reading gene family orthology file: " + familyFileName);
			System.err.println(e.getLocalizedMessage());
			System.exit(0);
		}
		catch (NullPointerException e) {
			System.err.println("WARNING: Null orthology file recieived.");
			System.err.println("         Assuming that no gene family information is desired");
		}
		
		//Each gene must belong to exactly one gene family, so find all orphan genes and make the families
		//For each organism...
		for (Iterator<String> org_it = orgs.keySet().iterator(); org_it.hasNext(); ) {
			String key = org_it.next();
			Organism org = orgs.get(key);
			//For each gene...
			for (Iterator<String> gene_it = org.genes.keySet().iterator(); gene_it.hasNext(); ) {
				String geneName = gene_it.next();
				Gene g = org.genes.get(geneName);
				//If the gene is an orphan...
				if (g.family == null) {
					//Create a family for the gene
					GeneFamily family = new GeneFamily();
					family.genes.add(g);
					g.family = family;
					org.families.add(family);
					family.id = families.size();
					families.add(family);
				}
			}
		}
	}
	
	public Vector<ExpressionDataset> getAllDatasets () {
		Vector<ExpressionDataset> dsets = new Vector<ExpressionDataset> ();
		for (Iterator<Organism> it = orgs.values().iterator(); it.hasNext(); ) {
			Organism org = it.next();
			dsets.addAll(org.datasets);
		}
		return dsets;
	}
	
	public Vector<ExpressionDataset> getDatasetsByID (String[] ids) {
		Vector<ExpressionDataset> dsets = new Vector<ExpressionDataset> ();
		for(int i=0; i<ids.length; i++) {
			int id = Integer.parseInt(ids[i]);
			for (Iterator<Organism> it = orgs.values().iterator(); it.hasNext(); ) {
				Organism org = it.next();
				if (org.id_to_dataset.keySet().contains(id)) {
					dsets.add(org.id_to_dataset.get(id));
				}
			}
		}
		return dsets;
	}
	
	public Vector<GeneFamily> getGenesByID (String[] ids) {
		HashSet<GeneFamily> genes = new HashSet<GeneFamily> ();
		for(int i=0; i<ids.length; i++) {
			int id = Integer.parseInt(ids[i]);
			for (Iterator<Organism> it = orgs.values().iterator(); it.hasNext(); ) {
				Organism org = it.next();
				if (org.id_to_gene.keySet().contains(id)) {
					genes.add(org.id_to_gene.get(id).family);
				}
			}
		}
		return new Vector<GeneFamily>(genes);
	}
	
	public GeneFamily findFamily (String geneName) {
		Gene g = null;
		for (Iterator<Organism> it = orgs.values().iterator(); it.hasNext(); ) {
			Organism org = it.next();
			if (org.genes.containsKey(geneName)) {
				if (g != null) {
					System.err.print("WARNING: " + geneName + " maps to multiple genes, including " + g.org.name + "|");
					System.err.println(g.name + " and " + org.name + "|" + org.genes.get(geneName).name);
				}
				else {
					g = org.genes.get(geneName);
				}
			}
		}
		if (g == null) {
			System.err.println("WARNING: unable to find gene: " + geneName);
			return null;
		}
		return g.family;
	}
}
