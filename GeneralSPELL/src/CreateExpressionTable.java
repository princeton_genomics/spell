import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Vector;


public class CreateExpressionTable {

	public static void main (String[] args) {
		//Usage messages:
		if (args.length < 1) {
			System.err.println("ARGS:");
			System.err.println("0 - configuration file:");
			System.err.println("    1 entry per file for each organism, tab-delimited with:");
			System.err.println("    OrgID \\t GeneFile \\t ListOfFiles \\t PathToFiles");
			System.err.println("STDOUT - expression table ready to import to mySql");
			System.exit(0);
		}
		
		//Parse the configuration file
		Vector<String>	orgIDs 			= new Vector<String>();
		Vector<String>	orgGeneFiles 	= new Vector<String>();
		Vector<String>	orgDataFiles	= new Vector<String>();
		Vector<String>	orgPath			= new Vector<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(args[0]));
			String line;
			while (((line = br.readLine()) != null) && (!line.equals(""))) {
				String[] parts = line.split("\t");
				orgIDs.add(parts[0]);
				orgGeneFiles.add(parts[1]);
				orgDataFiles.add(parts[2]);
				orgPath.add(parts[3]);
			}
		}
		catch (IOException e) {
			System.err.println("ERROR reading configuration file: " + args[0]);
			System.err.println(e.getMessage());
			System.exit(0);
		}
		
		//Load the genes & datasets in the config file
		Homology hom = new Homology();
		for (int i=0; i<orgIDs.size(); i++) {
			System.err.print("Loading " + orgIDs.get(i) + "...");
			hom.addOrganism(orgIDs.get(i), orgGeneFiles.get(i));
			hom.orgs.get(orgIDs.get(i)).loadDatasets(orgDataFiles.get(i), orgPath.get(i));
			System.err.println("done.");
		}
		
		//Output the expression information for all loaded datasets
		DecimalFormat twoPlaces = new DecimalFormat("0.00");
		Organism org;
		for (Iterator<Organism> it = hom.orgs.values().iterator(); it.hasNext(); ) {
			org = it.next();
			for (int d=0; d<org.datasets.size(); d++) {
				ExpressionDataset dset = org.datasets.get(d);
				Gene g;
				for (Iterator<Gene> git = dset.data.keySet().iterator(); git.hasNext(); ) {
					g = git.next();
					Vector<Float> vals = dset.data.get(g);
					if ((vals != null) && (vals.size() > 0)) {
						String valStr = "";
						for (int i=0; i<vals.size(); i++) {
							if (Float.isNaN(vals.get(i)))	valStr += "NA";
							else							valStr += twoPlaces.format(vals.get(i));
							if (i != vals.size() - 1)		valStr += ",";
						}
						System.out.println(g.id + "\t" + dset.id + "\t" + valStr);
					}
				}
				System.out.flush();
			}
		}
		
	}
	
}
