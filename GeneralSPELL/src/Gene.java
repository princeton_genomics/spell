import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author mhibbs
 *
 * Contains all information and operations about a gene, including it's unique identifier, organism, 
 * aliases, and membership in orthology groups.
 *
 */
public class Gene {

	public Organism		org;
	public String		name;
	public Set<String>	aliases;
	public GeneFamily	family;
	public int			id;
	
	/**
	 * Default constructor
	 */
	public Gene () {
		org = null;
		name = null;
		aliases = new HashSet<String>();
		family = null;
	}
	
	public Gene (Organism o, String n, int i) {
		org = o;
		name = n;
		aliases = new HashSet<String>();
		family = null;
		id = i;
	}
	
}
