import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import cern.jet.stat.Probability;

/**
 * 
 * @author mhibbs
 *
 * Contains all information for a single gene expression dataset, independent of platform.
 * This includes a reference to organism, data for genes valid in that organism. Can perform operations on that data,
 * such as correlations and distances between pairs of genes or groups of genes.
 * 
 * Each dataset file has a corresponding stats file (original_file_name.ext.sinfo) with the following format:
 * All 		\\t 	<gene_pairs_z_mean> 	\\t 	<gene_pairs_z_std>
 * Min 		\\t 	<family_min_z_mean> 	\\t 	<family_min_z_std>
 * Max 		\\t 	<family_max_z_mean> 	\\t 	<family_max_z_std>
 * Mean 	\\t 	<family_mean_z_mean> 	\\t 	<family_mean_z_std>
 * Median 	\\t 	<family_median_z_mean> 	\\t 	<family_median_z_std>
 * 
 */
public class ExpressionDataset {
	
	public Organism 						org;
	public HashMap<Gene, Vector<Float>>		data;
	public Vector<String>					conds;
	public String							filename;
	public int								id;
	
	public static final int MAX = 0;
	public static final int MIN = 1;
	public static final int MEAN = 2;
	public static final int MEDIAN = 3;
	public static final int SIGNIFICANT_MEDIAN = 4;
	
	public static final int PEARSON = 0;
	public static final int ZSCORE  = 1;
	public static final int ZSCORE2 = 2;
	public static final int SIGNIFICANCE = 3;
	
	public static final float ALPHA = 0.05f;
	
	public float z_mean, z_std;
	public float zmin_mean, zmin_std;
	public float zmax_mean, zmax_std;
	public float zmean_mean, zmean_std;
	public float zmedian_mean, zmedian_std;
	public float zsigmed_mean, zsigmed_std;
	
	/**
	 * Default constructor
	 */
	public ExpressionDataset() {
		org = null;
		data = new HashMap<Gene,Vector<Float>>();
		conds = new Vector<String>();
	}
	
	/**
	 * Basic constructor - loads all data in the specified pcl file that corresponds to one of the
	 * valid gene names loaded into the organism
	 * @param o Organism of the dataset
	 * @param dataFileName Path and filename of a pcl formatted expression dataset
	 */
	public ExpressionDataset(Organism o, String dataFileName, int idval) {
		org = o;
		data = new HashMap<Gene,Vector<Float>>();
		conds = new Vector<String>();
		filename = dataFileName;
		id = idval;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(dataFileName));
			//The first line of the file must contain the condition labels
			String line = br.readLine();
			String[] parts = line.split("\t");
			int offset = 2;
			if (parts[2].equalsIgnoreCase("GWEIGHT"))	offset = 3;
			for (int i=offset; i<parts.length; i++) {
				conds.add(parts[i]);
			}
			//If the second line begins with EWEIGHT, then ignore it
			line = br.readLine();
			parts = line.split("\t");
			if (parts[0].equalsIgnoreCase("EWEIGHT"))	line = br.readLine();
			//At this point, line contains the first full data line
			while (line != null) {
				parts = line.split("\t");
				//Try to find the gene corresponding to this line
				Gene g = org.genes.get(parts[0]);
				if (g == null)	g = org.genes.get(parts[0].toUpperCase());
				if (g == null)	g = org.genes.get(parts[1]);
				if (g == null)	g = org.genes.get(parts[1].toUpperCase());
				
				//If there is a corresponding gene, load the data
				if (g != null) {
					Vector<Float> d = new Vector<Float>();
					for (int i=offset; i<parts.length; i++) {
						try {
							d.add(Float.parseFloat(parts[i]));
						}
						catch (NumberFormatException e) {
							d.add(Float.NaN);
						}
					}
					while (d.size() < conds.size())	d.add(Float.NaN);
					data.put(g, d);
				}
					
				//Advance to the next line
				line = br.readLine();
			}
			br.close();
		}
		catch (IOException e) {
			System.err.println("ERROR reading expression dataset file: " + dataFileName);
			System.err.println(e.getLocalizedMessage());
			System.exit(0);
		}
		//Load the stats also
		loadOrCreateStatFile(dataFileName);
	}
	
	public void loadOrCreateStatFile (String dataFileName) {
		String statFileName = dataFileName + ".sinfo";
		//Intialize all values to NaN
		z_mean = z_std = zmin_mean = zmin_std = zmax_mean = zmax_std = zmean_mean = zmean_std
			= zmedian_mean = zmedian_std = zsigmed_mean = zsigmed_std = Float.NaN;
		boolean remainingNaNs = false;
		//Try to load the stats information...
		try {
			BufferedReader br = new BufferedReader(new FileReader(statFileName));
			//All gene pairs
			String line = br.readLine();
			String[] parts = line.split("\t");
			z_mean = Float.parseFloat(parts[1]);
			z_std  = Float.parseFloat(parts[2]);
			if (Float.isNaN(z_mean) || Float.isNaN(z_std))
				throw new Exception ("Invalid stat file.");
			//Min family pairs
			line = br.readLine();
			parts = line.split("\t");
			zmin_mean = Float.parseFloat(parts[1]);
			zmin_std  = Float.parseFloat(parts[2]);
			if (Float.isNaN(zmin_mean) || Float.isNaN(zmin_std))
				remainingNaNs = true;
			//Max family pairs
			line = br.readLine();
			parts = line.split("\t");
			zmax_mean = Float.parseFloat(parts[1]);
			zmax_std  = Float.parseFloat(parts[2]);
			if (Float.isNaN(zmax_mean) || Float.isNaN(zmax_std))
				remainingNaNs = true;
			//Mean family pairs
			line = br.readLine();
			parts = line.split("\t");
			zmean_mean = Float.parseFloat(parts[1]);
			zmean_std  = Float.parseFloat(parts[2]);
			if (Float.isNaN(zmean_mean) || Float.isNaN(zmean_std))
				remainingNaNs = true;
			//Median family pairs
			line = br.readLine();
			parts = line.split("\t");
			zmedian_mean = Float.parseFloat(parts[1]);
			zmedian_std  = Float.parseFloat(parts[2]);
			if (Float.isNaN(zmedian_mean) || Float.isNaN(zmedian_std))
				remainingNaNs = true;
			//Sig Med family pairs
			line = br.readLine();
			parts = line.split("\t");
			zsigmed_mean = Float.parseFloat(parts[1]);
			zsigmed_std  = Float.parseFloat(parts[2]);
			if (Float.isNaN(zsigmed_mean) || Float.isNaN(zsigmed_std))
				remainingNaNs = true;
			//Close file
			br.close();
			//If any of the family statistics were NaNs, and if we're using orthology information,
			//then you must recalculate...
			if (remainingNaNs && org.usingOrthology)
				throw new Exception ("GeneFamily linkage statistics not present.");
		}
		catch (Exception e) {
			System.err.println("WARNING: Unable to read stats file: " + statFileName);
			System.err.println("         Recalculating and saving stats");
			recalculateAndSaveStatFile(dataFileName);
		}
	}
	
	public void recalculateAndSaveStatFile (String dataFileName) {
		String statFileName = dataFileName + ".sinfo";
		
		Vector<Gene> geneVec = new Vector<Gene>(data.keySet());
		
		int maxSz = geneVec.size() * geneVec.size() / 2;
		
		//Try to allocate enough memory to hold all pairwise distance values
		try {
			float[] valArray = new float[maxSz];
					
			//Find the mean/stddev of all gene-wise pairs
			int idx = 0;
			if (Float.isNaN(z_mean) || Float.isNaN(z_std)) {
				System.err.print("\tCalculating per-gene zScore statistics...");
				for (int i=0; i<geneVec.size(); i++) {
					Gene x = geneVec.get(i);
					for (int j=i+1; j<geneVec.size(); j++) {
						Gene y = geneVec.get(j);
						float val = fisherZtransform(pearsonCorrelation(x, y));
						if (!Float.isNaN(val))	{
							valArray[idx++] = val;
						}
					}
				}
				z_mean = mean(valArray, idx);
				z_std  = stddev(valArray, idx, z_mean);
				System.err.println("done.");
			}
			
			//Then find the mean/stddev for all family-wise pairs
			if (org.usingOrthology) {
				Vector<GeneFamily> familyVec = org.getGeneFamilies();
				
				//--for min linkage
				if (Float.isNaN(zmin_mean) || Float.isNaN(zmin_std)) {
					System.err.print("\tCalculating per-family min-linkage statistics...");
					idx = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							float val = fisherZtransform(pearsonCorrelation(fx, fy, MIN));
							if (!Float.isNaN(val))	{
								valArray[idx++] = val;
							}
						}
					}
					zmin_mean = mean(valArray, idx);
					zmin_std  = stddev(valArray, idx, zmin_mean);
					System.err.println("done.");
				}
				
				//--for max linkage
				if (Float.isNaN(zmax_mean) || Float.isNaN(zmax_std)) {
					System.err.print("\tCalculating per-family max-linkage statistics...");
					idx = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							float val = fisherZtransform(pearsonCorrelation(fx, fy, MAX));
							if (!Float.isNaN(val))	{
								valArray[idx++] = val;
							}
						}
					}
					zmax_mean = mean(valArray, idx);
					zmax_std  = stddev(valArray, idx, zmax_mean);
					System.err.println("done.");
				}
					
				//--for mean linkage
				if (Float.isNaN(zmean_mean) || Float.isNaN(zmean_std)) {
					System.err.print("\tCalculating per-family mean-linkage statistics...");
					idx = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							float val = fisherZtransform(pearsonCorrelation(fx, fy, MEAN));
							if (!Float.isNaN(val))	{
								valArray[idx++] = val;
							}
						}
					}
					zmean_mean = mean(valArray, idx);
					zmean_std  = stddev(valArray, idx, zmean_mean);
					System.err.println("done.");
				}
					
				//--for median linkage
				if (Float.isNaN(zmedian_mean) || Float.isNaN(zmedian_std)) {
					System.err.print("\tCalculating per-family median-linkage statistics...");
					idx = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							float val = fisherZtransform(pearsonCorrelation(fx, fy, MEDIAN));
							if (!Float.isNaN(val))	{
								valArray[idx++] = val;
							}
						}
					}
					zmedian_mean = mean(valArray, idx);
					zmedian_std  = stddev(valArray, idx, zmedian_mean);
					System.err.println("done.");
				}
				
				//--for significant median linkage
				if (Float.isNaN(zsigmed_mean) || Float.isNaN(zsigmed_std)) {
					System.err.print("\tCalculating per-family significant median-linkage statistics...");
					idx = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							Vector<Float> corrs = allPearsonCorrelations(fx, fy);
							Vector<Float> sigs = new Vector<Float>();
							for (int z=0; z<corrs.size(); z++) {
								if (correlationSignificance(corrs.get(z), PEARSON) < ALPHA) {
									sigs.add(corrs.get(z));
								}
							}
							float val = Float.NaN;
							if (sigs.size() > 0) {
								val = median(sigs);
							}
							else {
								val = median(corrs);
							}
							if (!Float.isNaN(val))	{
								valArray[idx++] = val;
							}
						}
					}
					zsigmed_mean = mean(valArray, idx);
					zsigmed_std  = stddev(valArray, idx, zsigmed_mean);
					System.err.println("done.");
				}
			}//end if usingOrthology
		}
		catch (OutOfMemoryError e) {
			System.err.println("WARNING: Unable to allocate enough memory to store all pairwise distances");
			System.err.println("         Reverting to slower method that consumes less RAM.");
			recalculateMemoryLimitedStats();
		}
		
		//Write out the results to file
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(statFileName));
			bw.write("All\t" + z_mean + "\t" + z_std + "\n");
			bw.write("Min\t" + zmin_mean + "\t" + zmin_std + "\n");
			bw.write("Max\t" + zmax_mean + "\t" + zmax_std + "\n");
			bw.write("Mean\t" + zmean_mean + "\t" + zmean_std + "\n");
			bw.write("Median\t" + zmedian_mean + "\t" + zmedian_std + "\n");
			bw.write("SigMed\t" + zsigmed_mean + "\t" + zsigmed_std + "\n");
			bw.flush();
			bw.close();
		}
		catch (IOException e) {
			System.err.println("WARNING: Problem writing out stats file: " + statFileName);
			System.err.println(e.getMessage());
		}
	}
	
	public void recalculateMemoryLimitedStats () {
		Vector<Gene> geneVec = new Vector<Gene>(data.keySet());
		
		//Find the mean/stddev of all gene-wise pairs
		if (Float.isNaN(z_mean) || Float.isNaN(z_std)) {
			System.err.print("\tCalculating per-gene zScore statistics...");
			int count = 0;
			float sum = 0;
			for (int i=0; i<geneVec.size(); i++) {
				Gene x = geneVec.get(i);
				for (int j=i+1; j<geneVec.size(); j++) {
					Gene y = geneVec.get(j);
					float val = fisherZtransform(pearsonCorrelation(x, y));
					if (!Float.isNaN(val))	{
						sum += val;
						count++;
					}
				}
			}
			if (count == 0) {
				z_mean = Float.NaN;
				z_std = Float.NaN;
			}
			else {
				z_mean = sum / (float)count;
			
				count = 0;
				sum = 0;
				for (int i=0; i<geneVec.size(); i++) {
					Gene x = geneVec.get(i);
					for (int j=i+1; j<geneVec.size(); j++) {
						Gene y = geneVec.get(j);
						float val = fisherZtransform(pearsonCorrelation(x, y));
						if (!Float.isNaN(val))	{
							val = val - z_mean;
							val = val * val;
							sum += val;
							count++;
						}
					}
				}
				if (count == 0)
					z_std = Float.NaN;
				else
					z_std = (float)Math.sqrt(sum / (float)count);
			}
			System.err.println("done.");
		}
		
		//Then find the mean/stddev for all family-wise pairs
		if (org.usingOrthology) {
			Vector<GeneFamily> familyVec = org.getGeneFamilies();
			
			//--for min linkage
			if (Float.isNaN(zmin_mean) || Float.isNaN(zmin_std)) {
				System.err.print("\tCalculating per-family min-linkage statistics...");
				int count = 0;
				float sum = 0;
				for (int i=0; i<familyVec.size(); i++) {
					GeneFamily fx = familyVec.get(i);
					for (int j=i+1; j<familyVec.size(); j++) {
						GeneFamily fy = familyVec.get(j);
						float val = fisherZtransform(pearsonCorrelation(fx, fy, MIN));
						if (!Float.isNaN(val))	{
							sum += val;
							count++;
						}
					}
				}
				if (count == 0) {
					zmin_mean = Float.NaN;
					zmin_std = Float.NaN;
				}
				else {
					zmin_mean = sum / (float)count;
					
					count = 0;
					sum = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							float val = fisherZtransform(pearsonCorrelation(fx, fy, MIN));
							if (!Float.isNaN(val))	{
								val = val - zmin_mean;
								val = val * val;
								sum += val;
								count++;
							}
						}
					}
					if (count == 0)
						zmin_std = Float.NaN;
					else
						zmin_std = (float)Math.sqrt(sum / (float)count);
				}
				System.err.println("done.");
			}
			
			//--for max linkage
			if (Float.isNaN(zmax_mean) || Float.isNaN(zmax_std)) {
				System.err.print("\tCalculating per-family max-linkage statistics...");
				int count = 0;
				float sum = 0;
				for (int i=0; i<familyVec.size(); i++) {
					GeneFamily fx = familyVec.get(i);
					for (int j=i+1; j<familyVec.size(); j++) {
						GeneFamily fy = familyVec.get(j);
						float val = fisherZtransform(pearsonCorrelation(fx, fy, MAX));
						if (!Float.isNaN(val))	{
							sum += val;
							count++;
						}
					}
				}
				if (count == 0) {
					zmax_mean = Float.NaN;
					zmax_std = Float.NaN;
				}
				else {
					zmax_mean = sum / (float)count;
					
					count = 0;
					sum = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							float val = fisherZtransform(pearsonCorrelation(fx, fy, MAX));
							if (!Float.isNaN(val))	{
								val = val - zmax_mean;
								val = val * val;
								sum += val;
								count++;
							}
						}
					}
					if (count == 0)
						zmax_std = Float.NaN;
					else
						zmax_std = (float)Math.sqrt(sum / (float)count);
				}
				System.err.println("done.");
			}
				
			//--for mean linkage
			if (Float.isNaN(zmean_mean) || Float.isNaN(zmean_std)) {
				System.err.print("\tCalculating per-family mean-linkage statistics...");
				int count = 0;
				float sum = 0;
				for (int i=0; i<familyVec.size(); i++) {
					GeneFamily fx = familyVec.get(i);
					for (int j=i+1; j<familyVec.size(); j++) {
						GeneFamily fy = familyVec.get(j);
						float val = fisherZtransform(pearsonCorrelation(fx, fy, MEAN));
						if (!Float.isNaN(val))	{
							sum += val;
							count++;
						}
					}
				}
				if (count == 0) {
					zmean_mean = Float.NaN;
					zmean_std = Float.NaN;
				}
				else {
					zmean_mean = sum / (float)count;
					
					count = 0;
					sum = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							float val = fisherZtransform(pearsonCorrelation(fx, fy, MEAN));
							if (!Float.isNaN(val))	{
								val = val - zmean_mean;
								val = val * val;
								sum += val;
								count++;
							}
						}
					}
					if (count == 0)
						zmean_std = Float.NaN;
					else
						zmean_std = (float)Math.sqrt(sum / (float)count);
				}
				System.err.println("done.");
			}
				
			//--for median linkage
			if (Float.isNaN(zmedian_mean) || Float.isNaN(zmedian_std)) {
				System.err.print("\tCalculating per-family median-linkage statistics...");
				int count = 0;
				float sum = 0;
				for (int i=0; i<familyVec.size(); i++) {
					GeneFamily fx = familyVec.get(i);
					for (int j=i+1; j<familyVec.size(); j++) {
						GeneFamily fy = familyVec.get(j);
						float val = fisherZtransform(pearsonCorrelation(fx, fy, MEDIAN));
						if (!Float.isNaN(val))	{
							sum += val;
							count++;
						}
					}
				}
				if (count == 0) {
					zmedian_mean = Float.NaN;
					zmedian_std = Float.NaN;
				}
				else {
					zmedian_mean = sum / (float)count;
					
					count = 0;
					sum = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							float val = fisherZtransform(pearsonCorrelation(fx, fy, MEDIAN));
							if (!Float.isNaN(val))	{
								val = val - zmedian_mean;
								val = val * val;
								sum += val;
								count++;
							}
						}
					}
					if (count == 0)
						zmedian_std = Float.NaN;
					else
						zmedian_std = (float)Math.sqrt(sum / (float)count);
				}
				System.err.println("done.");
			}
			
			//--for significant median linkage
			if (Float.isNaN(zsigmed_mean) || Float.isNaN(zsigmed_std)) {
				System.err.print("\tCalculating per-family significant median-linkage statistics...");
				int count = 0;
				float sum = 0;
				for (int i=0; i<familyVec.size(); i++) {
					GeneFamily fx = familyVec.get(i);
					for (int j=i+1; j<familyVec.size(); j++) {
						GeneFamily fy = familyVec.get(j);
						Vector<Float> corrs = allPearsonCorrelations(fx, fy);
						Vector<Float> sigs = new Vector<Float>();
						for (int z=0; z<corrs.size(); z++) {
							if (correlationSignificance(corrs.get(z), PEARSON) < ALPHA) {
								sigs.add(corrs.get(z));
							}
						}
						float val = Float.NaN;
						if (sigs.size() > 0) {
							val = median(sigs);
						}
						else {
							val = median(corrs);
						}
						if (!Float.isNaN(val))	{
							sum += val;
							count++;
						}
					}
				}
				if (count == 0) {
					zsigmed_mean = Float.NaN;
					zsigmed_std = Float.NaN;
				}
				else {
					zsigmed_mean = sum / (float)count;
					
					count = 0;
					sum = 0;
					for (int i=0; i<familyVec.size(); i++) {
						GeneFamily fx = familyVec.get(i);
						for (int j=i+1; j<familyVec.size(); j++) {
							GeneFamily fy = familyVec.get(j);
							Vector<Float> corrs = allPearsonCorrelations(fx, fy);
							Vector<Float> sigs = new Vector<Float>();
							for (int z=0; z<corrs.size(); z++) {
								if (correlationSignificance(corrs.get(z), PEARSON) < ALPHA) {
									sigs.add(corrs.get(z));
								}
							}
							float val = Float.NaN;
							if (sigs.size() > 0) {
								val = median(sigs);
							}
							else {
								val = median(corrs);
							}
							if (!Float.isNaN(val))	{
								val = val - zsigmed_mean;
								val = val * val;
								sum += val;
								count++;
							}
						}
					}
					
					if (count == 0)
						zsigmed_std = Float.NaN;
					else
						zsigmed_std = (float)Math.sqrt(sum / (float)count);
				}
				System.err.println("done.");
			}
		}//end if usingOrthology
	}
	
	
	public float median (Vector<Float> vec) {
		if (vec.size() == 0)	return Float.NaN;
		if (vec.size() == 1)	return vec.get(0);
		Float[] array = new Float[vec.size()];
		array = vec.toArray(array);
		Arrays.sort(array);
		return array[array.length / 2];
	}
	
	public float median (float[] array) {
		if (array.length == 0)	return Float.NaN;
		if (array.length == 1)	return array[0];
		Arrays.sort(array);
		return array[array.length / 2];
	}
	
	public float mean (Vector<Float> vec) {
		float sum = 0;
		int count = 0;
		for (int i=0; i<vec.size(); i++) {
			float val = vec.get(i);
			if (!Float.isNaN(val)) {
				sum += val;
				count++;
			}
		}
		if (count == 0) return Float.NaN;
		return sum / (float)count;
	}
	
	public float mean (float[] array, int sz) {
		float sum = 0;
		int count = 0;
		for (int i=0; i<sz; i++) {
			if (!Float.isNaN(array[i])) {
				sum += array[i];
				count++;
			}
		}
		if (count == 0) return Float.NaN;
		return sum / (float)count;
	}
	
	public float stddev (Vector<Float> vec, float mean) {
		float sum = 0;
		int count = 0;
		for (int i=0; i<vec.size(); i++) {
			float val = vec.get(i);
			if (!Float.isNaN(val)) {
				val = val - mean;
				val = val * val;
				sum += val;
				count++;
			}
		}
		if (count == 0) return Float.NaN;
		return (float)Math.sqrt(sum / (float)count);
	}
	
	public float stddev (Vector<Float> vec) {
		return stddev(vec, mean(vec));
	}
	
	public float stddev (float[] array, int sz, float mean) {
		float sum = 0;
		int count = 0;
		for (int i=0; i<sz; i++) {
			float val = array[i];
			if (!Float.isNaN(val)) {
				val = val - mean;
				val = val * val;
				sum += val;
				count++;
			}
		}
		if (count == 0) return Float.NaN;
		return (float)Math.sqrt(sum / (float)count);
	}
	
	public float stddev (float[] array, int sz) {
		return stddev(array, sz, mean(array, sz));
	}
	
	public float fisherZtransform (float corr) {
		if (Float.isNaN(corr))
			return corr;
		if (corr <= -1)
			corr = -0.999999f;
		else if (corr >= 1) {
			corr =  0.999999f;
		}
		return (float)(0.5 * (Math.log((1 + corr) / (1 - corr))));
	}
	
	public float score(Gene gx, Gene gy, int distType) {
		if (distType == PEARSON)	return pearsonCorrelation(gx, gy);
		if (distType == ZSCORE)		return zscoreCorrelation(gx, gy);
		if (distType == ZSCORE2) {
			float val = zscoreCorrelation(gx, gy);
			if (Float.isNaN(val))	return val;
			if (val < 1)			return 0f;
			else					return val*val;
		}
		return Float.NaN;
	}
	
	public float score(GeneFamily gfx, GeneFamily gfy, int distType, int linkType) {
		if (distType == PEARSON)	return pearsonCorrelation(gfx, gfy, linkType);
		if (distType == ZSCORE)		return zscoreCorrelation(gfx, gfy, linkType);
		if (distType == ZSCORE2) {
			float val = zscoreCorrelation(gfx, gfy, linkType);
			if (Float.isNaN(val))	return val;
			if (val < 1)			return 0f;
			else					return val*val;
		}
		return Float.NaN;
	}
	
	public float score(Gene gx, GeneFamily gfy, int distType, int linkType) {
		if (distType == PEARSON)	return pearsonCorrelation(gx, gfy, linkType);
		if (distType == ZSCORE)		return zscoreCorrelation(gx, gfy, linkType);
		if (distType == ZSCORE2) {
			float val = zscoreCorrelation(gx, gfy, linkType);
			if (Float.isNaN(val))	return val;
			if (val < 1)			return 0f;
			else					return val*val;
		}
		return Float.NaN;
	}
	
	public float score(GeneFamily gfx, Gene gy, int distType, int linkType) {
		return score(gy, gfx, distType, linkType);
	}
	
	public float zscoreCorrelation(Gene gx, Gene gy) {
		return (fisherZtransform(pearsonCorrelation(gx, gy)) - z_mean) / z_std;
	}
	
	public float zscoreCorrelation(GeneFamily gfx, GeneFamily gfy, int type) {
		Vector<Float> corrs = new Vector<Float>();
		//For each pair of genes...
		for (Iterator<Gene> x_it = gfx.genes.iterator(); x_it.hasNext(); ) {
			Gene gx = x_it.next();
			for (Iterator<Gene> y_it = gfy.genes.iterator(); y_it.hasNext(); ) {
				Gene gy = y_it.next();
				//Calculate the correlation for this pair
				float corr = fisherZtransform(pearsonCorrelation(gx, gy));
				//Add it to the vector if valid
				if (!Float.isNaN(corr))	corrs.add(corr);
			}
		}
		//Return the desired linkage correlation
		return linkageZscore(corrs,type);
	}
	
	public float zscoreCorrelation(Gene gx, GeneFamily gfy, int type) {
		Vector<Float> corrs = new Vector<Float>();
		//For each gene in the family...
		for (Iterator<Gene> y_it = gfy.genes.iterator(); y_it.hasNext(); ) {
			Gene gy = y_it.next();
			//Calculate the correlation and store it if it's valid
			float corr = fisherZtransform(pearsonCorrelation(gx, gy));
			if (!Float.isNaN(corr))	corrs.add(corr);
		}
		//Return the desired linkage correlation
		return linkageZscore(corrs, type);
	}
	
	public float zscoreCorrelation(GeneFamily gfx, Gene gy, int type) {
		return zscoreCorrelation(gy,gfx,type);
	}
	
	public float correlationSignificance (float corr, int distType) {
		//If the distance type was ZSCORE, need to undo the atanh to check significance
		if (distType == ZSCORE) {
			corr = (float)Math.tanh(corr);
		}
		//Extreme values cause the math to blow up...
		if (corr <= -1)		corr = -0.999999f;
		else if (corr >= 1) corr =  0.999999f;
		
		int df = conds.size() - 2;
		float tval = (float)(corr * Math.sqrt(df) / Math.sqrt(1 - corr*corr));
		if (tval <= 0) {
			return 1;
		}
		try {
			return (float)(1 - Probability.studentT(tval, df));
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
			System.err.println("ACK!: corr = " + corr);
			System.err.println("        df = " + df);
			System.err.println("      tval = " + tval);
			System.exit(0);
		}
		return 0f;
	}
	
	
	public float pearsonCorrelation(Gene gx, Gene gy) {
		Vector<Float> x = data.get(gx);
		Vector<Float> y = data.get(gy);
		if ((x == null) || (y == null)) {
			return Float.NaN;
		}
		// Using the formula:
		//                    Sum(XY) - Sum(X)Sum(Y)/N
		// r = ------------------------------------------------------
		//     Sqrt( (Sum(X^2) - Sum(X)^2/N) (Sum(Y^2) - Sum(Y)^2/N)
		float sumx = 0;		float sumy = 0;		int count = 0;
		float sumxx = 0;	float sumyy = 0;	float sumxy = 0;
		// Calculate the individual terms
		for (int i = 0; i < x.size(); i++) {
			float xi = x.get(i);
			float yi = y.get(i);
			if (!Float.isNaN(xi) && !Float.isNaN(yi)) {
				sumx += xi;			sumy += yi;			count++;
				sumxx += xi * xi;	sumyy += yi * yi;	sumxy += xi * yi;	
			}
		}
		if (count < 3) { //You must have at least 3 measurements in common to correlate
			return Float.NaN;
		}		
		float rho = (float)((sumxy - (sumx * sumy) / count)
						/ Math.sqrt(  (sumxx - (sumx * sumx) / count)
									* (sumyy - (sumy * sumy) / count) ) );
		if (rho > 1.0) return 1.0f;
		else if (rho < -1.0) return -1.0f;
		return rho;
	}

	public Vector<Float> allPearsonCorrelations (GeneFamily gfx, GeneFamily gfy) {
		Vector<Float> corrs = new Vector<Float>();
		//For each pair of genes...
		for (Iterator<Gene> x_it = gfx.genes.iterator(); x_it.hasNext(); ) {
			Gene gx = x_it.next();
			for (Iterator<Gene> y_it = gfy.genes.iterator(); y_it.hasNext(); ) {
				Gene gy = y_it.next();
				//Calculate the correlation for this pair
				float corr = pearsonCorrelation(gx, gy);
				//Add it to the vector if valid
				if (!Float.isNaN(corr))	corrs.add(corr);
			}
		}
		return corrs;
	}
	
	public float pearsonCorrelation(GeneFamily gfx, GeneFamily gfy, int type) {
		//Return the desired linkage correlation
		return linkageValue(allPearsonCorrelations(gfx, gfy), type, PEARSON);
	}
	
	public Vector<Float> allPearsonCorrelations (Gene gx, GeneFamily gfy) {
		Vector<Float> corrs = new Vector<Float>();
		//For each gene in the family...
		for (Iterator<Gene> y_it = gfy.genes.iterator(); y_it.hasNext(); ) {
			Gene gy = y_it.next();
			//Calculate the correlation and store it if it's valid
			float corr = pearsonCorrelation(gx, gy);
			if (!Float.isNaN(corr))	corrs.add(corr);
		}
		return corrs;
	}
	
	public float pearsonCorrelation(Gene gx, GeneFamily gfy, int type) {
		//Return the desired linkage correlation
		return linkageValue(allPearsonCorrelations(gx, gfy), type, PEARSON);
	}
	
	public float pearsonCorrelation(GeneFamily gfx, Gene gy, int type) {
		return pearsonCorrelation(gy,gfx,type);
	}
	
	public float linkageValue(Vector<Float> vec, int type, int distType) {
		//If the vector is empty, return NaN
		if (vec.size() == 0)	return Float.NaN;
		//If the vector has one value, return it
		if (vec.size() == 1) 	return vec.get(0);
		//If MEAN is desired, calculate it...
		if (type == MEAN) {
			return mean(vec);
		}
		//If SIGNIFICANT_MEDIAN, median only those values that are significant
		if (type == SIGNIFICANT_MEDIAN) {
			Vector<Float> sigs = new Vector<Float>();
			for (int i=0; i<vec.size(); i++) {
				if (correlationSignificance(vec.get(i), distType) < ALPHA) {
					sigs.add(vec.get(i));
				}
			}
			if (sigs.size() == 0) { //If none are significant, median all
				type = MEDIAN;
			}
			else if (sigs.size() == 1) {  //If just one, return it
				return sigs.get(0);
			}
			else {  //If multiple, take the median of just those significant
				vec = sigs;
				type = MEDIAN;
			}
		}
		//Otherwise, for MIN, MAX, MEDIAN, sort the array
		Float[] arr = new Float [vec.size()];
		arr = (Float[])vec.toArray(arr);
		Arrays.sort(arr);
		if (type == MIN)	return arr[0];
		if (type == MAX)	return arr[arr.length - 1];
		if (type == MEDIAN)	return arr[arr.length / 2];
		//This should not occur, but return NaN if the type was invalid
		return Float.NaN;
	}
	
	public float linkageZscore(Vector<Float> vec, int type) {
		float val = linkageValue(vec,type,ZSCORE);
		if (type == MIN)	return (val - zmin_mean) / zmin_std;
		if (type == MAX)	return (val - zmax_mean) / zmax_std;
		if (type == MEAN)	return (val - zmean_mean) / zmean_std;
		if (type == MEDIAN)	return (val - zmedian_mean) / zmedian_std;
		if (type == SIGNIFICANT_MEDIAN)	return (val - zsigmed_mean) / zsigmed_std;
		return val;
	}
	
}
