import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * 
 * @author mhibbs
 *
 * Class contains all information related to a specific organism, including it's name, genes, gene sets/families,
 * and possibly annotations.
 *
 */
public class Organism {

	public String								name;
	public HashMap<Integer,Gene>				id_to_gene;
	public HashMap<String,Gene> 				genes;
	public Set<GeneFamily>						families;
	public HashMap<Integer,ExpressionDataset>	id_to_dataset;
	public Vector<ExpressionDataset>			datasets;
	
	public boolean								usingOrthology;
	
	/**
	 * Default constructor
	 */
	public Organism () {
		name = null;
		id_to_gene = new HashMap<Integer,Gene>();
		genes = new HashMap<String,Gene>();
		families = new HashSet<GeneFamily>();
		id_to_dataset = new HashMap<Integer,ExpressionDataset>();
		datasets = new Vector<ExpressionDataset>();
		usingOrthology = false;
	}
	
	/**
	 * Basic Constructor - initializes valid gene set, but not gene family set
	 * 
	 * @param id Three letter string ID of organism (e.g. HSA, SCE, ATH, etc.)
	 * @param validGeneFileName Path and filename of file containing all valid, unique gene names (1 per line)
	 */
	public Organism(String id, String validGeneFileName) {
		this();
		name = id;
		
		//Load all valid file names
		try {
			BufferedReader br = new BufferedReader(new FileReader(validGeneFileName));
			String line;
			while (((line = br.readLine()) != null) && (line != "")) {
				String[] parts = line.split("\t");
				int gid = Integer.parseInt(parts[0]);
				Gene g = new Gene(this, parts[1], gid);
				genes.put(parts[1], g);
				id_to_gene.put(gid, g);
			}
		}
		catch (NumberFormatException e) {
			System.err.println("ERROR parsing validGeneFileName: " + validGeneFileName);
			System.err.println("\tcontained an invalid gene id");
			System.err.println(e.getLocalizedMessage());
			System.exit(0);
		}
		catch (IOException e) {
			System.err.println("ERROR reading validGeneFileName: " + validGeneFileName);
			System.err.println(e.getLocalizedMessage());
			System.exit(0);
		}
	}
	
	public Vector<Gene> getGeneVector () {
		Set<Gene> geneSet = new HashSet<Gene>();
		geneSet.addAll(genes.values());
		return new Vector<Gene>(geneSet);
	}
	
	public Vector<GeneFamily> getGeneFamilies () {
		return new Vector<GeneFamily> (families);
	}
	
	public void loadDatasets (String datasetFileList, String pathToDatasets) {
		String filename = "";
		//Load all datasets in the file
		try {
			BufferedReader br = new BufferedReader(new FileReader(datasetFileList));
			String line;
			while (((line = br.readLine()) != null) && (!line.equals(""))) {
				String[] parts = line.split("\t");
				filename = pathToDatasets + parts[1];
				int id = Integer.parseInt(parts[0]);
				System.err.print("Loading dataset " + line + "...");
				ExpressionDataset dset = new ExpressionDataset(this, filename, id); 
				datasets.add(dset);
				id_to_dataset.put(id, dset);
				System.err.println("done.");
			}
		}
		catch (IOException e) {
			System.err.println("ERROR: Unable to load dataset: " + filename);
		}
	}
	
}
