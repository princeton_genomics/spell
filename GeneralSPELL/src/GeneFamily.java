import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * 
 * @author mhibbs
 *
 * Contains information about a gene family, including which genes belong to this family.
 *
 */
public class GeneFamily {

	public int			id;
	public Set<Gene>	genes;
	
	
	/**
	 * Default constructor
	 * Makes an empty GeneFamily
	 */
	public GeneFamily () {
		id = -1;
		genes = new HashSet<Gene> ();
	}
	
	public GeneFamily (int i) {
		id = i;
		genes = new HashSet<Gene> ();
	}
	
	public String toString() {
		if (genes.size() > 1) {
			String val = "{";
			for (Iterator<Gene> it = genes.iterator(); it.hasNext(); ) {
				Gene g = it.next();
				val += g.name + ",";
			}
			return val.substring(0, val.length() - 1) + "}";
		}
		else if (genes.size() == 1) {
			return genes.iterator().next().name;
		}
		else {
			return "{}";
		}
	}
	
}
