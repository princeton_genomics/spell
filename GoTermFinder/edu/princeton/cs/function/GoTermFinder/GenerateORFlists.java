package edu.princeton.cs.function.GoTermFinder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class GenerateORFlists {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 4) {
			System.out.println("Usage for GenerateORFLists (Specific to Yeast normal ORFs):");
			System.out.println("GoServer <OntologyFile> <AssociationFile> <GOidList> <ResultPath>");
			System.out.println("\tOntologyFile - The .obo file containing the GO");
			System.out.println("\tAssociationFile - For yeast, gene_associations.sgd");
			System.out.println("\tGOidList -  list of GO ids to get ORFs for");
			System.out.println("\tResultPath -  Path to put resulting files");
			System.exit(0);
		}
		
		Ontology GO = new Ontology(args[0]);
		GO.addAnnotations(args[1],"P");
		
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(args[2]));
			String line = br.readLine();
			while (line != null) {
				//Find the term
				line = line.replace('_',':');
				GoTerm term = (GoTerm)GO.terms.get(line);
				//System.out.println("Processing line: " + line);
				line = line.replace(':','_');
				BufferedWriter fout = new BufferedWriter(new FileWriter(args[3] + line + ".txt"));
				if (term != null) {
				Iterator<Gene> it = term.genes.iterator();
					while (it.hasNext()) {
						Gene g = it.next();
						Iterator<String> it2 = g.aliases.iterator();
						while (it2.hasNext()) {
							String al = it2.next();
							if ((al.charAt(0) == 'Y') && (al.length() > 5)) {
								fout.write(al + "\n");
							}
						}
					}
				}
				else {
					System.out.println("Term not found: " + line);
				}
				fout.close();
				
				line = br.readLine();
			}
			br.close();
		}
		catch (IOException e) {
			System.out.println("A problem occured reading " + args[2]);
		}

	}

}
