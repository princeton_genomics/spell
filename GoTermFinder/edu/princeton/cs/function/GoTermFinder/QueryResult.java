/*
 * Created on May 27, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.princeton.cs.function.GoTermFinder;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author mhibbs
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class QueryResult implements Comparable<QueryResult> {

	public GoTerm 		term;
	public ArrayList<String> 	queryGenesInTerm;
	public double 	pvalue;
	public int 		totalGenes;
	public int		totalGenesInTerm;
	public int		numQueryGenes;
	public int		numQueryGenesInTerm;	
	
	public QueryResult (GoTerm _term, int totGenes, int totQueryGenes) {
		term = _term;
		totalGenes = totGenes;
		totalGenesInTerm = term.totalAnnotations;
		numQueryGenes = totQueryGenes;
		numQueryGenesInTerm = 0;
		queryGenesInTerm = new ArrayList<String>();
	}
	
	public int compareTo (QueryResult qr) {
		if (pvalue < qr.pvalue)
			return -1;
		else if (pvalue > qr.pvalue)
			return 1;
		else
			return 0;
	}
	
	public String toString() {
		String out = term.id + "\t" + term.name + "\t" + term.namespace + "\t" + pvalue + "\t" + numQueryGenesInTerm +
		             "\t" + numQueryGenes + "\t" + totalGenesInTerm + "\t" + totalGenes + "\t";
		Iterator<String> it = queryGenesInTerm.iterator();
		if (it.hasNext()) {
			out += it.next();
		}
		while (it.hasNext()) {
			out += "," + it.next();
		}
		
		return out;					 
	}
	
}
