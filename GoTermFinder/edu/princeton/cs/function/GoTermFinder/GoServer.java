/*
 * Created on May 31, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.princeton.cs.function.GoTermFinder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * @author mhibbs
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class GoServer extends Thread {

	private Ontology GO;
	private ServerSocket inputSocket;
	
	
	public GoServer (String oboName, String annName, String aspect, String socket) {
		
		System.out.print("Loading ontology file...");
		GO = new Ontology(oboName);
		System.out.println("done");
		
		System.out.print("Loading associations file...");
		int numAnn = GO.addAnnotations(annName,aspect);
		System.out.println("done with " + numAnn + " annotations");		

		System.out.print("Creating server socket...");
		try {
			inputSocket = new ServerSocket(Integer.parseInt(socket),50); 
		}
		catch(IOException e) {
			System.err.println("Unable to create Server Socket.  Terminating execution.");
			System.exit(0);
		}
		System.out.println("done.");
	}
	
	public void run() {
		try {
			inputSocket.setSoTimeout(0);
		}
		catch (SocketException e) {
			System.err.println("Unable to set Server Socket timeout.  Terminating execution.");
			System.exit(0);
		}
		
		while (true) {
			Socket connection = null;
			BufferedReader br = null;
			PrintWriter out   = null;
			try {
				//System.out.println("Waiting for connection...");
				connection = inputSocket.accept();
				
				System.out.print("Established connection...");
				
				String queryGeneList = "";
				br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				
				double pvalCutoff = Double.parseDouble(br.readLine());
				System.out.print("p-val cutoff = " + pvalCutoff + "...");
				
				queryGeneList += br.readLine();
							
				System.out.print("query genes are " + queryGeneList + "...");
				
				String[] queryGenes = queryGeneList.split(",");
				QueryResult[] results = GO.goTermFind(queryGenes,pvalCutoff);
				
				System.out.print("sending results.");
				
				out = new PrintWriter(connection.getOutputStream(),false);
				out.println(results.length);
				
				for (int i=0; i<results.length; i++) {
					out.println(results[i]);
				}
				out.flush();
				
				out.close();
				br.close();
				connection.close();
			}
			catch (NumberFormatException e) {
				System.err.println("NumberFormatException Error.  Closing connection.");
				if (out != null)	out.close();
				try {
					if (br  != null)	br.close();
					if (connection != null) 	connection.close();
				}
				catch (IOException ex) {
					System.err.println("\tIOException when closing socket.");
				}
				
			}
			catch (Exception e) {
				System.err.println("Socket Error. Terminating execution.");
				System.err.println(e.getMessage());
				e.printStackTrace();
				System.exit(0);
			}
		}
	}
	
	
	public static void usage() {
		System.out.println("Usage for GoServer:");
		System.out.println("GoServer <OntologyFile> <AssociationFile> <aspect> <socket>");
		System.out.println("\tOntologyFile - The .obo file containing the GO");
		System.out.println("\tAssociationFile - For yeast, gene_associations.sgd");
		System.out.println("\taspect -  P - biological process, F - molecular function,");
		System.out.println("\t\tC - cellular component, X - load all branches");
		System.out.println("\tsocket -  TCP/IP server socket");
	}
	
	public static void main(String[] args) {
		if (args.length != 4) {
			usage();
			System.exit(0);
		}
		
		GoServer server = new GoServer (args[0],args[1],args[2],args[3]);
		System.out.println("Ready for connections.");
		server.start();
		
	}
}
