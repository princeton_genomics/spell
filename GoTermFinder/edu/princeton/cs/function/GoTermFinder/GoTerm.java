/*
 * Created on May 25, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.princeton.cs.function.GoTermFinder;

import java.util.HashSet;
import java.util.Iterator;

/**
 * @author mhibbs
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class GoTerm implements Comparable<GoTerm> {

	public String id;
	public String name;
	public String namespace;
	public HashSet<GoTerm> parents;
	public HashSet<GoTerm> children;
	public HashSet<Gene> genes;
	public HashSet<Gene> utilSet;
	public int numDirectAnnotations;
	public int totalAnnotations;
	public int queryAnnotations;
	public int counter;
	
	public GoTerm(String _id) {
		id = _id;
		totalAnnotations = 0;
		queryAnnotations = 0;
		numDirectAnnotations = 0;
		counter = 0;
		parents = new HashSet<GoTerm>();
		children = new HashSet<GoTerm>();
		genes = new HashSet<Gene>();
		utilSet = new HashSet<Gene>();
	}
	
	public boolean isAnnotated (Gene g) {
		return genes.contains(g);
	}
	
	public boolean isAnnotatedToChild(Gene g) {
		boolean result = false;
		Iterator<GoTerm> it = children.iterator();
		while (it.hasNext()) {
			GoTerm child = it.next();
			result |= child.isAnnotated(g);
		}
		return result;
	}
	
	public boolean annotate(Gene g) {
		if (!genes.contains(g)) {
			genes.add(g);
			g.annotations.add(this);
			totalAnnotations++;
		
			Iterator<GoTerm> it = parents.iterator();
			while (it.hasNext()) {
				GoTerm p = it.next();
				p.annotate(g);
			}
			return true;
		}
		return false;
	}
	
	public int compareTo (GoTerm other) {
		return id.compareTo(other.id);
	}
	
	public int sizeOfSmallestCommonAncestor(Gene g) {
		//If the gene is annotated to this term, return the terms size
		if (genes.contains(g)) {
			return totalAnnotations;
		}
		//Otherwise, examine the parents
		int sz = Integer.MAX_VALUE;
		Iterator<GoTerm> it = parents.iterator();
		while (it.hasNext()) {
			GoTerm parent = it.next();
			int tempSz = parent.sizeOfSmallestCommonAncestor(g);
			if (tempSz < sz) {
				sz = tempSz;
			}
		}
		return sz;
	}
	
	public int sizeOfLargestChild() {
		int sz = 0;
		Iterator<GoTerm> it = children.iterator();
		while (it.hasNext()) {
			GoTerm term = it.next();
			sz = Math.max(sz,term.totalAnnotations);
		}
		return sz;
	}
	
	public int sizeOfSmallestChild() {
		int sz = Integer.MAX_VALUE;
		if (children.size() == 0)
			return 0;
		else {
			Iterator<GoTerm> it = children.iterator();
			while (it.hasNext()) {
				GoTerm term = it.next();
				sz = Math.min(sz, term.totalAnnotations);
			}
			return sz;
		}
	}

}
