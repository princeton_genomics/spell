/*
 * Created on May 28, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.princeton.cs.function.GoTermFinder;

/**
 * @author mhibbs
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FindGoTerms {

	public static void usage() {
		System.out.println("Usage for FindGoTerms:");
		System.out.println("FindGoTerms <OntologyFile> <AssociationFile> <aspect> <numResults> <queryGenes>");
		System.out.println("\tOntologyFile - The .obo file containing the GO");
		System.out.println("\tAssociationFile - For yeast, gene_associations.sgd");
		System.out.println("\taspect - Either P, F, C, or X for all three.  The branch of GO to search through");
		System.out.println("\tp-value cutoff - (e.g. 0.05)");
		System.out.println("\tqueryGenes - Gene names in the query, separated by a \",\"");
	}
	
	public static void main(String[] args) {
		if (args.length != 5) {
			usage();
			System.exit(0);
		}
		
		double pval = Double.parseDouble(args[3]);
		String[] queryGenes = args[4].split(",");
		
		System.out.print("Loading ontology file...");
		Ontology GO = new Ontology(args[0]);
		System.out.println("done");
		
		System.out.print("Loading associations file...");
		int numAnn = GO.addAnnotations(args[1],args[2]);
		System.out.println("done with " + numAnn + " annotations");
		
		System.out.print("Running query...");
		QueryResult[] results = GO.goTermFind(queryGenes,pval);
		System.out.println("done with " + results.length + " results:");
		
		for (int i=0; i<results.length; i++) {
			System.out.println(results[i]);
		}
		
		//System.out.println(Double.MIN_VALUE);
		//System.out.println(Math.exp(-40000));
		
	}
}
