package edu.princeton.cs.function.GoTermFinder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class MarkupGeneNames {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("Usage for MarkupGeneNames.jar:");
			System.out.println("java -jar MarkupGeneNames.jar <.obo> <.sgd> <.cdt>");
			System.out.println("\tResult goes to STDOUT");
			System.exit(0);
		}
		
		Ontology GO = new Ontology (args[0]);
		GO.addAnnotations(args[1],"X");
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(args[2]));
			String line = br.readLine();
			System.out.println(line);
			line = br.readLine();
			System.out.println(line);
			line = br.readLine();
			System.out.println(line);
			line = br.readLine();
			while (line != null) {
				String[] parts = line.split("\t");
				Object obj = GO.genes.get(parts[0]);
				if (obj != null) {
					Gene g = (Gene)obj;
					System.out.print(parts[0] + "\t" + g.name);
					if (g.directAnnotations.size() > 0) {
						Iterator<GoTerm> it = g.directAnnotations.iterator();
						while (it.hasNext()) {
							System.out.print(" | " + (it.next()).name);
						}
					}
					for (int i=2; i<parts.length; i++) {
						System.out.print("\t" + parts[i]);
					}
					System.out.println();
				}
				else {
					System.out.println(line);
				}
				line = br.readLine();
			}
		}
		catch (IOException e) {
			System.err.println("Unable to locate file:" + args[2]);
			System.exit(0);
		}
	}
}
