/*
 * Created on May 25, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.princeton.cs.function.GoTermFinder;

import java.util.HashSet;

/**
 * @author mhibbs
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Gene implements Comparable<Gene> {
	
	public HashSet<String> aliases;
	public String name;
	public String systematic;
	public HashSet<GoTerm> annotations;
	public HashSet<GoTerm> directAnnotations;

	public Gene (String n) {
		name = n;
		systematic = name;
		aliases = new HashSet<String>();
		annotations = new HashSet<GoTerm>();
		directAnnotations = new HashSet<GoTerm>();
	}
	
	public Gene (String n, String s) {
		this(n);
		systematic = s;
	}
	
	public void associate (GoTerm term) {
		annotations.add(term);
	}
	
	public int compareTo (Gene other) {
		return name.compareTo(other.name);
	}
	
}
