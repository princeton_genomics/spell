/*
 * Created on May 27, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.princeton.cs.function.GoTermFinder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * @author mhibbs
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Ontology {
	
	public HashMap<String,GoTerm> terms;
	public HashMap<String,Gene> genes;
	public HashSet<Gene>	geneSet;
	public int numGenes;
	private double[] logFacts;
	
	/**
	 * Loads in the Gene Ontology hierarchy from the file name given
	 * @param oboName Name of the .obo file containing the hierarchy
	 */
	public Ontology (String oboName) {
		numGenes = 0;
		terms = new HashMap<String,GoTerm>();
		genes = new HashMap<String,Gene>();
		
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(oboName));
			String line;
			
			while ((line = in.readLine()) != null) {
				//Looking for [TERM] lines...
				if (line.equalsIgnoreCase("[Term]")) {
					//Next line is required to be the id: line
					line = in.readLine();
					String id = line.substring(4);

					//System.out.println("Found term " + id);
					
					//Determine if this id already exists
					GoTerm node = terms.get(id);
					if (node == null) {
						node = new GoTerm(id);
						terms.put(id,node);
					}
					
					//Now get the rest of the info for this term
					while (((line = in.readLine()) != null) && (!line.equals(""))) {
						int idx = line.indexOf(":");
						String tag = line.substring(0,idx);
						if (tag.equalsIgnoreCase("name")) {
							node.name = line.substring(idx+2);
						}
						else if (tag.equalsIgnoreCase("namespace")) {
							node.namespace = line.substring(idx+2);
						}
						else if (tag.equalsIgnoreCase("is_a")) {
							int idx2 = line.indexOf("!");
							String parentID = line.substring(idx+2,idx2-1);
							GoTerm parentNode  = terms.get(parentID);
							if (parentNode == null) {
								parentNode = new GoTerm(parentID);
								terms.put(parentID,parentNode);
							}
							node.parents.add(parentNode);
							parentNode.children.add(node);
						}
						else if (tag.equalsIgnoreCase("relationship")) {
							int idx2 = line.indexOf("!");
							String parentID = line.substring(idx+2,idx2-1);
							idx2 = parentID.indexOf(" ");
							parentID = parentID.substring(idx2+1);
							GoTerm parentNode = terms.get(parentID);
							if (parentNode == null) {
								parentNode = new GoTerm(parentID);
								terms.put(parentID,parentNode);
							}
							node.parents.add(parentNode);
							parentNode.children.add(node);
						}
						else if (tag.equalsIgnoreCase("is_obsolete")) {
							String status = line.substring(idx+2);
							if (status.equalsIgnoreCase("true")) {
								terms.remove(node.id);
							}
						}
					}
				}
			}
		}
		catch(FileNotFoundException e) {
			System.err.println("FileNotFound: " + oboName + ". Program aborted.");
			System.exit(0);
		}
		catch(IOException e) {
			System.err.println("IOException: " + oboName + ". Program aborted.");
			System.exit(0);
		}
	}//end Constructor
	
	/**
	 * Loads an annotation file into the ontology
	 * @param associationName Name of association file
	 * @return int - The number of annotations made
	 */
	
	public int numGenes(){
		return numGenes;
	}
	
	public int addAnnotations (String associationName, String aspect) {
		return addAnnotations(associationName, aspect, new HashSet<String>());
	}
	
	public int addAnnotations (String associationName, String aspect, HashSet<String> excludeCodes) {
		int numAnnotations = 0;
		
		try {
			BufferedReader in = new BufferedReader(new FileReader(associationName));
			String line;
			
			while (((line = in.readLine()) != null)) {
				if ((!line.equals("")) && (!line.substring(0, 1).equals("!"))) {
					String[] parts = line.split("\t");
					/*
					 * Note on association file format:
					 * index/col# - field
					 * 1 - systematic name
					 * 2 - common name
					 * 3 - qualifier (important one is "NOT")
					 * 4 - GO:##### string
					 * 6 - evidence code
					 * 8 - aspect (P, C, or F)
					 * 10 - aliases (not req'd, may contain multiple with |s between)
					 */
					// If annotation is in the right aspect
					//  AND the entry isn't qualified as a "not" entry
					//  AND the evidence code is not being excluded...
					if ((aspect.equalsIgnoreCase("X") || parts[8].equalsIgnoreCase(aspect))
							&& !parts[3].equalsIgnoreCase("NOT")
							&& !excludeCodes.contains(parts[6])) {
						//Upper case all gene names
						parts[1] = parts[1].toUpperCase();
						parts[2] = parts[2].toUpperCase();
						parts[10] = parts[10].toUpperCase();

						//Locate this gene
						Gene g = genes.get(parts[1]);
						if (g == null) {
							g = new Gene(parts[2], parts[1]);
							
							//geneSet = new HashSet(genes.values());
							//numGenes = geneSet.size();
							//System.err.println("NewGene-" + genes.values().size() + " - " + numGenes + "\t" + g.name + " (" + g.systematic + ")");
							
							//Try to store this gene in the name map
							if (!genes.containsKey(parts[2])) {
								genes.put(parts[2], g);
							}
							else { //This systematic or display name is already in use -- reclaim it
								Gene other = genes.get(parts[2]);
								other.aliases.remove(parts[2]);
								genes.put(parts[2], g);
								System.err.println("WARNING: Alias collision: The gene " + other.name + "(" + other.systematic + ")"
										+ " cannot use the alias " + parts[2] + " because it is the display name for " + g.systematic);
							}
							if (!genes.containsKey(parts[1])) {
								genes.put(parts[1], g);
							}
							else {
								Gene other = genes.get(parts[1]);
								other.aliases.remove(parts[1]);
								genes.put(parts[1], g);
								System.err.println("WARNING: Alias collision: The gene " + other.name + "(" + other.systematic + ")"
										+ " cannot use the alias " + parts[1] + " because it is the systematic name for " + g.systematic);
							}
							
							//Add aliases (if any)
							String[] als = parts[10].split("\\|");
							for (int i=0; i<als.length; i++) {
								if ((als[i] != "") && (als[i].length() > 1)) {
									//If the alias does not exist yet, assign it (for now)
									if (!genes.keySet().contains(als[i])) {
										g.aliases.add(als[i]);
										genes.put(als[i], g);
									}
									else {
										//If the existing alias doesn't already point to this gene
										if (genes.get(als[i]) != g) {
											//If the alias matches the display name (or systematic name) for this gene, then override
											if (als[i].equalsIgnoreCase(g.name) || als[i].equalsIgnoreCase(g.systematic)) {
												g.aliases.add(als[i]);
												Gene other = genes.get(als[i]);
												other.aliases.remove(als[i]);
												genes.put(als[i], g);
												System.err.println("WARNING: Alias collision: The gene " + other.name + "(" + other.systematic + ")"
														+ " cannot use the alias " + als[i] + " because it is a name for " + g.systematic);
											}
											else {
												System.err.println("WARNING: Alias collision: The gene " + g.name + "(" + g.systematic + ")"
																 + " cannot use the alias " + als[i] + " because it is already assigned to "
																 + genes.get(als[i]).name + "(" + genes.get(als[i]).systematic + ")");
											}
										}
									}
								}
							}
						}

						//Get the GO term
						GoTerm t = terms.get(parts[4]);
						if (t != null) {
							if (t.annotate(g)) {
								t.numDirectAnnotations++;
								g.directAnnotations.add(t);
								numAnnotations++;
							}
						}
						else {
							System.err.println("GO node " + parts[4] + " was not found, but is present in association.");
						}
					}
				}
			}
		}
		catch(FileNotFoundException e) {
			System.err.println("FileNotFound: " + associationName + ". Program aborted.");
			System.exit(0);
		}
		catch(IOException e) {
			System.err.println("IOException: " + associationName + ". Program aborted.");
			System.exit(0);
		}
		
		geneSet = new HashSet<edu.princeton.cs.function.GoTermFinder.Gene>(genes.values());
		numGenes = geneSet.size();
		
		//Pre-calculate the log factorials
		logFacts = new double [numGenes+1];
		logFacts[0] = 0;
		double current = 0;
		for (int i=1; i<numGenes+1; i++) {
			current += Math.log(i);
			logFacts[i] = current;
		}
	
		return numAnnotations;
	}
	
	public void fixAnnotationFile (String associationName, String aspect, String outName) {
			try {
				BufferedReader in = new BufferedReader(new FileReader(associationName));
				String line = in.readLine();
				
				BufferedWriter fout = new BufferedWriter(new FileWriter(outName));
				
				//System.out.println("1st line = " + line);
				while ((line != null)) {
					if ((!line.equals("")) && (!line.substring(0, 1).equals("!"))) {

						String[] parts = line.split("\t");
						/*
						 * Note on association file format:
						 * index/col# - field
						 * 2 - common name
						 * 3 - qualifier (important one is "NOT")
						 * 4 - GO:##### string
						 * 8 - aspect (P, C, or F)
						 * 10 - orf (not req'd, may contain multiple with |s between)
						 */
						//If annotation is in the right aspect
						//  AND the entry isn't qualified as a "not" entry...
						if ((aspect.equalsIgnoreCase("X") || parts[8].equalsIgnoreCase(aspect))
								&& !parts[3].equalsIgnoreCase("NOT")) {
							//Upper case all gene names
							parts[2] = parts[2].toUpperCase();
							parts[10] = parts[10].toUpperCase();

							//Locate this gene
							Gene g;
							Object obj = genes.get(parts[2]);
							if (obj != null) {
								g = (Gene) obj;
								//Get the GoTerm
								obj = terms.get(parts[4]);
								if (obj != null) {
									GoTerm t = (GoTerm) obj;
									if (t.isAnnotated(g) && !t.isAnnotatedToChild(g) && !t.utilSet.contains(g)) {
										fout.write(line + "\n");
										t.utilSet.add(g);
									}
								}
							}
							else {
								System.err.println("Unable to locate gene " + parts[2]);
							}
						}
					}
					else {
						fout.write(line + "\n");
					}
					line = in.readLine();
				}
				
				fout.close();
			}
			catch(FileNotFoundException e) {
				System.err.println("FileNotFound: " + associationName + ". Program aborted.");
				System.exit(0);
			}
			catch(IOException e) {
				System.err.println("IOException: " + associationName + ". Program aborted.");
				System.exit(0);
			}
	}
	
	public double logChoose (int n, int k) {
		if (n == 0) {
			if (k==0)
				return 0;
			else
				return Double.NEGATIVE_INFINITY;
		}
		if (k == 0) {
			return 0;
		}
		if (k == 1) {
			return Math.log(n);
		}
		if (n < k) {
			return Double.NEGATIVE_INFINITY;
		}
		//System.out.println(logFacts[n] + " - (" + logFacts[k] + " + " + logFacts[n-k] + ") = " +
		//		(logFacts[n] - (logFacts[k] + logFacts[n-k])));
		return logFacts[n] - (logFacts[k] + logFacts[n-k]);
	}

	public QueryResult[] goTermFind(String[] queryGeneNames, double pvalueCutoff) {
		HashSet<Gene> queryGenesHS = new HashSet<Gene>();
		for (int i=0; i<queryGeneNames.length; i++) {
			Gene g = genes.get(queryGeneNames[i].toUpperCase());
			if (g != null) {
				queryGenesHS.add(g);
			}
			else {
				System.err.println("WARNING: " + queryGeneNames[i].toUpperCase() + " was not found");
			}
		}		
		
		Gene[] queryGenes = (Gene[])queryGenesHS.toArray(new Gene[queryGenesHS.size()]);
		HashMap<GoTerm,QueryResult> queries = new HashMap<GoTerm,QueryResult>();
		for (int i=0; i<queryGenes.length; i++) {
			Iterator<GoTerm> it = queryGenes[i].annotations.iterator();
			while (it.hasNext()) {
				GoTerm term = it.next();
				//Only include the term if it has more than 1 annotations
				if (term.genes.size() > 1) {
					QueryResult query = queries.get(term);
					if (query == null) {
						query = new QueryResult(term,numGenes,queryGenes.length);
						queries.put(term,query);
					}
					query.numQueryGenesInTerm++;
					query.queryGenesInTerm.add(queryGenes[i].name);
				}
			}
		}
		
		Iterator<QueryResult> it = queries.values().iterator();
		int totResults=0;
		while (it.hasNext()) {
			QueryResult qr = (QueryResult)it.next();
			double sum = 0;
			for(int j=qr.numQueryGenes; j>=qr.numQueryGenesInTerm; j--) {
				sum += Math.exp(logChoose(qr.totalGenesInTerm, j) +
		                logChoose(qr.totalGenes - qr.totalGenesInTerm, qr.numQueryGenes - j)
						- logChoose(qr.totalGenes, qr.numQueryGenes));
			}
			qr.pvalue = sum * queries.values().size();
			if(qr.pvalue < pvalueCutoff) totResults++;
		}
		
		QueryResult[] allResults = (QueryResult[]) queries.values().toArray(new QueryResult[queries.values().size()]);
		Arrays.sort(allResults);
		
		QueryResult[] results = new QueryResult[totResults];
		for (int i=0; i<totResults; i++) {
			results[i] = allResults[i];
		}		
		
		return results;
	}
	
	public QueryResult singleTermFind (Gene[] queryGenes, GoTerm term) {
		QueryResult result = new QueryResult(term,numGenes,queryGenes.length);
		for (int i=0; i<queryGenes.length; i++) {
			if (term.isAnnotated(queryGenes[i])) {
				result.numQueryGenesInTerm++;
			}
		}
		
		double sum = 0;
		for(int j=result.numQueryGenes; j>=result.numQueryGenesInTerm; j--) {
			sum += Math.exp(logChoose(result.totalGenesInTerm, j) +
	                logChoose(result.totalGenes - result.totalGenesInTerm, result.numQueryGenes - j)
					- logChoose(result.totalGenes, result.numQueryGenes));
		}
		result.pvalue = sum;
		
		return result;
	}
	
	/**
	 * Returns a reference to the GO term with the fewest annotations that
	 * is a common ancestor of the genes passed as arguments 
	 * @param g1 Gene name or ORF
	 * @param g2 Gene name or ORF
	 * @return reference to the GoTerm
	 */
	public GoTerm smallestCommonAncestor(String geneName1, String geneName2, String aspect) {
		GoTerm result = null;
		
		Gene gene1 = genes.get(geneName1);
		if (gene1 == null) {
			return null;
		}
		
		Gene gene2 = genes.get(geneName2);
		if (gene2 == null) {
			return null;
		}
		
		if (aspect.equalsIgnoreCase("p")) {
			aspect = "biological_process";
		}
		else if (aspect.equalsIgnoreCase("c")) {
			aspect = "cellular_component";
		}
		else if (aspect.equalsIgnoreCase("f")) {
			aspect = "molecular_function";
		}
		
		//Find the intersection of the annotations
		HashSet<GoTerm> termIntersection = new HashSet<GoTerm>();
		Iterator<GoTerm> it1 = gene1.annotations.iterator();
		while (it1.hasNext()) {
			GoTerm term1 = it1.next();
			if (term1.namespace.equalsIgnoreCase(aspect)) {
				Iterator<GoTerm> it2 = gene2.annotations.iterator();
				while (it2.hasNext()) {
					if (term1 == it2.next()) {
						termIntersection.add(term1);
					}
				}
			}
		}
		
		//Find the smallest common node and return that
		int smallest = Integer.MAX_VALUE;
		Iterator<GoTerm> it = termIntersection.iterator();
		while (it.hasNext()) {
			GoTerm term = it.next();
			if (term.totalAnnotations < smallest) {
				smallest = term.totalAnnotations;
				result = term;
			}
		}
		
		return result;
	}
	
	public void resetTermCounters () {
		Iterator<String> it = terms.keySet().iterator();
		while (it.hasNext()) {
			String termName = it.next();
			GoTerm term = terms.get(termName);
			term.counter = 0;
		}
	}	
	
	public boolean isInTerm (String gName, String termName) {
		GoTerm term;
		if(termName.charAt(0) == 'G') {
			term = terms.get(termName);
		}
		else {
			Integer termIDInt = new Integer (termName);
			term = terms.get(termIDInt);
		}
		
		if (term == null) {
			return false;
		}
		else {
			Gene g = genes.get(gName);
			if (g == null) {
				return false;
			}
			else {
				return term.isAnnotated(g);
			}
		}
	}
	
	public static void main (String[] args) {
		if (args.length < 2) {
			System.err.println("ARGS:");
			System.err.println("0 - gene_ontology obo file");
			System.err.println("1 - annotation file to fix");
			System.err.println("2 - name for fixed annotation file");
			System.exit(0);
		}
		
		System.out.print("Reading ontology file...");
		Ontology GO = new Ontology(args[0]);
		System.out.println("done.");
		
		System.out.print("Reading annotation file...");
		GO.addAnnotations(args[1],"X");
		System.out.println("done.");
		
		System.out.print("Fixing annotation file...");
		GO.fixAnnotationFile(args[1], "X", args[2]);
		System.out.println("done.");
	}
	
}
