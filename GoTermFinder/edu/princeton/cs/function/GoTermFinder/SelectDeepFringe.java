package edu.princeton.cs.function.GoTermFinder;

import java.util.Iterator;

public class SelectDeepFringe {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 3) {
			System.err.println("ARGS:");
			System.err.println("0 - gene_ontology obo file");
			System.err.println("1 - annotation file to fix");
			System.err.println("2 - size cutoff for deep fringe");
			System.exit(0);
		}
		
		System.out.print("Reading ontology file...");
		Ontology GO = new Ontology(args[0]);
		System.out.println("done.");
		
		System.out.print("Reading annotation file...");
		GO.addAnnotations(args[1],"P");
		System.out.println("done.");
		
		int cutoff = Integer.parseInt(args[2]);
		GoTerm bpRoot = (GoTerm)GO.terms.get("GO:0008150");
		traverseChildren(bpRoot, cutoff, GO);
		
	}

	public static void traverseChildren(GoTerm term, int cutoff, Ontology GO) {
		int currSz = term.genes.size();
		if (currSz < cutoff) return;
		
		if (term.sizeOfSmallestChild() < cutoff) {
			System.out.println(term.id);
		}
		
		Iterator<GoTerm> it = term.children.iterator();
		while (it.hasNext()) {
			traverseChildren(it.next(), cutoff, GO);
		}
	}
	
}
