package edu.princeton.cs.function.GoTermFinder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class GetGeneAnnotations {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("ARGS:");
			System.out.println("0 - .obo file");
			System.out.println("1 - annotation file");
			System.out.println("2 - file of gene names to get annotations for");
			System.exit(0);
		}
		
		Ontology GO = new Ontology(args[0]);
		GO.addAnnotations(args[1],"X");
		
		try {
			BufferedReader fin = new BufferedReader(new FileReader(args[2]));
			String line = fin.readLine();
			while (line != null) {
				//Find the gene
				Gene g = (Gene)GO.genes.get(line);
				if (g == null) {
					System.out.println(line + "\t" + "none~none~none");
				}
				else {
					System.out.print(line + "\t");
					GoTerm[] terms = new GoTerm [g.directAnnotations.size()];
					terms = (GoTerm[])g.directAnnotations.toArray(terms);
					Arrays.sort(terms);
					String bp = "", mf = "", cc = "";
					for (int i=0; i<terms.length; i++) {
						if (terms[i].namespace.equalsIgnoreCase("biological_process")) {
							if (bp == "") bp = terms[i].name + "(" + terms[i].id + ")";
							else bp += "|" + terms[i].name + "(" + terms[i].id + ")";
						}
						else if (terms[i].namespace.equalsIgnoreCase("molecular_function")) {
							if (mf == "") bp = terms[i].name + "(" + terms[i].id + ")";
							else mf += "|" + terms[i].name + "(" + terms[i].id + ")";
						}
						else if (terms[i].namespace.equalsIgnoreCase("celluar_component")) {
							if (cc == "") bp = terms[i].name + "(" + terms[i].id + ")";
							else cc += "|" + terms[i].name + "(" + terms[i].id + ")";
						}
					}
					if (bp == "") System.out.print("none");
					else System.out.print(bp);
					if (mf == "") System.out.print("~none");
					else System.out.print("~" + mf);
					if (cc == "") System.out.print("~none");
					else System.out.print("~" + cc);
					System.out.println();
				}
				line = fin.readLine();
			}
		}
		catch (IOException e) {
			System.err.println("Exception caught!");
			System.err.println(e.getMessage());
		}

	}

}
