package edu.princeton.cs.function.GoTermFinder;

import java.util.Iterator;

public class GetDirectAnnotated {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("ARGS:");
			System.out.println("0 - .obo file");
			System.out.println("1 - annotation file");
			System.out.println("2 - ID of term to get direct annotations from");
			System.exit(0);
		}
		
		Ontology GO = new Ontology(args[0]);
		GO.addAnnotations(args[1],"X");
		
		GoTerm term = (GoTerm)GO.terms.get(args[2]);
		if (term != null) {
			Iterator<Gene> it = term.genes.iterator();
			while (it.hasNext()) {
				Gene g = it.next();
				if (g.directAnnotations.contains(term)) {
					System.out.println(g.systematic);
				}
			}
		}

	}

}
