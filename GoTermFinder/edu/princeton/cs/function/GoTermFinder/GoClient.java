/*
 * Created on May 31, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package edu.princeton.cs.function.GoTermFinder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author mhibbs
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class GoClient {

	public static void usage() {
		System.out.println("Usage for GoClient:");
		System.out.println("GoClient <socket> <pvalCutoff> <queryGenes>");
		System.out.println("\tsocket - TCP/IP socket running GO server");
		System.out.println("\tpvalCutoff - corrected p-value cutoff");
		System.out.println("\tqueryGenes - Gene names in the query, separated by a \",\"");
	}
	
	public static void main(String[] args) {
		if (args.length != 3) {
			usage();
			System.exit(0);
		}
		
		Socket sock = null;
		PrintWriter out = null;
		
		try {
			sock = new Socket ("localhost",Integer.parseInt(args[0]));
			out = new PrintWriter(sock.getOutputStream(),false);
			//System.out.println(args[0]);
			out.println(args[1]);
			//System.out.println(args[1]);
			out.println(args[2]);
			out.flush();
						
			BufferedReader br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			int numResults = Integer.parseInt(br.readLine());
			for (int i=0; i<numResults; i++) {
				System.out.println(br.readLine());
			}
			
			out.close();
			br.close();
			sock.close();
		}
		catch(IOException e) {
			System.err.println("IOException occured.  Terminating execution.");
			System.exit(0);
		}
	}
}
