#!/bin/sh

echo ""
echo "configuring Spell++ build system"
echo "" 

if [ ! -d build_aux ]; then
    mkdir build_aux
fi

echo "calling autotools"
echo ""

autoreconf -f -i 

if test $? -ne 0; then
echo ""
echo "Error bootstrapping build system"
echo "Make sure you have the gnu autotools installed"
exit
fi

echo ""
echo "build system configuration complete, ready for:"
echo "   ./configure:"
echo "   make"
