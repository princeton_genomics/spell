/*
 * SearchServer.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SEARCHSERVER_H
#define SEARCHSERVER_H

#include <string>

#include "Searcher.h"

/**
 * SearchServer binds to a port and listens for requests. As a request is 
 * recieved a SearchThread is created and executed as a separate thread. 
 *
 */
class SearchServer {
private:
    /// socket file descriptor
    int sockfd;
    
    /// a search object that can perform the actual query
    Searcher &search;
    
    /// port the SearchServer is bound to
    int port;
    
    /// distance measurement to use for searches spawned by this search server
    unsigned int distType;
    
    /// linkage type to use for searches spawened by this search server, 
    /// only used by search if orthology information has been loaded. 
    unsigned int linkageType;
    
    /// hint to the operating system to queue 5 connections waiting to be accepted
    static const int LISTEN_BACKLOG = 5;
    
    /// if set to true then some extra debugging information is printed
    bool debug;
    
    /**
     * the thread function passed to pthread_create by SearchServer::spawnSearchThread.
     * this will execute a SearchThread in a new thread.
     * 
     * @note This must take a single void pointer as an argument and it must 
     * return a void pointer.  We pass a pointer to a SearchThread object. 
     * Inside threadFunc this is cast back to a SearchThread pointer and we call
     * the execute method that does the search and replies to the client.  
     * threadFunc() then frees the SearchThread with delete and returns (so the 
     * SearchThread must have been allocated with new).
     *
     * @param st a void poniter that points to a SearchThread object
     * @returns a null pointer
     *
     */
    static void *threadFunc(void *st);
    
    /**
     * daemonize the server - this forks a new process and the parent terminates
     * the child process then creates its own session, and closes stdout, 
     * stdin, and stderr, and reopens them directed to /dev/null
     *
     */
    static void daemonizeServer();

public:
    /**
     * Create and initialize the SearchServer. This will open a socket and bind 
     * it to all addresses for this host on the specified port. 
     *
     * @param search a Searcher object to perform queries on incomming requests
     * @param port integer port number to listen on
     * @param distType specifies the distance measurement used for the search
     * @param linkageType specifies the linkage type used for the search (only 
     *        used if orthology information has been loaded)
     * @throws SpellException
     */
    SearchServer (Searcher &search, int port, unsigned int distType, unsigned int linkageType);
    
    /**
     * Listen for incomming search requests. This method enters an infinite loop
     * and accepts connections on the established socket. A thread is spawened 
     * to process incomming requests.
     *
     * @param daemonize if true the server will daemonize itself, if false
     *   it will continue to run in the foreground
     *
     * @throws SpellException
     */
    void listenForRequest(bool daemonize);
    
    /**
     * Spawn a new SearchThread to handle the incomming request.
     *
     * @param connection socket opened to client vi accept call
     * @param address a std::string containing the address the request came from,
     *        for logging purposes
     */
    void spawnSearchThread(int connection, std::string address);
    
    /**
     * turn debugging mode on or off
     *
     * @param debug true to turn on debugging output, false to turn it off
     *
     */
    void setDebug(bool debug)
    {
        this->debug = debug;
    }
  
};

#endif
