/*
 * GeneFamily.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <boost/functional/hash.hpp>

#include "GeneFamily.h"



std::string GeneFamily::toString()
{
    std::string str;
    if (genes.size() > 1) {
        str = "{";
        for (boost::unordered_set<Gene*>::iterator it = genes.begin(); it != genes.end(); ++it) {
            str += (*it)->getName() + ",";
        }
        str[str.length() - 1] = '}';
    }
    else if (genes.size() == 1) {
        str = (*genes.begin())->getName();
    }
    else {
        str = "{}";
    }
    
    return str;
}

std::vector<int> GeneFamily::getGeneIDs() const
{
    std::vector<int> IDs;
    for (boost::unordered_set<Gene*>::iterator it = genes.begin(); it != genes.end(); ++it) {
        Gene* g = *it;
        IDs.push_back(g->getID());
    }
    return IDs;
}
