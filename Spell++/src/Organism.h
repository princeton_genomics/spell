/*
 * Organism.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ORGANISM_H
#define ORGANISM_H

#include <string>
#include <map>
#include <vector>
#include <cstring>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>


#include "ExpressionDataset.h"
#include "Gene.h"
#include "utilities.h"
#include "GeneFamily.h"



/**
 * This class contains all of our information about an Organism. 
 *
 */
class Organism {
    
private:
    /// The text name for the Organism
    std::string name;
    
    /// A hash map to lookup Genes by integer id
    boost::unordered_map<int, Gene*> id_to_gene;
    
    /// A hash map to look up Genes by their name
    boost::unordered_map<std::string, Gene*> genes;
    
    /// A hash set of GeneFamilies
    boost::unordered_set<GeneFamily*> families;
    
    /// A hash map of gene expression datasets with the integer dataset id as the key
    boost::unordered_map<int, ExpressionDataset*> datasets_by_id;
    
    /// A flag to indicate if orthology information was loaded for the Organism
    bool usingOrthology;
    
public:
    
    /**
     * organism constructor initializes the organism ID and loads gene info
     * from a file
     *
     * @param id the string identifiyer for the Organism
     * @param geneFileName the path to a file containing Gene IDs and names to load for this organism
     * @throws SpellException
     */
    Organism(std::string id, std::string geneFileName);
    
    /**
     * Organism descructor. This deletes the Genes and ExpressionDatasets loaded for this Organism.
     *
     */
    ~Organism();
    
    /**
     * Gets a Gene that belongs to this Organism with a given integer ID.
     *
     * @param id an integer Gene identifier
     * @returns a Gene pointer for the Gene, NULL if no gene with this id exists
     */
    Gene *getGeneByID(int id);
    
    /**
     * Gets a Gene that belongs to this Organism with a given name.
     *
     * @param name the Gene name that we are retrieving
     * @returns a Gene pointer for the Gene, NULL if no gene with this name exists
     */
    Gene *getGeneByName(std::string name);
    
    /**
     * get the number of genes that have been loaded for this Organism
     *
     * @returns Organism::genes.size()
     */
    int getNumGenes()
    {
        return genes.size();
    }
    
    /**
     * Get the name for this organism
     *
     * @returns Organism::name
     */
    std::string getName()
    {
        return name;
    }
    
    /**
     *  Load the datasets for this organism.
     *
     *  @param datasetFile the path to a file containig the list of dataset filenames for this organism
     *  @param dataPath the path to the directory containing the dataset files
     *  @throws SpellException
     */
    void loadDatasets(std::string datasetFile, std::string dataPath);
    
    /**
     * Check to see if this Organism is using gene orthology information
     *
     * @returns Organism::usingOrthology
     */
    bool usesOrthology()
    {
        return usingOrthology;
    }
    
    /**
     * Set the Organisms "using orthology" status
     *
     * @param usingOrthology a bool value indicating if the Organism has loaded Orthology information or not
     */
    void setUsingOrthology(bool usingOrthology)
    {
        this->usingOrthology = usingOrthology;
    }
    
    /**
     * add a GeneFamily to this Organism
     *
     * @param family a pointer to a GeneFamily object
     *
     */
    void addFamily(GeneFamily *family)
    {
        families.insert(family);
    }
    
    /**
     * make default GeneFamilies for any orphan Genes (Gene objects that have
     * no associated GeneFamily)
     *
     * @returns a std::vector of GeneFamily pointers, which point to the newly 
     *   created gene families
     */
    std::vector<GeneFamily*> makeDefaultFamilies();
    
    /**
     * get all gene families for this Organism
     *
     * @returns a std::vector of GeneFamily pointers. 
     */
    std::vector<GeneFamily*> getGeneFamilies();
    
    /**
     * get an ExpressionDataset for a given ID
     *
     * @param id an integer ID
     * @returns a pointer to the ExpressionDataset or NULL if no dataset with 
     *   the given ID exists
     */
    ExpressionDataset* getDatasetByID(int id);
    
    /**
     * get a vector containing pointers to all of the ExpressionDatasets loaded
     * for this Organism
     *
     */
    std::vector<ExpressionDataset*> getDatasetVector();
};
#endif
