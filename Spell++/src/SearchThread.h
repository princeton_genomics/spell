/*
 * SearchThread.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SEARCH_THREAD_H
#define SEARCH_THREAD_H

#include <string>

#include "Searcher.h"

/**
 * A namespace containing an enum with error codes for sending error messages
 * back to the client.
 */
namespace error_codes {
    /**
     * enum of error codes used to send error messages back to the client
     *
     */
    enum errorTypes {
        SYS_ERR = 1,
        PROTOCOL_ERROR = 2,
        INVALID_PARAMETER = 3,
        TIME_OUT = 40,
        NO_DSETS = 41
    };
}


/**
 * run a search in a separate thread. This is passed into our thread function
 * that we pass to pthread_create, and the thread function then calls the 
 * execute() method.  Everything needed to perform a query must be available 
 * in the SearchThread, since the only thing that gets passed into the thread 
 * function is a void pointer (we pass a pointer to the SearchThread)
 *
 */
class SearchThread {
private:
    /// socket used for communication with the client
    int connection;
    
    /// initialized Searcher object used to perform the actual search
    Searcher &searcher;
    
    /// string client address for logging purposes
    std::string address;
    
    /// distance type to use for the search
    unsigned int distType;
    
    /// linkage type to use for the search, only used by search if orthology
    /// information has been loaded
    unsigned int linkageType;
    
    /// set to true for debug mode (extra information is printed)
    bool debug;
    
    /// constants used to specify a buffer size used for reading from the socket
    static const int READ_BUFFER_SIZE = 16384;
    
    /// constant used to specify in seconds how long we will wait before timing
    /// out while recieving a query from a client
    static const int RECV_TIMEOUT = 10;
    
    /// default maximum number of genes to return
    static const int MAX_GENES_RETURNED_DEFAULT = 100;
    
    

    
    /**
     *  recieve a full query message from a client (web front end)
     *
     * @returns a std::string containing a complete query from the client
     *
     * @throws SpellException if the socket connection fails
     * @throws TimeoutException if we time out waiting for the client
     */
    std::string recvQuery();
    
    /**
     * reply to the client with a message
     *
     * @param mesg - string message to send to the client
     *
     * @note prints an error message if there was an error communicating with 
     *       the client.  This is an unrecoverable situation, so we just let 
     *       the search thread continue and terminate. 
     *
     */
    void reply(std::string mesg);
    
    /**
     * reply to a client with an error message
     *
     * @param type - integer error code
     * @param mesg - string error message
     *
     * @throws SpellError if there was a problem sending the message
     */
    void replyWithError(int type, std::string mesg);
    
    
    
public:
    
    /**
     * construct a fully initialized SearchThread ready for execution
     *
     * @param searcher a fully initialized Searcher object ready to perform a Spell search
     * @param connection a socket for communicating with the client
     * @param address a std::string address of the client for logging purposes
     * @param distType
     * @param linkageType
     * @param debug set to true for extra debugging output
     * 
     */
    SearchThread(Searcher &searcher, int connection, std::string address, 
                 unsigned int distType, unsigned int linkageType, bool debug):connection(connection),searcher(searcher),
                             address(address), distType(distType), linkageType(linkageType), debug(debug){}
    
    /**
     * destructor
     *
     */
    ~SearchThread();
    
    /**
     * Execute the search. execute() will perform the search
     * and reply to the client with the result.
     *
     */
    void execute();
    
};

#endif
