/*
 * Organism.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <sstream>
#include <iostream>

#include "Organism.h"
#include "SpellException.h"
#include "utilities.h"

using namespace std;


Organism::Organism(std::string name, std::string geneFileName)
{
    
    this->name = name;
    usingOrthology = false;
    
    // load genes from file
    ifstream geneFile(geneFileName.c_str());
    
    if(!geneFile.is_open() || !geneFile.good()) {
        string mesg = "Error opening gene file ";
        mesg = mesg + geneFileName + "\n";
        throw SpellException(mesg);           
    }
    
    string line;

    while (getline(geneFile, line)) {
        
        if (line != "") {
            vector<string> tokens;
            
            utilities::tokenize(line, tokens, "\t");
            if (tokens.size() != 2) {
                string mesg = "Error parsing gene file ";
                mesg = mesg + geneFileName + "\n";
                throw SpellException(mesg);
            }
            
            int id;
            istringstream s(tokens[0]);
            if (!(s >> id)) {
                string mesg = "Error parsing gene file ";
                mesg = mesg + geneFileName + "\n";
                throw SpellException(mesg);
            }
            
            Gene *g = new Gene(this, id, tokens[1]);
            genes[tokens[1]] = g;
            id_to_gene[id] = g;
            
        }
    }
}
    



Organism::~Organism()
{
    for (boost::unordered_map<int, Gene*>::iterator it = id_to_gene.begin();
         it != id_to_gene.end(); ++it) {
        delete it->second;
    }
    
    for (boost::unordered_map<int, ExpressionDataset*>::iterator it = datasets_by_id.begin();
         it != datasets_by_id.end(); ++it) {
        delete it->second;
    }
}


void Organism::loadDatasets(string datasetFileList, string dataPath)
{
    string filename;
    ifstream datasetFile(datasetFileList.c_str());
    
    if(datasetFile.is_open() && datasetFile.good()) {

        
        while (!datasetFile.eof()) {
            string line;
            getline(datasetFile, line);
            if (line != "") {
                vector<string> tokens;
                utilities::tokenize(line, tokens, "\t");
                
                if (tokens.size() != 2) {
                    string mesg = "Error parsing Dataset ID file ";
                    mesg = mesg + datasetFileList + "\n";
                    throw SpellException(mesg);  
                }
                
                int id;
                istringstream s(tokens[0]);
                s >> id;
                
                filename = dataPath + tokens[1];
                cout << "Loading dataset: " << filename << std::endl;
                try {
                    datasets_by_id[id] = new ExpressionDataset(this, filename, id);
                }
                catch (SpellException &e) {
                    throw e;
                }

                
            }
            
        }
    }
    else
    {
        string mesg = "Error opening Dataset ID file ";
        mesg = mesg + datasetFileList + "\n";
        throw SpellException(mesg);   
    }
    
}

Gene *Organism::getGeneByID(int id)
{
    if (id_to_gene.find(id) != id_to_gene.end()) {
        return id_to_gene[id];
    }
    return NULL;
}

Gene *Organism::getGeneByName(std::string name)
{
    if (genes.find(name) != genes.end()) {
        return genes[name];
    }

    return NULL;
    
}

std::vector<GeneFamily*> Organism::makeDefaultFamilies()
{
    vector<GeneFamily*> ret;
    
    for (boost::unordered_map<int, Gene*>::iterator it = id_to_gene.begin(); it != id_to_gene.end(); ++it) {
        Gene *g = it->second;
        
        if (g->getFamily() == NULL) {
            GeneFamily *family = new GeneFamily();
            family->addGene(g);
            g->setFamily(family);
            ret.push_back(family);
        }
    }
    
    return ret;
}

std::vector<GeneFamily*> Organism::getGeneFamilies()
{
    vector<GeneFamily*> famVector;
    
    for (boost::unordered_set<GeneFamily*>::iterator it = families.begin();
         it != families.end(); ++it) {
        famVector.push_back(*it);
    }
    
    return famVector;
}

ExpressionDataset* Organism::getDatasetByID(int id)
{
    if (datasets_by_id.find(id) != datasets_by_id.end()) {
        return datasets_by_id[id];
    }
    
    return NULL;
}

std::vector<ExpressionDataset*> Organism::getDatasetVector()
{
    vector<ExpressionDataset*> dsets;
    
    for (boost::unordered_map<int, ExpressionDataset*>::iterator it = datasets_by_id.begin(); 
         it != datasets_by_id.end(); ++it) {
        dsets.push_back(it->second);
    }
    
    return dsets;
}
