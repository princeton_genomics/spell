/*
 * utilities.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "utilities.h"

    
void utilities::tokenize(std::string str, std::vector<std::string> &tokens,
                  const std::string delimiters, bool clear)
{
    if (clear) {
        tokens.clear();
    }
    
    std::string::size_type last = str.find_first_not_of(delimiters, 0);
    
    std::string::size_type pos = str.find_first_of(delimiters, last);
    
    while (std::string::npos != pos || std::string::npos != last) {
        tokens.push_back(str.substr(last, pos - last));
        
        //skip delimiters
        last = str.find_first_not_of(delimiters, pos);
        //find next delimiter
        pos = str.find_first_of(delimiters, last);
    }
}
    

/* along with #include <ext/hash_set>, this could be used to provide a hash
   function that works with strings, it calls the gcc hash< const char* >
namespace __gnu_cxx
{
    template<> struct hash< std::string >
    {
        size_t operator()( const std::string& x ) const
        {
            return hash< const char* >()( x.c_str() );
        }
    };
}    
*/
