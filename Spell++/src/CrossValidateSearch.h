/*
 * crossValidateSearch.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef CROSS_VALIDATE_SEARCH_H
#define CROSS_VALIDATE_SEARCH_H

#include <boost/multi_array.hpp>

#include "Searcher.h"
#include "SearcherThreadManager.h"
#include "CrossValidateSearchResult.h"
#include "ExpressionDataset.h"
#include "GeneFamily.h"

class SearcherThreadManager;

/**
 * specialized Searcher for cross validation searches
 *
 *
 */
class CrossValidateSearch: public Searcher {
    
    friend class SearcherThreadManager;
    
private:
    
    /// typedef for the xvalDists and xvalQDists types
    typedef boost::multi_array<float, 3> dist_array_type;
    
    /// cached crossvalidation distances indexed by [dataset][query][family]
    dist_array_type xvalDists;
    
    /// cached query distances indexed by [dataset][query1][query2]
    dist_array_type xvalQDists;
      
    /// set to true if cached distances are available
    bool haveDistances;
    
    /// so that the SearcherThreadManager works with a CrossValdateSearch 
    /// instead of a Searcher object we need to keep the arguments to 
    /// scoreGeneFamily function identical.  In order for scoreGeneFamily to 
    /// look up a cahced distance it needs to know the index of the GeneFamily,
    /// so we store that information in this map so scoreGeneFamily can look it up.
    boost::unordered_map<const GeneFamily*, int> famIdx;
    
    
    struct DistanceThreadTask {
        /// starting family index
        unsigned int dsetStart;
        
        /// ending family index
        unsigned int dsetEnd;
        
        /// distance measurement type
        unsigned int distType; 
        
        /// linkage type used
        unsigned int linkageType;
                
        /// pointer to the dataset vector
        std::vector<ExpressionDataset*> *dsets;
        
        /// pointer to the query vector
        std::vector<GeneFamily*> *query;
        
        /// pointer to GeneFamily vector
        std::vector<GeneFamily*> *fams;
        
        /// pointer to CrossValidateSearch object
        CrossValidateSearch *searcher;  
        

    };
    
    
    /**
     * calculate or return cached distance between query[q] and fam
     *
     * @param query vector of query GeneFamily pointers
     * @param dsets vector of ExpressionDataset pointers
     * @param fam GeneFamily pointer
     * @param d dset index
     * @param q query index
     * @param distType distance type to use for calculating score
     * @param linkType linkage type if using orthology information
     *
     * @returns distance for query[q],fam for dsets[d]
     */
    float xvalDist(const std::vector<GeneFamily*> &query, 
                   const std::vector<ExpressionDataset*> &dsets, 
                   const GeneFamily *fam, unsigned int d, 
                   unsigned int q, unsigned int f, 
                   unsigned int distType, unsigned int linkType);
    
    /**
     * calculate or return cached distance between query[q] and query[p]
     *
     * @param query vector of query GeneFamily pointers
     * @param dsets vector of ExpressionDataset pointers
     * @param d dset index
     * @param q query index
     * @param p query index
     * @param distType distance type to use for calculating score
     * @param linkType linkage type if using orthology information
     *
     * @returns distance for query[q],query[p] for dsets[d]
     */
    float queryDist(const std::vector<GeneFamily*> &query, 
                    const std::vector<ExpressionDataset*> &dsets, 
                    unsigned int d, unsigned int q, unsigned int p, 
                    unsigned int distType, unsigned int linkType);
    
    
    /**
     * score a gene family against all query gene families for all datasets
     *
     * @param fam GeneFamily pointer
     * @param datasets vector of ExpressionDataset pointers
     * @param query vector of query GeneFamily pointers
     * @param weights vector of dataset weights
     * @param numWeighted number of datasets that were weighted
     * @param distType distance calculation to use in scoring 
     *        (see distance_type in ExpressionDataset.h)
     * @param linkageType linkage type to use if orthology information loaded
     *        (see linkage_type in ExpressionDatase.h)
     * 
     * @returns the score for fams[f]
     *
     */
    float scoreGeneFamily(const GeneFamily *fam, 
                          const std::vector<ExpressionDataset*> &datasets, 
                          const std::vector<GeneFamily*> &query, 
                          const std::vector<float> &weights, unsigned int numWeighted, 
                          unsigned int distType, unsigned int linkageType);
    
    /**
     * initialize xvalDists and xvalQDists (single threaded)
     *
     * @param query vector of query GeneFamily pointers
     * @param dsets vector of ExpressionDataset pointers
     * @param fams vector of GeneFamily pointers
     * @param distType distType distance calculation to use in scoring 
     *        (see distance_type in ExpressionDataset.h)
     * @param linkageType linkage type to use if orthology information loaded
     *        (see linkage_type in ExpressionDatase.h)
     *
     */
    void calculateAllDistances(std::vector<GeneFamily*> &query, 
                               std::vector<ExpressionDataset*> &dsets,
                               std::vector<GeneFamily*> &fams, 
                               unsigned int distType, 
                               unsigned int linkageType);
    
    /**
     * initialize xvalDists and xvalQDists (multi threaded)
     *
     * @param query vector of query GeneFamily pointers
     * @param dsets vector of ExpressionDataset pointers
     * @param fams vector of GeneFamily pointers
     * @param distType distType distance calculation to use in scoring 
     *        (see distance_type in ExpressionDataset.h)
     * @param linkageType linkage type to use if orthology information loaded
     *        (see linkage_type in ExpressionDatase.h)
     */
    void calculateAllDistancesThreaded(std::vector<GeneFamily*> &query, 
                                       std::vector<ExpressionDataset*> &dsets,
                                       std::vector<GeneFamily*> &fams, 
                                       unsigned int distType, 
                                       unsigned int linkageType);

    /**
     * initialize xvalDists[d] and xvalQDists[d] (multi threaded)
     *
     * @param d dataset index
     * @param query vector of query GeneFamily pointers
     * @param dsets vector of ExpressionDataset pointers
     * @param fams vector of GeneFamily pointers
     * @param distType distance calculation to use in scoring 
     *        (see distance_type in ExpressionDataset.h)
     * @param linkageType linkage type to use if orthology information loaded
     *        (see linkage_type in ExpressionDatase.h)     
     *
     */
    void  calculateDistancesForDataset(unsigned int d,
                                       std::vector<GeneFamily*> *query, 
                                       std::vector<ExpressionDataset*> *dsets,
                                       std::vector<GeneFamily*> *fams,
                                       unsigned int distType, 
                                       unsigned int linkageType);
    
    /**
     * pthread function to initialize xvalDists and xvalQDists for one or more 
     * datasets
     *
     * @param task a pointer to a DistanceThreadTask struct
     */
    static void *distanceThreadFunc(void *task);
    
public:
    
    /**
     * constructor for a CrossValidateSearch object, calls the Searcher constructor
     *
     * @param homology pointer to the homology object to use for the search
     * @param maxThreads the maximum number of threads to use for the search. If
     *        > 1 then we use pthreads to parallelize some of the more
     *        computatinally intensive tasks. 
     */
    CrossValidateSearch (Homology *homology, unsigned int maxThreads=1):Searcher(homology, maxThreads)
    {
        haveDistances = false;
    }
    
    /**
     * perform a cross validation search
     *
     * @param query vector of query GeneFamily pointers
     * @param cvSize cross validation sample size
     * @param numSamples number of random samples to take
     * @param distType distance calculation to use in scoring 
     *        (see distance_type in ExpressionDataset.h)
     * @param linkageType linkage type to use if orthology information loaded
     *        (see linkage_type in ExpressionDatase.h)
     * @param useWeights weight each dataset based on co-expression of query
     *        genes
     * @param storePartialResults if set to true results from each sample will 
     *        be saved
     *
     * @returns a CrossValidateSearchResult object
     */
    CrossValidateSearchResult search(std::vector<GeneFamily*> &query, 
                                     unsigned int cvSize, 
                                     unsigned int numSamples, 
                                     unsigned int distType, 
                                     unsigned int linkageType,
                                     bool useWeights, bool storePartialResults);
    


    
};

#endif
