/*
 * crossValidateSearch.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>

#include <boost/multi_array.hpp>



#include "CrossValidateSearch.h"
#include "CrossValidateSearchResult.h"
#include "GeneFamily.h"
#include "Homology.h"
#include "ExpressionDataset.h"
#include "config.h"


using namespace std;

CrossValidateSearchResult CrossValidateSearch::search(std::vector<GeneFamily*> &query, 
                                                      unsigned int cvSize, 
                                                      unsigned int numSamples, 
                                                      unsigned int distType, 
                                                      unsigned int linkageType,
                                                      bool useWeights, 
                                                      bool storePartialResults)
{
    vector<ExpressionDataset*> dsets;
    vector<GeneFamily*> families;
    
    boost::unordered_map<GeneFamily*, float> rankMap; 
    boost::unordered_map<GeneFamily*, unsigned int> countMap;
    vector<float> avgDsetWeight;
    
    CrossValidateSearchResult *results = 0;

    
    dsets = homology->getDatasets();
    families = homology->getFamilies();
    
    // initialize famIdx
    famIdx.clear();
    for (int i = 0; i < families.size(); i++) {
        famIdx[families[i]] = i;
    }
    
    
    if (cvSize > query.size()) {
        cvSize = query.size();
    }
    

#ifdef DISABLED_AS_OF_2_0_2_DATASETS_ARE_NOW_MORE_DYNAMIC
    //Calculate distances from all queries to every GeneFamily in the specified datasets
    //using the distance type specified - indexed by [dataset][query][family]
    try {
        xvalDists.resize(boost::extents[dsets.size()][query.size()][families.size()]);
        xvalQDists.resize(boost::extents[dsets.size()][query.size()][families.size()]);
                
        // we were able to allocate enough memory to hold the distances
        haveDistances = true;
        
        cout << "   Calculating distances...";
        flush(cout);
        if (maxThreads > 1) {
            calculateAllDistancesThreaded(query, dsets, families, distType, linkageType);
        }
        else {
            calculateAllDistances(query, dsets, families, distType, linkageType);
        }
        
        cout << "done\n";
        flush(cout);
    }
    catch (bad_alloc&) {
        cout << "   WARNING: unable to allocate enough memory for full distance matrix\n"
             << "            distances will be calculated on the fly\n";
        haveDistances = false;
    }
#else
    haveDistances = false;
#endif

    //initialize the average dataset weights
    avgDsetWeight.resize(dsets.size(), 0.0f);
 
    srand(time(NULL));
    
    //Sample from the query vector to construct numSamples individual queries each of size cvSize
    for (int numSamplesTaken = 0; numSamplesTaken < numSamples; numSamplesTaken++) {
        boost::unordered_set<unsigned int> qIdxSet;
        vector<GeneFamily*> sampleQuery;
        
        //choose some random indexes into our query genes for this sample
        while (qIdxSet.size() < cvSize) {
            unsigned int randVal = (unsigned int)((double)rand() * query.size() / (RAND_MAX + 1.0));
            qIdxSet.insert(randVal);
        }
        

        for (boost::unordered_set<unsigned int>::iterator it = qIdxSet.begin();
             it != qIdxSet.end(); ++it) {
            sampleQuery.push_back(query[*it]);
        }
        
        //Determine dataset weights
        int numWeighted = 0;
        vector<float> weights;
        for (int d = 0; d < dsets.size(); d++) {
            if (!useWeights) {
                weights.push_back(1.0f);
            }
            else { 
                weights.push_back(0.0f);
                int numPairs = 0;
                for (int i = 0; i < sampleQuery.size(); i++) {
                    for (int j = i+1; j < sampleQuery.size(); j++) {
                        float val = queryDist(sampleQuery, dsets, d, i, j, distType, linkageType);
                        if (!isnan(val)) {
                            numPairs++;
                            weights[d] += val;
                        }
                    }
                }
                if (numPairs > 0) {
                    weights[d] /= (float)numPairs;
                    numWeighted++;
                }
            }
        }

	// We only want to consider the datasets which contain at least one
	// of the genes in the query set, and at least 5 conditions.
	// Getting good correlation statistics relies on having 5 or more
	// conditions in the dataset.
	vector<ExpressionDataset*> tmpDatasets = vector<ExpressionDataset*>();
	vector<float> tmpWeights = vector<float>();

	for (int d = 0; d < dsets.size(); d++) {
	    if ((!isnan(weights[d])) && (dsets[d]->getNumConds() >= 5)) {
                tmpWeights.push_back(weights[d]);
		tmpDatasets.push_back(dsets[d]);
	    }
	    else {
	        if (weights[d] > 0.0)
		    numWeighted--;
	    }
	}
	dsets = tmpDatasets;
	weights = tmpWeights;

	//If very few datasets showed significant correlation, 
	//weight all datasets the same
	if (numWeighted != 0 && numWeighted < 3) {
	    for (unsigned int i = 0; i < weights.size(); i++) {
                weights[i] = 1.0f;
	    }
	    numWeighted = dsets.size();
	}

	
	results = new CrossValidateSearchResult(query, dsets);

        //Calculate the scores for all families to this query
        vector<SingleResult> singleResults;
        
        if (maxThreads > 1) {
            try {
                singleResults = threadMan->scoreGeneFamiliesThreaded(families, 
                                                                     dsets, 
                                                                     query, 
                                                                     weights, 
                                                                     numWeighted, 
                                                                     distType, 
                                                                     linkageType); 
            }
            catch (SpellException &e) {
                throw e;
            }
            
        }
        else {
            float score;
            for (unsigned int f = 0; f < families.size(); f++) {
                
                score =  scoreGeneFamily(families[f], dsets, sampleQuery, weights,
                                         numWeighted, distType, linkageType);
                
                singleResults.push_back(SingleResult(families[f], score)); 
            }
        }        
        // sort and calculate results of this fold
        
        // sort singleResults in decending order by score (highest score is at
        // singleResults[0])
        sort(singleResults.begin(), singleResults.end(), 
             SingleResult::sortCompDescending);
        
        
        // build vectors containing the order and scores for this fold
        vector<GeneFamily*> iterOrder;
        vector<float> iterScore;
        for (int i = 0; i < singleResults.size(); i++) {
            iterOrder.push_back(singleResults[i].getFamily());
            iterScore.push_back(singleResults[i].getScore());
        }
        
        boost::unordered_set<GeneFamily*> iterQuery;
        for (int i = 0; i < sampleQuery.size(); i++) {
            iterQuery.insert(sampleQuery[i]);
        }
        
        //Update the combined result vectors with the resulting orders and weights
        int rank = 0;
        for (int j = 0; j < iterOrder.size(); j++) {
            GeneFamily *f = iterOrder[j];
            if (iterQuery.find(f) == iterQuery.end()) {
                if (rankMap.find(f) == rankMap.end()) {
                    rankMap[f] = (float)rank;
                    countMap[f] = 1;
                }
                else {
                    rankMap[f] += rank;
                    countMap[f]++;
                }
                rank++;
            }            
        }
        for (int d = 0; d < dsets.size(); d++) {
            avgDsetWeight[d] += weights[d];
        }
        
        
        //If requested, also store the partial results
        if (storePartialResults == true) {
            results->insertPartialOrder(iterOrder);
            results->insertPartialScores(iterScore);
            results->insertPartialQueries(iterQuery);
            results->insertPartialWeights(weights);
        }
    }
    
    //Create the by rank averages, and store the results in the result object
    if (families.size() != rankMap.size()) {
        throw SpellException("families.size() != rankMap.size()");
    }
    vector<SingleResult> rankResults;
    for (boost::unordered_map<GeneFamily*, float>::iterator it = rankMap.begin();
         it != rankMap.end(); ++it) {
        GeneFamily *f = it->first;
        rankResults.push_back(SingleResult (f, rankMap[f] / countMap[f]));
    }
    sort(rankResults.begin(), rankResults.end(), SingleResult::sortCompAscending);
    
    vector<GeneFamily*> finalOrder;
    vector<float> finalScores;
    for (int i = 0; i < rankResults.size(); i++) {
        finalOrder.push_back(rankResults[i].getFamily());
        finalScores.push_back(rankResults[i].getScore());
    }
    
    results->setFinalOrder(finalOrder);
    results->setFinalScores(finalScores);
    
    //Also store the average dataset weight in the result object
    results->setFinalWeights(avgDsetWeight);

    
    
    return *results;
}

float CrossValidateSearch::xvalDist(const std::vector<GeneFamily*> &query, 
                                    const std::vector<ExpressionDataset*> &dsets, 
                                    const GeneFamily *fam, unsigned int d, 
                                    unsigned int q, unsigned int f, 
                                    unsigned int distType, unsigned int linkageType) {
    if (haveDistances) {
        return xvalDists[d][q][f];
        
    }
    else {        
        if (homology->usesOrthology()) {
            return dsets[d]->score(query[q], fam, distType, linkageType);
        }
        else {
            return dsets[d]->score(*(query[q]->begin()), *(fam->begin()), distType);
        }
    }
}

float CrossValidateSearch::queryDist(const std::vector<GeneFamily*> &query, 
                                     const std::vector<ExpressionDataset*> &dsets,
                                     unsigned int d, unsigned int q, unsigned int p, unsigned int distType, 
                                     unsigned int linkageType) {
    if (haveDistances) {
        return xvalQDists[d][q][p];
    }
    else {
        if (homology->usesOrthology()) {
            return dsets[d]->score(query[q], query[p], distType, linkageType);
        }
        else {
            return dsets[d]->score(*(query[q]->begin()), *(query[p]->begin()), distType);
        }
    }
}


float CrossValidateSearch::scoreGeneFamily(const GeneFamily *fam, 
                                           const std::vector<ExpressionDataset*> &datasets, 
                                           const std::vector<GeneFamily*> &query, 
                                           const std::vector<float> &weights, unsigned int numWeighted, 
                                           unsigned int distType, unsigned int linkageType)
{
    float score = 0.0f;
    int dsetsUsed = 0;
    bool singleGeneQuery = (query.size() == 1);
    
    for (unsigned int d = 0; d < datasets.size(); d++) {
        float wgt = weights[d];
        if (wgt > 0) {
            float dsetScore = 0.0f;
            int numPairs = 0;
            
            for (unsigned int i = 0; i < query.size(); i++) {
                float val =  xvalDist(query, datasets, fam, d, i, 
                                      famIdx[fam], distType, linkageType);
                if (!isnan(val)) {
                    dsetScore += val;
                    numPairs++;
                }
            }

            // For singleGeneQueries, we only use the weight to order
            // the datasets, not to calculate the scores.
            if (singleGeneQuery)
                wgt = 1.0;

            if (numPairs > 0) {
                score += wgt * (dsetScore / (float)numPairs);
                dsetsUsed++;
            }
        }
    }
    
    // Through V2.0.1, the score was divided by the sum of the weights of the
    // datasets in which it was found, forming a weighted average over those
    // datasets.  That resulted in non-intuitive displays, as a gene that
    // was not represented in the heavily weighted datasets, but was highly
    // correlated in relatively unimportant datasets, would be ranked very
    // highly.
    // Now we divide the score by 100 (the total weights of all the datasets)
    // which in effect causes us to treat the gene as "uncorrelated" (score
    // of zero) in the datasets in which it doesn't appear.
    score /= 100;
    
    if (dsetsUsed < min((float)numWeighted, 
                        max((0.1f * datasets.size()), 4.0f))) {
        score = 0.0f;
    }
    
    return score;
    
}

void CrossValidateSearch::calculateAllDistances(std::vector<GeneFamily*> &query, 
                                                std::vector<ExpressionDataset*> &dsets,
                                                std::vector<GeneFamily*> &fams, 
                                                unsigned int distType, 
                                                unsigned int linkageType)
{
    for (unsigned int d = 0; d < dsets.size(); d++) {
        calculateDistancesForDataset(d, &query, &dsets, &fams, distType, linkageType);
    }
}


void CrossValidateSearch::calculateDistancesForDataset(unsigned int d,
                                                       std::vector<GeneFamily*> *query, 
                                                       std::vector<ExpressionDataset*> *dsets,
                                                       std::vector<GeneFamily*> *fams,
                                                       unsigned int distType, 
                                                       unsigned int linkageType)
{
    for (int q = 0; q < query->size(); q++) {
        for (int f = 0; f < fams->size(); f++) {
            if (homology->usesOrthology()) {
                xvalDists[d][q][f] = (*dsets)[d]->score((*query)[q], (*fams)[f], distType, linkageType);
            }
            else {
                xvalDists[d][q][f] = (*dsets)[d]->score(*((*query)[q]->begin()), *((*fams)[f]->begin()), distType);
            }
        }
        for (int p = 0; p < query->size(); p++) {
            if (homology->usesOrthology()) {
                xvalQDists[d][q][p] = (*dsets)[d]->score((*query)[q], (*query)[p], distType, linkageType);
            }
            else {
                xvalQDists[d][q][p] = (*dsets)[d]->score(*((*query)[q]->begin()), *((*query)[p]->begin()), distType);
            }
        }
    }
}


void CrossValidateSearch::calculateAllDistancesThreaded(std::vector<GeneFamily*> &query, 
                                                        std::vector<ExpressionDataset*> &dsets,
                                                        std::vector<GeneFamily*> &fams, 
                                                        unsigned int distType, 
                                                        unsigned int linkageType)
{
    unsigned int numThreads;
    unsigned int dsetsPerThread;
    unsigned int remainder;
    unsigned int next;
    int r;
    DistanceThreadTask *tasks;
    
    numThreads = min(maxThreads, (unsigned int)dsets.size());
    dsetsPerThread = dsets.size() / numThreads;
    remainder = dsets.size() % numThreads;
    
    tasks = new DistanceThreadTask[numThreads];
    
    // setup the task structs.
    next = 0;
    for (unsigned int i = 0; i < numThreads; i++) {
        
        // common setup
        tasks[i].query       = &query;
        tasks[i].fams        = &fams;
        tasks[i].dsets       = &dsets;
        tasks[i].distType    = distType; 
        tasks[i].linkageType = linkageType;
        tasks[i].searcher    = this;
        
        // task specific setup
        
        // start index for datasets
        tasks[i].dsetStart = next;
        
        // end index for families
        tasks[i].dsetEnd = next + (dsetsPerThread - 1);
        
        // if the number of families didn't divide evenly by the number 
        // of threads then spread the remainder out
        if (remainder > 0) {
            tasks[i].dsetEnd++;
            --remainder;
        }
        
        // set next to start of next task
        next = tasks[i].dsetEnd + 1; 
    }
    
    //  explicitly create the pthread as joinable 
    // (default on fully POSIX complient implementations)
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_t threads[numThreads];
    
    // launch each task in its own pthread
    for (unsigned int i = 0; i < numThreads; i++) {
        r = pthread_create(&threads[i], &attr, 
                           CrossValidateSearch::distanceThreadFunc, 
                           (void*)&tasks[i]);
        
        
        if (r != 0) {
            ostringstream oss;
            oss << "ERROR creating thread, pthread_create returned " << r;
            throw SpellException(oss.str());
        }
        
    }
    pthread_attr_destroy(&attr);
    
    // wait for all pthreads to complete
    for (unsigned int i = 0; i < numThreads; i++) {
        r = pthread_join(threads[i], NULL);
        
        // pthread_join returned an error code. This shouldn't happen, maybe a 
        // bug caused the thread to crash?
        if (r != 0) {
            cerr << "WARNING: pthread_join() returned " << r << " for thread " << i << endl;
        }
    }
    
    delete[] tasks;
}


void *CrossValidateSearch::distanceThreadFunc(void *task)
{
    DistanceThreadTask *t = (DistanceThreadTask*)task;
    
    // iterate over my portion of the dataset vector
    for (unsigned int i = t->dsetStart; i <= t->dsetEnd; i++) {
        t->searcher->calculateDistancesForDataset(i, t->query, t->dsets, 
                                                  t->fams, t->distType, 
                                                  t->linkageType);
    }
    
    return NULL;
}

