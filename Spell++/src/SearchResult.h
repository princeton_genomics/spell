/*
 * SearchResult.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SEARCH_RESULT_H
#define SEARCH_RESULT_H

#include <vector>

#include "GeneFamily.h"
#include "ExpressionDataset.h"


/**
 * encapuslates the results from a search
 * when we search on a single gene, the search algorithm is run twice (see Searcher.cpp); this class can rationalize the results of both passes.
 */
class SearchResult {
    
protected:
    
    /**
     * private inner class to store the score for a dataset 
     *
     */
    class DatasetResult {
    private:
        /// pointer to the ExpressionDataset that this result is for
        ExpressionDataset *dataset;
        
        /// the score for this ExpressionDataset
        float score;

    public:
        
        /**
         * construct a DatasetResult
         *
         * @param dset a pointer to an ExpressionDataset
         * @param score the score for this dataset
         */
        DatasetResult(ExpressionDataset *dset, float score):dataset(dset), score(score) {}
        
        /**
         * get a pointer to the dataset this result is for
         *
         * @returns #dataset
         *
         */
        ExpressionDataset* getDataset()
        {
            return dataset;
        }
        
        /**
         * get the score
         *
         * @returns #score
         */
        float getScore()
        {
            return score;
        }
        
        /**
         * a comparision function to use with std::sort(). This will result in 
         * decending order by score.
         *
         * @param r1 a DatasetResult passed by reference
         * @param r2 a DatasetResult passed by reference
         * @returns true if r1.score > r2.score
         *
         */
        static bool sortCompDescending(const DatasetResult &r1, const DatasetResult &r2) 
        {
            return (bool)(r1.score > r2.score);
        }
        
    };
    
    
    /* search parameters, used by the toString() method to build a string 
       representation of the results */
    
    /// pointers to the GeneFamily objects for all of the query genes
    std::vector<GeneFamily*> query;
    
    /// pointers to the ExpressionDatasets specified in the request from the client 
    std::vector<ExpressionDataset*> datasets;
    
    
    /* search results */
    
    /// vector of pointers to the coexpressed gene fammiles, ordered decending by score
    std::vector<GeneFamily*> finalOrder;
    
    /// scores of the gene families in finalOrder
    std::vector<float> finalScores;
    
    /// weights of the datasets
    std::vector<float> finalWeights;
    
    
public:
    
    /**
     * construct a new SearchResult. This is initialized with the query 
     * information but not results.
     *
     * @param q a vector of pointers to the query genes (converted to GeneFamilies)
     * @param dset a vector of pointers to the ExpressionDatasets specified in the query
     *
     */
    SearchResult(std::vector<GeneFamily*> q, 
                 std::vector<ExpressionDataset*> dset):query(q),datasets(dset){}
 
    /**
     *
     *
     *
     *
     */
    SearchResult() {}
    
    /**
     * format the Gene Score using finalOrder[i] finalScores[i]
     *
     * @param i index into finalOrder and finalScore to use for output
     * @returns a string with the Gene ID and score formatted for output
     *
     */
    std::string formatGeneScore(unsigned int i);
    
    /**
     * set the final list of Gene Families
     *
     * @param finalOrder a vector of GeneFamily pointers sorted in decending order by score
     */
    void setFinalOrder(std::vector<GeneFamily*> finalOrder)
    {
        this->finalOrder = finalOrder;
    }
    
    /**
     * set the list of scores corresponding to our ordered list of GeneFamilies
     *
     * @param finalScores a vector of floats in decending order. finalScores[i]
     *        is the score for finalOrder[i]
     *
     */
    void setFinalScores(std::vector<float> finalScores)
    {
        this->finalScores = finalScores;
    }
    
    /**
     * set the weights for the query datasets
     *
     * @param weights a vector of floats containing dataset weights. weights[i] 
     *        is the weight for datasets[i]
     *
     */
    void setFinalWeights(std::vector<float> weights)
    {
        finalWeights = weights;
    }

    void setFinalWeightsFromPass2(SearchResult& pass2);

    /**
     * convert the search result into a string formatted for sending back to 
     * the client
     *
     * @returns std::string containing the search results
     * @throws SpellException
     *
     */
    std::string toString();
};

#endif
