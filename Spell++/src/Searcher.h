/*
 * Searcher.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SEARCH_H
#define SEARCH_H

#include <vector>

#include "Homology.h"
#include "SearchResult.h"

// forward declaration of SearcherThreadManager
class SearcherThreadManager;


/**
 * provides an object that can perform Spell searches
 *
 */
class Searcher {
    
    friend class SearcherThreadManager;
    
protected:
    /// the Homology to use for the search, this contains all of the 
    /// GeneFamilies and ExpressionDatasets
    Homology *homology;
    
    /// maximum number of threads to use for multi-threaded portions of the search
    unsigned int maxThreads;
    
    /// pointer to a thread manager object, may be NULL if we are running single 
    /// threaded
    SearcherThreadManager *threadMan;
    
    //these are for cross validation searches (not yet implemented)
    //std::vector<std::vector<std::vector<float> > > xvalDists;
    //std::vector<std::vector<std::vector<float> > > xvalQDist;
    
    

    /**
     * calculate the score for a gene family given query datasets and genes
     *
     * @param fam pointer to a GeneFamily object
     * @param datasets reference to a vector of ExpressionDataset pointers(query datasets)
     * @param query reference to a vector of GeneFamily poiners (query genes)
     * @param weights reference to a vector of floating point dataset weights
     * @param numWeighted the number of datasets that were weighted (not 
     *        necessarily the size of weights)
     * @param distType the type of distance measurement to use in scoring
     * @param linkageType the linkage type used for correlating genes in a gene family
     * 
     * @returns a floating point score
     */
    virtual float scoreGeneFamily(const GeneFamily *fam, 
                          const std::vector<ExpressionDataset*> &datasets, 
                          const std::vector<GeneFamily*> &query, 
                          const std::vector<float> &weights, 
                          unsigned int numWeighted, unsigned int distType, 
                          unsigned int linkageType);
    
    
    /**
     * calculate the weight for each query dataset
     *
     * @param weights a reference to a vector to store the dataset weights in
     * @param datasets reference to a vector of ExpressionDataset pointers (query datasets)
     * @param query a reference to a vector of GeneFamily pointers (query genes)
     * @param distType the type of distance measurement to use
     * @param linkageType the linkage type to use for correlating genes in a gene family
     * 
     * @returns the number of datasets weighted
     */
    int calculateDatasetWeights(std::vector<float> &weights, 
                                const std::vector<ExpressionDataset*> &datasets,
                                const std::vector<GeneFamily*> &query,
                                unsigned int distType, unsigned int linkageType);
    
public:
    
    /**
     * a single result for a single GeneFamily. This is used to store and then
     * sort scores for gene families. 
     *
     * @note This used to be a private member of Searcher as it is only needed 
     *   by Searcher and SearcherThreadManager (which is a friend of Searcher).
     *   However, with some older versions of g++ SearcherThreadManager could not
     *   use the SingleResult class unless it was made public. It appears that 
     *   with gcc 4.0 this needs to be public, but with 4.2 it can be private.
     *
     */  
    class SingleResult {
    private:
        /// the GeneFamily that this result is for
        GeneFamily *family;
        
        /// the score
        float score;
        
        
        
    public:
        
        /**
         * construct an initialized SingleResult
         *
         * @param f a GeneFamily pointer
         * @param s a float score
         *
         */
        SingleResult(GeneFamily *f, float s):family(f),score(s) {}
        
        
        
        /**
         * get the score of this SingleResult
         *
         * @returns score
         *
         */
        float getScore()
        {
            return score;
        }
        
        /**
         * get the GeneFamily for this SearchResult
         *
         * @returns family
         */
        GeneFamily* getFamily()
        {
            return family;
        }
        
        /**
         * a comparision function to use with std::sort(). This will result in 
         * descending order by score.
         *
         * @param s1 a SingleResult passed by reference
         * @param s2 a SingleResult passed by reference
         * @returns true if s1.score > s2.score
         *
         */
        static bool sortCompDescending(const SingleResult &s1, const SingleResult &s2) 
        {
            return (bool)(s1.score > s2.score);
        }
        
        /**
         * a comparision function to use with std::sort(). This will result in 
         * ascending order by score.
         *
         * @param s1 a SingleResult passed by reference
         * @param s2 a SingleResult passed by reference
         * @returns true if s1.score < s2.score
         *
         */
        static bool sortCompAscending(const SingleResult &s1, const SingleResult &s2) 
        {
            return (bool)(s1.score < s2.score);
        }
        
    };
    
    
    
    /**
     * construct a complete Searcher object. This contains everything needed
     * to create a Searcher ready to perform searches
     *
     * @param homology a pointer to an initialized Homology object
     * @param maxThreads max number of threads to use for a search
     */
    Searcher(Homology *homology, unsigned int maxThreads=1):homology(homology),maxThreads(maxThreads)
    {
        threadMan = NULL;
        
        if (maxThreads > 1) {
            initThreadManager(maxThreads);
        }
    }
    
    
    /**
     * copy constructor for Searcher.  This creates a new thread manager, so 
     * it won't get trashed when the source Searcher's destructor gets called. 
     * We changed everywhere this might get called to pass a reference instead, 
     * so it should no longer get called, but it is left in place to prevent 
     * crashes that may occur if the code ever changes and the copy constructor
     * is called
     *
     */
    Searcher(const Searcher &s) {
        homology = s.homology;
        
        if (s.maxThreads > 1) {
            initThreadManager(s.maxThreads);
        }
        else {
            maxThreads = 1;
            threadMan = NULL;
        }
    }

    /**
     * destructor
     *
     */
    virtual ~Searcher();
    
    /**
     * initialize the SearcherThreadManager for this Searcher object
     *
     * @param maxThreads the maximum number of threads the thread manager will
     *        spawn
     *
     */
    void initThreadManager(unsigned int maxThreads);

    /**
     * perform a normal Spell search. If this->maxThreads is > 1 then the 
     * gene family scoring will be done in multiple threads.
     *
     * @param geneIDs a vector of integer Gene IDs specified by the client in its query
     * @param datasetIDs a vector of integer ExpressionDataset IDs specified by the client
     * @param distType distance measurement to use, one of the enum values in distance_type
     * @param linkageType linkage statistics type, one of the enum values in link_type
     * @param useWeights weight the datasets by co-expression of query genes,
     *        weight them all the same
     * @param maxGenes maximum number of genes to return in the results
     *
     * @returns a SearchResult object
     * @throws a SpellException if there is a failure spawning threads
     *
     */
    SearchResult querySearch(std::vector<int> geneIDs, 
                             std::vector<int> datasetIDs, unsigned int distType,
                             unsigned int linkageType, unsigned int maxGenes);
    

    
};

#endif

