/*
 * utilities.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef UTILITIES_H
#define UTILITIES_H

#include <string>
#include <vector>
#include <sstream>
#include <limits>
#include <cstring>

#include "SpellException.h"

/**
 * this namespace contains a few functions that are generally useful and don't 
 * belong in any specific class
 *
 */
namespace utilities {
    
    /**
     * tokenize a std::string into a std::vector of strings
     *
     * @param str - the string we are tokenizing
     * @param tokens - the vector to store the tokens in, pass by reference
     * @param delimiters - a string containing the characters we will use to 
     *        delimit our tokens
     * @param clear - boolean to indicate if the tokens vector should be cleared
     *        or if we are going to append new tokens to it
     */
    void tokenize(std::string str, std::vector<std::string> &tokens,
                  const std::string delimiters = "\t", bool clear = true);
    
    
    /**
     * convert a string into another type
     *
     * @param s string to convert, passed by reference
     * @returns s converted into a type T. If the conversion fails, quite_NaN 
     *          is returned (when supported, i.e., for floats and doubles)
     *
     * @throws a SpellException if T is an integer type and the conversion fails
     */
    template <class T> T from_string(std::string &s)
    {
        std::istringstream iss(s);
        T t;
        if ((iss >> t).fail()) {
            if (std::numeric_limits<T>::is_integer) {
                throw SpellException("Invalid value");
            }
            else {
                t = std::numeric_limits<T>::quiet_NaN();
            }
        }
        return t;
    }
    
    
    template<class T> int vectorContains(std::vector<T> v, T item)
    {
        for (unsigned int i = 0; i < v.size(); i++) {
            if (v[i] == item) {
                return true;
            }
        }
        
        return false;
    }
}

#endif
