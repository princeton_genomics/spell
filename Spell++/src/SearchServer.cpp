/*
 * SearchServer.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>

#include <pthread.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>

#include "SearchServer.h"
#include "SearchThread.h"
#include "Searcher.h"
#include "SpellException.h"


using namespace std;



SearchServer::SearchServer (Searcher &search, int p, unsigned int distType, 
                            unsigned int linkageType): search(search), port(p), distType(distType), linkageType(linkageType) 
{
    
    struct addrinfo hints;
    struct addrinfo *res;
    std::string portString;
    
    if (port < 1 || port > 65535) {
        ostringstream mesg;
        mesg << "Listening port (" << port << ") is invalid\n";
        throw SpellException(mesg.str());     
    }
    
    ostringstream oss;
    oss << port;
    
    portString = oss.str();

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;     // use IPv4 or IPv6, whichever
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me
    
    
    int r = getaddrinfo(NULL, portString.c_str(), &hints, &res);
    if (r != 0) {
        std::string mesg = "Error getting address info for local host: ";
        mesg += gai_strerror(r);
        mesg += "\n";    
        throw SpellException(mesg);
    }
    
    // open the socket and bind to it
    
    // open socket, throw exception if we are not sucessful
    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sockfd == -1) {
        std::string mesg = "Unable to open socket: ";
        mesg += strerror(errno);
        mesg += "\n";    
        throw SpellException(mesg);
    }
    
    
    // set SO_REUSEADDR on a socket to true (1):
    int optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
    
    // bind socket to the listening address/port
    if (bind(sockfd, res->ai_addr, res->ai_addrlen) != 0) {
        std::string mesg = "Unable to bind socket to address: ";
        mesg += strerror(errno);
        mesg += "\n";    
        throw SpellException(mesg);
    }
    
    freeaddrinfo(res);

}

void SearchServer::listenForRequest(bool daemonize)
{
    int conn;
    struct sockaddr_storage client_addr;
    socklen_t addr_size = sizeof client_addr;
    
    cout << "Listening on port " << port << endl;
    if (listen(sockfd, LISTEN_BACKLOG) != 0) {
        std::string mesg = "Error listening on socket: ";
        mesg += strerror(errno);
        mesg += "\n";    
        throw SpellException(mesg);
    }
    
    if (daemonize) {
        cout << "\nDaemonizing server\n";
        try {
            daemonizeServer();
        }
        catch (SpellException &e) {
            throw e;
        }
    }
    
    for (;;) {
        conn = accept(sockfd, (sockaddr *)&client_addr, &addr_size);
        
        if (conn > 0) {
            string address = "";
            if (client_addr.ss_family == AF_INET) {
                char str[INET_ADDRSTRLEN];
                struct sockaddr_in *sa = (struct sockaddr_in*)&client_addr;
                inet_ntop(AF_INET, &sa->sin_addr, str, INET_ADDRSTRLEN);
                address = str;
            }
            else if (client_addr.ss_family == AF_INET6) {
                char str[INET6_ADDRSTRLEN];
                struct sockaddr_in6 *sa = (struct sockaddr_in6*)&client_addr;
                inet_ntop(AF_INET6, &sa->sin6_addr, str, INET6_ADDRSTRLEN);
                address = str;
            }

            
            spawnSearchThread(conn, address);
        }
    }
}

void SearchServer::spawnSearchThread(int connection, std::string address)
{
    pthread_t threadID;
    int r;
    
    /* we create a new SearchThread with "new" so its destructor doesn't get 
       called when we return from spawnSearchThread (since the thread may still
       be executing). It is the responsibility of threadFunc to free the
       SearchThread when it is done with it.
     */
    r = pthread_create(&threadID, NULL, SearchServer::threadFunc, 
                       (void*)new SearchThread(this->search, connection, 
                                               address, distType, linkageType, debug));
    
    if (r != 0) {
        cerr << "ERROR: unable to spawn SearchThread. Closing socket.\n";
        close(connection);
    }
    
    pthread_detach(threadID);
}


void *SearchServer::threadFunc(void *st)
{
    SearchThread *search = (SearchThread*)st;
    search->execute();
    delete search;
    
    return NULL;
}


void SearchServer::daemonizeServer()
{
    pid_t pid;
    pid_t sid;
    
    //already a daemon
    if ( getppid() == 1 ) {
        return;
    }
    
    // do the fork
    pid = fork();
    
    // check for fork() failure
    if (pid < 0) {
        throw SpellException(string(strerror(errno)));
    }
    
    // make parent process exit
    if (pid > 0) {
        exit(0);
    }
    
    /* we are now the child */
    
    // change umask
    umask(0);
    
    // setup a new session ID
    sid = setsid();
    if (sid < 0) {
        throw SpellException("Unable to setup new session ID in daemonize()");
    }
    
    // be a good daemon and get out of the directory we were started in so that
    // it does not become locked
    if ((chdir("/")) < 0) {
        throw SpellException("Unable to chdir in daemonize()");
    }
    
    // reopen standard file handles
    freopen( "/dev/null", "r", stdin);
    freopen( "/dev/null", "w", stdout);
    freopen( "/dev/null", "w", stderr);
    
}

