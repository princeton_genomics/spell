/*
 * ConfigFile.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <iostream>

#include "ConfigFile.h"
#include "SpellException.h"
#include "utilities.h"


ConfigFile::ConfigFile(char *filename)
{
    std::string f(filename);
    try {
        loadConfiguration(f);
    }
    catch (SpellException &e) {
        throw e;
    }
}

ConfigFile::ConfigFile(std::string filename)
{
    try {
        loadConfiguration(filename);
    }
    catch (SpellException &e) {
        throw e;
    }
}

void ConfigFile::loadConfiguration(std::string filename)
{
    std::string line;
    std::ifstream confFile(filename.c_str());
    
    if(confFile.is_open() && confFile.good()) {
        while (!confFile.eof()) {
            getline(confFile, line);
            if (line != "") {
                std::vector<std::string> tokens;
                
                utilities::tokenize(line, tokens, "\t");
                if (tokens.size() != 4) {
                    std::string mesg = "Error parsing config file ";
                    mesg = mesg + filename + "\n";
                    throw SpellException(mesg);
                }
                
                OrgConfig conf(tokens[0], tokens[1], tokens[2], tokens[3]);
                orgConfigs.push_back(conf);
            }
        }
    }
    else {
        // problem reading from file
        std::string mesg = "Error opening config file \"";
        mesg = mesg + filename +"\"\n";
        throw SpellException(mesg);
    }    
}

OrgConfig ConfigFile::getOrgConfig(unsigned int i)
{
    if (i >= orgConfigs.size()) {
        std::string mesg = "out of range error\n";
        throw SpellException(mesg);
    }
    return orgConfigs[i];
}

int ConfigFile::getNumOrganisms()
{
    return orgConfigs.size();
}
