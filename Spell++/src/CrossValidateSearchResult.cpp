/*
 * crossValidateSearchResult.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <map>
#include <fstream>

#include <boost/unordered_set.hpp>

#include "Organism.h"
#include "GeneFamily.h"
#include "SpellException.h"
#include "utilities.h"
#include "CrossValidateSearchResult.h"

using namespace std;


void CrossValidateSearchResult::writeFinalEvaluationStatistics(std::string filename, boost::unordered_set<GeneFamily*> &agnostic)
{
    
    Stats stats = finalEvaluationStatistics(agnostic);
    
    vector<float> &prec = stats.precision;
    vector<float> &rec  = stats.recall;
    vector<float> &spec = stats.specificity;
    vector<float> &acc  = stats.accuracy;
    
    ofstream ofs(filename.c_str());
    
    if(!ofs.is_open() || !ofs.good()) {
        string mesg = "Error opening ";
        mesg = mesg + filename + "\n";
        throw SpellException(mesg);
    }
    
    
    float avgPrec = calculateAveragePrecision(agnostic);
    float foldImp = avgPrec / ((float)query.size() / (float)(finalOrder.size() - agnostic.size()));
    
    ofs << "AP\t" << avgPrec << "\t" << foldImp << "\n";
    ofs << "Gene\tTP\tScore\tRank\tPrecision\tRecall\tSpecificity\tAccuracy\n";
    
    for (unsigned int i = 0; i < prec.size(); i++) {
        ofs << finalOrder[i]->toString() << "\t";
        if (utilities::vectorContains<GeneFamily*>(query, finalOrder[i])) {
            ofs << "1";
        }
        else if (agnostic.find(finalOrder[i]) != agnostic.end()) {
            ofs << "A";
        }
        else {
            ofs << "0";
        }
        ofs << "\t" << finalScores[i] << "\t" << i << "\t" << prec[i] << "\t";
        ofs << rec[i] << "\t" << spec[i] << "\t" << acc[i] << endl;
    }
    
}

void CrossValidateSearchResult::writeFinalEvaluationStatistics (std::string filename)
{
    boost::unordered_set<GeneFamily*> hs;
    try {
       writeFinalEvaluationStatistics(filename, hs); 
    }
    catch (SpellException &e) {
        throw e;
    }
}

void CrossValidateSearchResult::writeAverageDatasetWeights (std::string filename)
{
    
    ofstream ofs(filename.c_str());
    
    if(!ofs.is_open() || !ofs.good()) {
        string mesg = "Error opening ";
        mesg = mesg + filename + "\n";
        throw SpellException(mesg);
    }
    
    for (unsigned int i = 0; i < datasets.size(); i++) {
        ofs << datasets[i]->getFilename() << "\t" + datasets[i]->getOrganism()->getName() << "\t" <<  finalWeights[i] << endl;
    }

}



CrossValidateSearchResult::Stats CrossValidateSearchResult::finalEvaluationStatistics(boost::unordered_set<GeneFamily*> &agnostic)
{
    Stats stats;
    
    boost::unordered_set<GeneFamily*> qSet;
    for (unsigned int i = 0; i < query.size(); i++) {
        qSet.insert(query[i]);
    }
    
    //Find the intersection of the query set and the agnostic set (if any)
    boost::unordered_set<GeneFamily*> overlap;
    for (boost::unordered_set<GeneFamily*>::iterator it = qSet.begin(); it != qSet.end(); ++it) {
        if (agnostic.find(*it) != agnostic.end()) {
            overlap.insert(*it);
        }
    }
    
    //Initialize TP, FP, TN, and FN based on totals and the query and agnostic set sizes
    int total        = finalOrder.size();
    int ttp         = qSet.size();
    int ttn         = total - ttp - (agnostic.size() - overlap.size());
    int tp = 0;                //Initially there are no positives
    int tn = ttn;         //Initially all negatives are correct
    int fp = 0;         //Initially no positives
    int fn = ttp;         //Initially just the positives are incorrectly classified
    
    //Calculate the statistics as the pos/neg threshold is swept through the results
    for (unsigned int i = 0; i < finalOrder.size(); i++) {
        if (qSet.find(finalOrder[i]) != qSet.end()) {
            // was in query set
            tp++;
            fn--;
        }
        else if (agnostic.find(finalOrder[i]) == agnostic.end()) {
            // not in query set or agnostic set
            fp++;
            tn--;
        }

        stats.precision.push_back((float)tp / (float)(tp + fp));
        stats.recall.push_back((float)tp / (float)(tp + fn));
        stats.specificity.push_back((float)tn / (float)(tn + fp));
        stats.accuracy.push_back((float)(tp + tn) / (float)(tp + fp + tn + fn));
    }
    
    //Convex hull the precision
    for (unsigned int i = stats.precision.size() - 1; i > 0; i--) {
        if ((stats.precision[i] > stats.precision[i-1]) || (isnan(stats.precision[i-1]))) {
            stats.precision[i-1] = stats.precision[i];
        }
    }
    
    return stats;
}

float CrossValidateSearchResult::calculateAveragePrecision (boost::unordered_set<GeneFamily*> &agnostic)
{
    int numFound = 0;
    int numExamined = 0;
    boost::unordered_set<GeneFamily*> qSet;
    for (unsigned int i = 0; i < query.size(); i++) {
        qSet.insert(query[i]);
    }
    
    //Calculate precisions at the TPs
    vector<float> precision;
    for (unsigned int i = 0; i < finalOrder.size(); i++) {
        if (qSet.find(finalOrder[i]) != qSet.end()) {
            // is in qSet
            numFound++;
            numExamined++;
            precision.push_back((float)numFound / (float)numExamined);
        }
        else if (agnostic.find(finalOrder[i]) == agnostic.end()) {
            // not in agnostic
            numExamined++;
        }
    }
    
    //Sum the convex hull of the precisions
    float ap = precision.back();
    for (unsigned int i = precision.size()-1; i > 0; i--) {
        if (precision[i] > precision[i-1]) {
            precision[i-1] = precision[i];
        }
        ap += precision[i-1];
    }
    
    return ap / precision.size();
}
