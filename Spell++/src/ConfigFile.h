/*
 * ConfigFile.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <string>
#include <vector>

/**
 * Organism configuration, contains information about all of the files
 * Spell needs to load an Organism
 *
 */
class OrgConfig {
private:
    /// organismID a std::string Organism identifier (e.g. MMU for mouse)
    std::string organismID;
    
    /// path to the file containing Gene information for the Organism
    std::string geneFile;
    
    /// path to a file containing ExpressionDataset file names
    std::string datasetListFile;
    
    /// path to the directory containing all of the ExpressionDatasets
    std::string dataDir;

public:
    
    /**
     * OrgConfig constructor
     *
     * @param ID std::string identifier for the Organism
     * @param gene std::string path to Gene information file
     * @param data std::string containing path to the file with the list of ExpressionData file names
     * @param path std::string containing the path to the directory containing the ExpressionData files
     */
    OrgConfig(std::string ID, std::string gene, std::string data, 
              std::string path):organismID(ID),geneFile(gene),datasetListFile(data),dataDir(path){}

    /**
     * get the string ID for the Organism that this configuration is for
     *
     * @returns OrgConfig::organismID
     */
    std::string getOrgID()
    {
        return organismID;
    }
    
    /**
     * get the path to the Gene information file for this Organism
     *
     * @returns OrgConfig::geneFile
     */
    std::string getGeneFile()
    {
        return geneFile;
    }
    
    /**
     * get the path to the file containing the list of ExpressionDataset files 
     * to load for this Organism
     *
     * @returns OrgConfig::datasetFileList
     */
    std::string getDatasetListFile()
    {
        return datasetListFile;
    }
    
    /**
     * get the path to the ExpressionDataset file directory
     *
     * @returns OrgConfig::dataDir
     */
    std::string getDataDir()
    {
        return dataDir;
    }
};


/**
 *  Load and store the information from a Spell instance configuration file
 *
 *
 */
class ConfigFile {

private:
    /// vector of OrgConfigs, which is the configuration information for each 
    /// Organism contained in the ConfigFile
    std::vector<OrgConfig> orgConfigs;
    
public:
    /**
     * construct an uninitialized ConfigFile object
     */
    ConfigFile(){};
    
    /**
     * construct a ConfigFile object and load the config information from a file
     *
     * @param filename a C-string containing the path to the configuration file
     * @throws SpellException
     */
    ConfigFile(char *filename);
    
    /**
     * construct a ConfigFile object and load the config information from a file
     *  
     * @param filename std::string containign the path to the configuration file
     * @throws SpellException
     */
    ConfigFile(std::string filename);
    
    /** 
     * load the configuration information from a file
     *
     * @param filename a std::string containing the path to the Spell configuration file
     * @throws SpellException
     *
     */
    void loadConfiguration(std::string filename);
    
    /**
     * get the OrgConfig (organism configuration) for the ith ortanism in the ConfigFile
     *
     * @param i integer index for the configuration to return
     * @throws SpellException if the index is not valid
     */
    OrgConfig getOrgConfig(unsigned int i);
    
    /**
     * get the number of Organisms we loaded configuration information for
     *
     * @returns int containg the number of organism configurations loaded
     */
    int getNumOrganisms();
    
};

#endif
