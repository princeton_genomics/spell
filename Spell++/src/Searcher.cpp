/*
 * Searcher.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <algorithm>

#include "Searcher.h"
#include "SearcherThreadManager.h"

using namespace std;


Searcher::~Searcher()
{
    // if the searcher has a thread manager delete it
    if (threadMan) {
         delete threadMan;
    }
}

SearchResult Searcher::querySearch(std::vector<int> geneIDs, 
                         std::vector<int> datasetIDs, unsigned int distType,
                         unsigned int linkageType, unsigned int maxGenes)
{

    vector<GeneFamily*> families = homology->getFamilies();
    vector<ExpressionDataset*> datasets = homology->getDatasetsByID(datasetIDs);
    vector<GeneFamily*> query = homology->getGeneFamiliesByGeneID(geneIDs);

    int numResults;

    //Determine dataset weights
    vector<float> weights(datasets.size());
    int numWeighted = 0;

    numWeighted = calculateDatasetWeights(weights, datasets, query, distType, linkageType);

    // We only want to consider the datasets which contain at least one
    // of the genes in the query set, and at least 5 conditions.
    // Getting good correlation statistics relies on having 5 or more
    // conditions in the dataset.
    vector<ExpressionDataset*> tmpDatasets = vector<ExpressionDataset*>();
    vector<float> tmpWeights = vector<float>();

    for (int d = 0; d < datasets.size(); d++) {
      if ((!isnan(weights[d])) && (datasets[d]->getNumConds() >= 5)) {
            tmpWeights.push_back(weights[d]);
            tmpDatasets.push_back(datasets[d]);
        }
        else {
          if (weights[d] > 0.0)
            numWeighted--;
        }
    }
    datasets = tmpDatasets;
    weights = tmpWeights;

    //If very few datasets showed significant correlation, 
    //weight all datasets the same
    if (numWeighted != 0 && numWeighted < 3) {
        for (unsigned int i = 0; i < weights.size(); i++) {
            weights[i] = 1.0f;
        }
        numWeighted = datasets.size();
    }

    vector<SingleResult> singleResults;

    if (maxThreads > 1) {
        try {
            singleResults = threadMan->scoreGeneFamiliesThreaded(families, datasets, query, weights, numWeighted, distType, linkageType); 
        }
        catch (SpellException &e) {
            throw e;
        }

    }
    else {
        float score;
        for (unsigned int f = 0; f < families.size(); f++) {
            
            score =  scoreGeneFamily(families[f], datasets, query, weights,
                                     numWeighted, distType, linkageType);
            
            singleResults.push_back(SingleResult(families[f], score)); 
        }
    }


    
    // sort singleResults in decending order by score (highest score is at
    // singleResults[0])
    sort(singleResults.begin(), singleResults.end(), SingleResult::sortCompDescending);
    
    // limit the number of Genes to return if necessary
    if (maxGenes > 0) {
        numResults = min((unsigned int)singleResults.size(), maxGenes);
    }
    else {
        numResults = singleResults.size();
    }
    
    
    // use singleResults to build the vector of Gene IDs and scores that 
    // we return as part of the final result
    vector<GeneFamily*> order;
    vector<float> scores;
    for (int i = 0; i < numResults; i++) {
        order.push_back(singleResults[i].getFamily());
        scores.push_back(singleResults[i].getScore());
    }

    SearchResult result(query, datasets);
    
    result.setFinalOrder(order);
    result.setFinalScores(scores);
    result.setFinalWeights(weights);

    if (query.size() == 1) {
        // We have a single gene query.  Make a recursive call with the 
        // original query gene and the top two correlated genes.
        // We'll use the gene scores from the first pass, but the dataset
        // weights from the second pass for those datasets that were in
        // the first pass. (The second pass might have more datasets
        // used.)
        // 
        assert(singleResults.size() >= 3);
        vector<int>newIDs = geneIDs;
        for (int i=1; i<3; i++) {
            GeneFamily* family = singleResults[i].getFamily();
            std::vector<int> IDs = family->getGeneIDs();
            for (int j=0; j<IDs.size(); j++)
                newIDs.push_back(IDs[j]);
        }
        SearchResult pass2 = querySearch(newIDs, datasetIDs, distType,
                                         linkageType, maxGenes);
        result.setFinalWeightsFromPass2(pass2);
    }

    return result;
}



float Searcher::scoreGeneFamily(const GeneFamily *fam, 
                                const std::vector<ExpressionDataset*> &datasets, 
                                const std::vector<GeneFamily*> &query, 
                                const std::vector<float> &weights, unsigned int numWeighted, 
                                unsigned int distType, unsigned int linkageType)
{
    float score = 0.0f;
    int dsetsUsed = 0;
    bool singleGeneQuery = (query.size() == 1);

    for (unsigned int d = 0; d < datasets.size(); d++) {
        float wgt = weights[d];
        if (wgt > 0) {
            float dsetScore = 0.0f;
            int numPairs = 0;
            
            //If using orthology groups
            if (homology->usesOrthology() == true) {
                for (unsigned int i = 0; i < query.size(); i++) {
                    float val = datasets[d]->score(fam, query[i], distType, linkageType);
                    if (!isnan(val)) {
                        dsetScore += val;
                        numPairs++;
                    }
                }
            }
            else { // not using orthology groups
                for (unsigned int i = 0; i < query.size(); i++) {
                    
                    float val = datasets[d]->score(*(fam->begin()), 
                                                   *(query[i]->begin()),
                                                   distType);
                    if(!isnan(val)) {
                        dsetScore += val;
                        numPairs++;
                    }
                }
            }
            // For singleGeneQueries, we only use the weight to order
            // the datasets, not to calculate the scores.
            if (singleGeneQuery)
                wgt = 1.0;

            if (numPairs > 0) {
                score += wgt * (dsetScore / (float)numPairs);
                dsetsUsed++;
            }
        }
    }
    
    // Through V2.0.1, the score was divided by the sum of the weights of the
    // datasets in which it was found, forming a weighted average over those
    // datasets.  That resulted in non-intuitive displays, as a gene that
    // was not represented in the heavily weighted datasets, but was highly
    // correlated in relatively unimportant datasets, would be ranked very
    // highly.
    // Now we divide the score by 100 (the total weights of all the datasets)
    // which in effect causes us to treat the gene as "uncorrelated" (score
    // of zero) in the datasets in which it doesn't appear.
    score /= 100;

    if (dsetsUsed < min((float)numWeighted, 
                        max((0.1f * datasets.size()), 4.0f))) {
        score = 0.0f;
    }
    return score;
}


int Searcher::calculateDatasetWeights(std::vector<float> &weights, 
                                      const std::vector<ExpressionDataset*> &datasets,
                                      const std::vector<GeneFamily*> &query, 
                                      unsigned int distType, unsigned int linkageType)
{
    
    int numWeighted = 0;
    
    for (unsigned int d = 0; d < datasets.size(); d++) {
        
        weights[d] = 0.0f;
        int numPairs = 0;

        if (query.size() == 1) {
            // Weights for a single gene query are computed differently.
            // They are the standard deviation of the gene's expression
            // in each dataset.
            if (homology->usesOrthology() == true) {
                // Don't really know what to do for gene families with one
                // single gene query.
                throw (1);
            }
            else {
                Gene *g = *(query[0]->begin());
                float score = datasets[d]->score(g);
                if (!isnan(score))
                    numWeighted++;
                weights[d] = score;
            }
        }
        else {
            // weight each dataset based on coexpression of query gene familes
            for (unsigned int i = 0; i < query.size(); i++ ) {

                //If orthology information is being used
                if (homology->usesOrthology() == true) {
                    GeneFamily *fam = query[i];
                    for (unsigned int j = i+1; j < query.size(); j++) {
                        float score = datasets[d]->score(fam, query[j], distType, linkageType);
                        if (!isnan(score)) {
                            numPairs++;
                            weights[d] += score;
                        }
                    }
                }
                else { //not using orthology information
                    Gene *gi = *(query[i]->begin());

                    for (unsigned int j = i+1; j < query.size(); j++) {
                        Gene *gj = *(query[j]->begin());
                        float score = datasets[d]->score(gi, gj, distType);
                        if (!isnan(score)) {
                            numPairs++;
                            weights[d] += score;
                        }
                    }
                }
            }
            if (numPairs == 0)
                weights[d] == numeric_limits<float>::quiet_NaN();
        }
        
        if (numPairs > 0 && weights[d] > 0) {
            weights[d] /= (float)numPairs;
            numWeighted++;
        }
    }
    
    return numWeighted;
}

void Searcher::initThreadManager(unsigned int maxThreads) {
    if (threadMan) {
        delete threadMan;
        threadMan = NULL;
    }
    
    this->maxThreads = maxThreads;
    if (maxThreads > 1) {
        threadMan = new SearcherThreadManager(this, maxThreads);
    }
}

