/*
 * SearcherThreadManager.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ExpressionDataset.h"
#include "GeneFamily.h"
#include "Searcher.h"
#include <pthread.h>

#include <iostream>

/**
 * class to manage threaded actions for the Searcher class
 *
 * @note this is a friend of the Searcher class
 *
 */
class SearcherThreadManager {
    
private:
    
    /**
     * a struct used to pass data to a function spawned in a thread by 
     * pthread_create
     *
     *
     */
    struct ScoreThreadTask {
        /// starting family index
        unsigned int famStart;
        
        /// ending family index
        unsigned int famEnd;
        
        /// number of weighted datasets
        int numWeighted;
        
        /// distance measurement type
        unsigned int distType; 
        
        /// linkage type used
        unsigned int linkageType;
        
        /// mutex used when merging results from all threads
        pthread_mutex_t *mutex;
        
        /// pointer back to the thread manager that called pthread_create
        SearcherThreadManager *manager;
        
        /// pointer to the families vector
        std::vector<GeneFamily*> *families;
        
        /// pointer to the results vector
        std::vector<Searcher::SingleResult> *results;
        
        /// pointer to the dataset vector
        std::vector<ExpressionDataset*> *datasets;
        
        /// pointer to the query vector
        std::vector<GeneFamily*> *query;
        
        /// pointer to the vector of dataset weights
        std::vector<float> *weights;         
    };
    
    /// maximum number of threads this SearcherThreadManager should use. 
    unsigned int maxThreads;
    
    /// pointer to the Searcher that owns this SearchThreadManager
    Searcher *searcher;
    
    /**
     * thread function, used to pass to pthread_create. This must be delcared to 
     * take a void pointer as an argument and return a void pointer.  This must 
     * also be a static function.
     *
     * @param task a pointer to a ScoreThreadTask struct cast as a void pointer
     * @returns NULL
     *
     */
    static void *scoreGeneFamiliesThreadFunc(void *task);
    
public:
    
    /**
     * construct a new SearcherThreadManager
     *
     * @param s a pointer to the Searcher that owns this SearcherThreadManager
     * @param maxThreads the maximum number of threads to use
     *
     */
    SearcherThreadManager(Searcher *s, unsigned int maxThreads):maxThreads(maxThreads),searcher(s){}
    
    
    /**
     * compute scores for all gene families using threads
     *
     * @param families a reference to the vector with pointers to all gene families
     * @param datasets a reference to the vector containg pointers to the 
     *        ExpressionDatasets indicated in the query
     * @param query a reference to the query vector
     * @param weights a reference to the vector of dataset weights
     * @param numWeighted the number of datasets that were weighted
     * @param distType the distance measure used
     * @param linkageType linkage type to use for correlation between genes in a family
     *
     * @returns a vector of SingleResult objects, each of which contains a score
     *   for a query gene
     */
    std::vector<Searcher::SingleResult> scoreGeneFamiliesThreaded
        (std::vector<GeneFamily*> &families, 
         std::vector<ExpressionDataset*> &datasets, 
         std::vector<GeneFamily*> &query, std::vector<float> &weights, 
         int numWeighted, unsigned int distType, unsigned int linkageType);
};
