/*
 * SearchResult.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <sstream>
#include <iomanip>
#include <assert.h>
#include "GeneFamily.h"

#include "SearchResult.h"


#include <ostream>
#include <fstream>

using namespace std;

string SearchResult::toString()
{
    ostringstream ss;

    ss << fixed << setprecision(1);

    // we have weights for the datasets, we'll sort them before writing them
    // to the string stream
    ///XXX should we be sorting the datasets in the Searcher::querySearch() method?
    
    // use a vector of DatasetResult objects to sort
    vector<DatasetResult> datasetResults;
    float datasetSum = 0;

    // fill the vector
    for (unsigned int i = 0; i < datasets.size(); i++) {
        datasetResults.push_back(DatasetResult(datasets[i], finalWeights[i]));
        datasetSum += finalWeights[i];
    }

    // If there were no datasets weighted because the query genes weren't
    // represented in the datasets, return the null response.
    if (datasetSum == 0.)
        return "\n\n";

    // sort decending order by weight
    sort(datasetResults.begin(), datasetResults.end(), DatasetResult::sortCompDescending);

    // now we can write them out to the string stream in decending order

    // if the query size is one then we don't want to show the weights;
    // they're only used to order the datasets. 
    // write the dataset IDs and use "--" for the weight
    for (unsigned int i = 0; i < datasets.size(); i++) {
        ss << datasetResults[i].getDataset()->getID();
        if (query.size() == 1) {
            ss << ":--";
        }
        else {
            ss << ":" << datasetResults[i].getScore() / datasetSum * 100 << "%";
        }

        if (i == datasets.size() - 1) {
            ss << "\n";
        }
        else {
            ss << ",";
        }
    }
        
    
    // we have the genes in the proper order, formatGeneScore(i) will 
    // use finalOrder[i] and finalScore[i] to format the Gene ID and score
    for (unsigned int i = 0; i < finalOrder.size(); i++) {
        ss << formatGeneScore(i);
        if (i == finalOrder.size() - 1) {
            ss << "\n";
        }
        else {
            ss << ",";
        }
    }

    // return the string from the string stream
    return ss.str();
}


string SearchResult::formatGeneScore(unsigned int i)
{
    // check for a logic error in the program, we want to catch this
    // during development
    assert(i < finalOrder.size());    
    
    ostringstream ss;
    
    //translate from family to gene IDs
    GeneFamily *family = finalOrder[i];
    std::vector<int> IDs = family->getGeneIDs();
    if (IDs.size() > 1) {
        ss << "{";
        for (int j=0; j < IDs.size(); j++) {
            ss << IDs[j];
            
            if (j < IDs.size()-1) {
                ss << ",";
            }
            else {
                ss << "}";
            }
        }
    }
    else {
        ss << IDs[0];
    }
    
    //append score and return the resulting string
    ss << ":" << fixed << setprecision(1) << sqrt(finalScores[i]);
    return ss.str();
}

void SearchResult::setFinalWeightsFromPass2(SearchResult& pass2)
{
    // We will set the weights for our datasets bsaed on the weights
    // determined in pass2.
    for (int i=0; i<datasets.size(); i++) {
        int dsID = datasets[i]->getID();
        for (int j=0; j<pass2.datasets.size(); j++) {
            if (dsID == pass2.datasets[j]->getID()) {
                finalWeights[i] = pass2.finalWeights[j];
                break;
            }
        }
    }
}
