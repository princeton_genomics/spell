/*
 * Gene.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef GENE_H
#define GENE_H

#include <string>
#include <set>

class Organism;
class GeneFamily;

/**
 *  The Gene class. This contains information about a single gene, including 
 *  the name of the gene, the integer ID, any aliases for the gene, and 
 *  references to the corresponding organism and gene families.
 */
class Gene {
    
private:
    ///The name of the gene
    std::string name;
    
    ///Pointer to the Organism object that this Gene belongs to
    Organism *org;
    
    ///All aliases of this Gene
    std::set<std::string> aliases;
    
    ///Integer ID corresponds to the ID used by the web front-end to reference a Gene
    int id;
    
    ///Pointer to the GeneFamily object that this Gene belongs to
    GeneFamily *family;
    
public:
    
    /**
     * default constructor. This constructor creates an uninitialized Gene.
     *
     */
    Gene(){}
    
    /**
     * constructor to initialize a basic Gene object
     *
     * @param org pointer to an Organism object that this Gene belongs to
     * @param id integer ID, corresponds to the ID used by the front end
     * @param name Gene name.
     *
     */
    Gene(Organism *org, int id, std::string name):name(name),org(org),id(id)
    {
        family = NULL;
    }
    
    /**
     * get the integer ID for this Gene
     *
     * @return int Gene::id
     *
     */
    int getID() const
    {
        return id; 
    }
    
    /**
     * get the name of this Gene
     *
     * @return std::string Gene::name
     */
    std::string getName() const
    {
        return name;
    }

    /**
     * set the GeneFamily for this Gene
     *
     * @param family pointer to the GeneFamily that this Gene belongs to
     *
     */
    void setFamily(GeneFamily *family) 
    {
        this->family = family;
    }
    
    /**
     * get the GeneFamily for this Gene
     *
     * @return pointer to the GeneFamily that this Gene belongs to
     *
     */
    GeneFamily *getFamily() const
    {
        return family;
    }
    
    /**
     * get the Organism this Gene belongs to
     *
     * @return pointer to the Organism that this Gene belongs to
     *
     */
    Organism *getOrganism()  
    {
        return org;
    }
    
};

#endif
