/*
 * SearcherThreadManager.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <pthread.h>
#include <iostream>

#include "SearcherThreadManager.h"


using namespace std;

vector<Searcher::SingleResult> SearcherThreadManager::scoreGeneFamiliesThreaded
    (std::vector<GeneFamily*> &families, std::vector<ExpressionDataset*> &datasets, 
     std::vector<GeneFamily*> &query, std::vector<float> &weights, int numWeighted, 
     unsigned int distType, unsigned int linkageType)
{
    unsigned int numThreads;
    unsigned int familiesPerThread;
    unsigned int remainder;
    unsigned int next;
    unsigned int r;
    ScoreThreadTask *tasks;
    vector<Searcher::SingleResult> results;
    pthread_mutex_t resultMutex = PTHREAD_MUTEX_INITIALIZER;
    
    numThreads = min(this->maxThreads, (unsigned int)families.size());
    familiesPerThread = families.size() / numThreads;
    remainder = families.size() % numThreads;
    
    tasks = new ScoreThreadTask[numThreads];
    
    // setup the task structs.
    next = 0;
    for (unsigned int i = 0; i < numThreads; i++) {
        
        // common setup
        tasks[i].manager     = this;
        tasks[i].mutex       = &resultMutex;
        tasks[i].results     = &results;
        tasks[i].families    = &families;
        tasks[i].datasets    = &datasets; 
        tasks[i].query       = &query; 
        tasks[i].weights     = &weights;   
        tasks[i].numWeighted = numWeighted;
        tasks[i].distType    = distType; 
        tasks[i].linkageType = linkageType;
        
        // task specific setup
        
        // start index for familes
        tasks[i].famStart = next;
                
        // end index for families
        tasks[i].famEnd = next + (familiesPerThread - 1);
        
        // if the number of families didn't divide evenly by the number 
        // of threads then spread the remainder out
        if (remainder > 0) {
            tasks[i].famEnd++;
            --remainder;
        }
        
        // set next to start of next task
        next = tasks[i].famEnd + 1; 
    }
    
    
    //  explicitly create the pthread as joinable 
    // (default on fully POSIX complient implementations)
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_t threads[numThreads];
    
    // launch each task in its own pthread
    for (unsigned int i = 0; i < numThreads; i++) {
        r = pthread_create(&threads[i], &attr, 
                           SearcherThreadManager::scoreGeneFamiliesThreadFunc, 
                           (void*)&tasks[i]);
        
        
        if (r != 0) {
            ostringstream oss;
            oss << "ERROR creating thread, pthread_create returned " << r;
            throw SpellException(oss.str());
        }
        
    }
    pthread_attr_destroy(&attr);
    
    // wait for all pthreads to complete
    for (unsigned int i = 0; i < numThreads; i++) {
        r = pthread_join(threads[i], NULL);
        
        // pthread_join returned an error code. This shouldn't happen, maybe a 
        // bug caused the thread to crash?
        if (r != 0) {
            cerr << "WARNING: pthread_join() returned " << r << " for thread " << i << endl;
        }
    }
    
    pthread_mutex_destroy(&resultMutex);

    delete[] tasks;
    return results;
}


void *SearcherThreadManager::scoreGeneFamiliesThreadFunc(void *task)
{
    ScoreThreadTask *t = (ScoreThreadTask*)task;
    Searcher *searcher = t->manager->searcher;
    vector <Searcher::SingleResult> my_results;
    float score;   
    
    // iterate over my portion of the familes vector and score each 
    // family against the query families
    for (unsigned int i = t->famStart; i <= t->famEnd; i++) {
        
        score = searcher->scoreGeneFamily((*t->families)[i], *t->datasets, 
                                          *t->query, *t->weights, 
                                          t->numWeighted, t->distType, t->linkageType);
    
        my_results.push_back(Searcher::SingleResult((*t->families)[i], score));
    }
    
    // add my_results to the shared results vector
    pthread_mutex_lock(t->mutex);
    for (unsigned int i = 0; i < my_results.size(); i++) {
        t->results->push_back(my_results[i]);
    }
    pthread_mutex_unlock(t->mutex);
            
    return NULL;
}

