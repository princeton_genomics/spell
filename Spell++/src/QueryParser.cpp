/*
 * QueryParser.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <limits>
#include <string>

#include "SpellException.h"
#include "QueryParser.h"
#include "utilities.h"

using namespace std;


void QueryParser::parse(std::string message)
{
    vector<string> tokens;
    vector<string> datasetIdStrings;
    vector<string> geneIdStrings;
    vector<string> optionalParameters;
    vector<string> parameter;
    
    // clear out datasetIDs and geneIDs in case this QueryParser is being reused
    datasetIDs.clear();
    geneIDs.clear();
    
    //reset maxGenes
    maxGenes = -1; // negative value indicates that maxGenes is not set...
    
    utilities::tokenize(message, tokens, "\t");
    
    if (tokens.size() > 3) {
        throw SpellException("Invalid query format");
    }
    
    utilities::tokenize(tokens[0], datasetIdStrings, ",");
    utilities::tokenize(tokens[1], geneIdStrings, ",");
    
    for (unsigned int i = 0; i < datasetIdStrings.size(); i++) {
        datasetIDs.push_back(utilities::from_string<int>(datasetIdStrings[i]));
    }
    
    for (unsigned int i = 0; i < geneIdStrings.size(); i++) {
        geneIDs.push_back(utilities::from_string<int>(geneIdStrings[i]));
    }
    
    // tokens[2]: optional parameters
    if (tokens.size() == 3) {
        // this query contains one or more optional parameters
        
        //optional parameters are comma delimited, split them up
        utilities::tokenize(tokens[2], optionalParameters, ",");
        
        // for each optional parameter...
        for (unsigned int i = 0; i < optionalParameters.size(); i++) {
            
            // optional parameters are key=value pairs, split on the "="
            utilities::tokenize(optionalParameters[i], parameter, "=");
            
            // check for malformed parameter...
            if (parameter.size() != 2) {
                string mesg = "Syntax Error: ";
                mesg += optionalParameters[i];
                throw InvalidParameterException(mesg);
            }
            
            // see if the parameter name matches any that we expect
            if (!parameter[0].compare("MAX_GENES")) {
                try {
                    maxGenes = utilities::from_string<int>(parameter[1]);
                }
                catch (SpellException &e) {
                    string mesg = "Invalid MAX_GENES value: \"";
                    mesg += parameter[1] + "\"";
                    throw InvalidParameterException(mesg);
                }
                
                if (maxGenes < 0) {
                    string mesg = "Invalid MAX_GENES value: \"";
                    mesg += parameter[1] + "\"";
                    throw InvalidParameterException(mesg);
                }

            }
            else {
                // unknown parameter...
                string mesg = "Unknown Parameter: \"";
                mesg += parameter[0] + "\"";
                throw InvalidParameterException(mesg);
            }
        }
    }
   
    
}
