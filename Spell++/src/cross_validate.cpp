/*
 * CrossValidateSearch.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <fstream>
#include <boost/unordered_set.hpp>
#include <errno.h>

#include "CrossValidateSearchResult.h"
#include "CrossValidateSearch.h"
#include "ConfigFile.h"
#include "SpellException.h"
#include "ExpressionDataset.h"
#include "Homology.h"
#include "Searcher.h"
#include "utilities.h"
#include "config.h"

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
#include <unistd.h>
#endif

void printUsage();

using namespace std;

#define DEFAULT_MAX_THREADS 1
#define DEFAULT_LINKAGE_TYPE linkage_type::MEDIAN
#define DEFAULT_DISTANCE_TYPE distance_type::ZSCORE2


int main(int argc, char **argv)
{
    bool usage = false;
    bool useWeights = true;
    bool storePartialResults = false;
    
    string orthologyFile = "";
    string argstr;
    string agnosticGeneFile;
    string geneSetPath;
    string resultPath;
    string queryFiles;
    
    unsigned int maxThreads  = DEFAULT_MAX_THREADS;
    unsigned int cvSize;
    unsigned int numSamples;
    unsigned int distType = DEFAULT_DISTANCE_TYPE;
    unsigned int linkType = DEFAULT_LINKAGE_TYPE;
    
    boost::unordered_set<GeneFamily*> agnostic;
    
    ConfigFile confFile;
    Homology homology;
    CrossValidateSearchResult result;
    
    // getopt/getopt_long variables
    int c;
    string optstr;
    

    

    
    
    
    

#ifdef ENABLE_ORTHOLOGY    
    optstr = "aht:l:o:";  // include orthology related command line options
#else
    optstr = "aht:";      // exclude orthology related command line options
#endif
    
#ifdef HAVE_GETOPT_H
    //we have getopt.h so getopt_long() is available
    
    struct option long_options[] =
    {
        //don't use the getopt_long "flag" syntax since we share the 
        //swtich statement with the getopt() impelmentation
        {"agnostic",        required_argument, 0, 'a'},
        {"help",            no_argument,       0, 'h'},
        {"max_threads",     required_argument, 0, 't'},
#ifdef ENABLE_ORTHOLOGY 
        {"linkage_type",    required_argument, 0, 'l'},
        {"orthology_file",  required_argument, 0, 'o'},
#endif
        {0, 0, 0, 0}
    };
    
    // passed to getopt_long, but we do not use (sharing while() body with getopt)
    int option_index;
    
    //setup while loop using getopt_long
    while ((c = getopt_long(argc, argv, optstr.c_str(), long_options, &option_index)) != -1)
#else 
    // no getopt.h so no getopt_long(), use getopt() instead
    while ((c = getopt(argc, argv, optstr.c_str())) != -1)
#endif
    {
        // in the shared while loop body for getopt and getopt_long
        
        //shared switch statement for getopt() and getopt_long()
        switch (c) {
            case 'a':
                agnosticGeneFile = optarg;
                break;
            case 'h':
                printUsage();
                return 0;
            case 't':
                argstr = optarg;
                try {
                    maxThreads = utilities::from_string<unsigned int>(argstr);
                }
                catch (SpellException &e) {
                    cerr << "ERROR: Invalid number of threads: " << optarg << ".\n";
                    usage = true;
                }
                break;
#ifdef ENABLE_ORTHOLOGY 
            case 'l':
                argstr = optarg;
                try {
                    int linkArg = utilities::from_string<unsigned int>(argstr);
                    
                    if (linkArg == 0) {
                        linkType = linkage_type::MIN;
                    }
                    else if (linkArg == 1) {
                        linkType = linkage_type::MAX;
                    }
                    else if (linkArg == 2) {
                        linkType = linkage_type::MEAN;
                    }
                    else if (linkArg == 3) {
                        linkType = linkage_type::MEDIAN;
                    }
                    else if (linkArg == 4) {
                        linkType = linkage_type::SIGNIFICANT_MEDIAN;
                    }
                    else {
                        cerr << "ERROR: Invalid linkage type: " << linkArg << ".\n";
                        usage = true;
                    }

                }
                catch (SpellException &e) {
                    cerr << "ERROR: Invalid linkage type: " << optarg << ".\n";
                    usage = true;
                }
                
                break;
            case 'o':
                orthologyFile = optarg;
                break;
#endif
            case '?':
                // getopt_long or getopt already printed an error message.
                usage = true;
                break;
            default:
                usage = true;
        }
    }
            
    
    //by default getopt/getopt_long permutes argv so the non-options are at the end
    //we'll increment argv to skip all the options we've already parsed. 
    //(and skip over the executable name element in argv[0])
    argc -= optind;
    argv += optind;
    
    // after parsing the command line options there should be our required arguments left
    if (argc != 6) {
        cerr << endl << "Invalid number of required command line options\n";
        printUsage();
        return EINVAL;
    }
    
    
    
    //parse required numeric types
    try {
        argstr = argv[3];
        cvSize = utilities::from_string<unsigned int>(argstr);
    }
    catch (SpellException &e) {
        cerr << "ERROR: Invalid query size: " << argv[3] << ".\n";
        usage = true;
    }
    
    try {
        argstr = argv[4];
        numSamples = utilities::from_string<unsigned int>(argstr);
    }
    catch (SpellException &e) {
        cerr << "ERROR: Invalid number of samples: " << argv[4] << ".\n";
        usage = true;
    }
    
    
    //If we ran into a problem parsing options or required arguments, then 
    //print usage and exit
    if (usage) {
        cerr << endl << "One or more errors parsing command line options\n";
        printUsage();
        return EINVAL;
    }
    
    geneSetPath = argv[5];
    resultPath  = argv[2];
    queryFiles  = argv[1];
    

    
    // load the configuration file
    try {
        confFile = ConfigFile(argv[0]);
    }
    catch (SpellException &e) {
        cerr << "ERROR loading config file:\n";
        cerr << "\t" << e.what() << endl;
        return 1;
    }
    
    // load each organism in the config file
    for (unsigned int i = 0; i < confFile.getNumOrganisms(); i++) {
        cout << "Loading " << confFile.getOrgConfig(i).getOrgID() << "... " << flush;
        try {
            // get this organism's configuration information
            OrgConfig orgConf = confFile.getOrgConfig(i);
            string orgID = orgConf.getOrgID();
            
            // add the Organism to the homology
            homology.addOrganism(orgID, orgConf.getGeneFile());
            cout << homology.getOrganism(orgID)->getNumGenes() << " genes loaded\n";
            
            // load the datasets for the organism
            homology.getOrganism(orgID)->loadDatasets(orgConf.getDatasetListFile(), 
                                                      orgConf.getDataDir());
            
            // add orthology information (empty string, "", indicates no orthology info)
            homology.addGeneFamilies(orthologyFile);
        }
        catch (SpellException &e) {
            cerr << endl << "Error loading " << confFile.getOrgConfig(i).getOrgID() <<":\n";
            cerr << e.what() << endl;
            return 1;
        }
        
    }
    
    // create a Searcher object
    CrossValidateSearch searcher(&homology, maxThreads);
    
    
    // load the agnostic set
    if (!agnosticGeneFile.empty()) {
        
        
        try {
            ifstream fs(agnosticGeneFile.c_str());
            string line;
            
            cout << "Loading agnostic gene information\n";
            
            if(!fs.is_open() || !fs.good()) {
                string mesg = "Error opening agnostic gene file \"";
                mesg = mesg + agnosticGeneFile + "\"\n";
                throw SpellException(mesg); 
            }
            
            while (getline(fs, line))  {
                GeneFamily *fam = homology.findFamily(line);
                if (fam != NULL) {
                    agnostic.insert(fam);
                }
            }
            
            cout << "done.\n";
            
        }
        catch (SpellException &e) {
            cerr << "Error loading agnostic gene information:\n"
            << "  " << e.what() << endl;
            return 1;
        }
    }
    else {
        cout << "WARNING: no agnostic list specified, assuming no genes are agnostic\n";
    }

    // for each query file
    try {
        
        string line;
        string fullpath;
        string filename;
        vector<string> tokens;
        ifstream fs(queryFiles.c_str());
        ifstream queryGeneFs;
        
        if(!fs.is_open() || !fs.good()) {
            string mesg = "Error opening query file \"";
            mesg = mesg + argv[1] + "\"\n";
            throw SpellException(mesg); 
        }
        
        while (getline(fs, line))  {
            boost::unordered_set<GeneFamily*> query;
            vector<GeneFamily*> familyVec;

            utilities::tokenize(line, tokens, "\t");
            if (tokens.size() > 1) {
                filename = tokens[0];
            }
            fullpath = geneSetPath + "/" + filename;
            
            cout << "Cross validating search results for " << filename << "...\n";
            flush(cout);
            
            //read in all of the query genes
            queryGeneFs.open(fullpath.c_str());
            if(!queryGeneFs.is_open() || !queryGeneFs.good()) {
                string mesg = "Error opening query gene file \"";
                mesg = mesg + fullpath + "\"\n";
                throw SpellException(mesg); 
            }
            
            //reuse the line string here
            while (getline(queryGeneFs, line)) {
                //try to map this line to a GeneFamily
                vector<GeneFamily*> families = homology.findAllFamilies(line);
                for (int i = 0; i < families.size(); i++) {
                    query.insert(families[i]);
                }
                
            }
            
            for (boost::unordered_set<GeneFamily*>::const_iterator it = query.begin(); 
                 it != query.end(); ++it) {
                familyVec.push_back(*it);
            }
            
            if (familyVec.size() > cvSize) {
                
                
                // perform the cross validated search
                try {
                    result = searcher.search(familyVec, cvSize, numSamples, 
                                             distType, linkType, useWeights, 
                                             storePartialResults); 
                    
                    //write the results out to a file
                    try {
                        fullpath = resultPath + "/" + filename + ".results";
                        cout << "   Writing final evaluation statistics to " << fullpath << endl;
                        result.writeFinalEvaluationStatistics(fullpath, agnostic);
                    }
                    catch (SpellException &e) {
                        cerr << "Error writing final evaluation statistics:\n";
                        cerr << "\t" << e.what() << endl;
                        return 1;
                    }
                    
                    try {
                        fullpath = resultPath + "/" + filename + ".weights";
                        cout << "   Writing average dataset weights to " << fullpath << endl;
                        result.writeAverageDatasetWeights(fullpath);
                    }
                    catch (SpellException &e) {
                        cerr << "Error writing average dataset weights:\n";
                        cerr << "\t" << e.what() << endl;
                        return 1;
                    }
                    cout << "done.\n\n";
                    
                }
                catch (SpellException &e) {
                    cout << "WARNING:       Error in cross validation search, skipping output\n\n";
                    cerr << e.what() << endl;
                }
                 
                
                
                
            }
            else {
                cout << "skipping, too few query genes\n\n";
            }
    
            queryGeneFs.close();
            
        }
        
    }
    catch (SpellException &e) {
        cerr<< "ERROR: " << e.what() << endl;
        return 1;
    }
    
    return 0;
}

void printUsage()
{
    cerr << "USAGE:\n"
         << "cross_validate [options] required arguments\n\n"
         << "  OPTIONAL ARGUMENTS:\n"
#ifdef HAVE_GETOPT_H
         << "  --help (-h)                        - print this information and exit\n\n"
         << "  --agnostic (-a) <filename>         - list of genes to be agnostic to\n" 
         << "  --max_threads (-t) <num_threads>   - maximum number of threads for search\n"
         << "                                       (default: " << DEFAULT_MAX_THREADS << ")\n"
#ifdef ENABLE_ORTHOLOGY        
         << "  --orthology_file (-o) <filename>   - orthology file (if not passed then no\n"
         << "                                       orthology information will be loaded)\n"
         << "  --linkage_type (-l) <linkage_type> - 0=min, 1=max, 2=mean, 3=median,\n" 
         << "                                       4=sig. med. (default: " << DEFAULT_LINKAGE_TYPE << ")\n\n"
#endif
#else //no getopt_long
         << "  -h                - print this information and exit\n\n"
         << "  -a <filename>     - list of genes to be agnostic to\n"
         << "  -t <num_threads>  - maximum number of threads for search (default 1)\n"
#ifdef ENABLE_ORTHOLOGY
         << "  -o <filename>     - orthology file (if not passed no orthology information loaded)\n"
         << "  -l <linkage_type> - 0=min, 1=max, 2=mean, 3=median, 4=sig. med.\n"
         << "                      (default: " << DEFAULT_LINKAGE_TYPE << ")\n\n"
#endif
#endif

         << endl
         << "  REQUIRED ARGUMENTS:\n"
         << "  0 - Configuration file:\n"
         << "      1 entry for each organism, tab-delimited with:\n"
         << "      OrgID\\tGeneFile\\tListOfFiles\\tPathToFiles\n"
         << "  1 - File containing list of query files\n"
         << "  2 - path to result files\n"
         << "  3 - query size (e.g. 2)\n"
         << "  4 - number of samples\n"
         << "  5 - path to gene set files\n";
    
}
