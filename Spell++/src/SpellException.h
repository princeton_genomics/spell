/*
 * SpellException.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SPELLEXCEPTION_H
#define SPELLEXCEPTION_H

#include <exception>
#include <string>

/**
 * A basic exception for Spell.
 *
 */
class SpellException: public std::exception {
protected:
    /// a string error message that the thrower wants to pass back to the catcher
    std::string mesg;
    
public:
    
    /**
     * SpellException constructor, initializes the SpellException with an error message 
     *
     * @param s std::string error message
     */
    SpellException(std::string s):mesg(s){}
 
    /**
     * SpellException descructor. 
     *
     */
    virtual ~SpellException() throw(){}
    
    /**
     * overloaded what() method returns the error message that was set by the 
     * constructor
     *
     * @returns std::string error message
     */
    virtual const char *what() const throw();
};

/**
 * A more specialized SpellException, indicates the query containt an invalid
 * parameter
 *
 */
class InvalidParameterException:  public SpellException {
    
public:
    /**
     * InvalidParameterException constructor, initializes the 
     * InvalidParameterException with an error message.
     *
     * @param s std::string error message
     */
    InvalidParameterException(std::string s): SpellException(s) {}
};

/**
 * A more specialized SpellException, indicates we timed out waiting to recieve
 * a complete query from the client.
 *
 */
class TimeOutException:  public SpellException {
    
public:
    /**
     * TimeOutException constructor, initializes the 
     * TimeOutException with an error message.
     *
     * @param s std::string error message
     */
    TimeOutException(std::string s): SpellException(s) {}
};

#endif
