/*
 * main.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>

#include <errno.h>



#include "ExpressionDataset.h"
#include "Searcher.h"
#include "SearchServer.h"
#include "SpellException.h"
#include "ConfigFile.h"
#include "Homology.h"
#include "Organism.h"
#include "utilities.h"

#include "config.h"

//must be AFTER including config.h so HAVE_GETOPT_H has a chance of being defined 
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
#include <unistd.h>
#endif


// default values for options the user does not specify on the command line
#define DEFAULT_PORT 5011
#define DEFAULT_MAX_THREADS 1
#define DEFAULT_LINKAGE_TYPE linkage_type::MEDIAN
#define DEFAULT_DISTANCE_TYPE distance_type::ZSCORE2



using namespace std;


/**
 * print usage information for spell++. The format of the usage information
 * depends on whether configure found getopt.h or not (without getopt.h
 * getopt() will be used instead of getopt_long() and only short options will
 * be accepted).
 *
 */
static void printUsage();

/**
 * main entry point for Spell++: parses command line, and initializes a 
 * SearchServer, which does the real work.
 *
 * @param argc size of argument array
 * @param argv program arguments
 * @returns non-zero on error, does not return otherwise 
 *          (SearchServer::listenForRequest() sets up an infinite loop)
 */
int main (int argc, char * const argv[]) {
    
    bool debug = false;
    bool usage = false;
    bool daemonize = true;
    
    string orthologyFile = "";
    string argstr;
    
    unsigned int port        = DEFAULT_PORT;
    unsigned int maxThreads  = DEFAULT_MAX_THREADS;
    unsigned int linkageType = DEFAULT_LINKAGE_TYPE;
    unsigned int distType    = DEFAULT_DISTANCE_TYPE;
    
    Homology homology;
    ConfigFile confFile;
    
    // getopt/getopt_long variables
    int c;
    string optstr;
    
    
    
    // output the package name and version 
    cout << endl << PACKAGE_NAME << " " << PACKAGE_VERSION << endl << endl;
    

    /*
       parse command line
     
       sorry for the #idefs, getopt_long is non-standard on some platforms so 
       we provide a getopt_long and getopt version of the for loop that parses
       the command line.  The same loop body is used, but the while test
       is picked based on whether configure finds getopt.h or not.
     
       Also, the orthology command line options are disabled by default
       since they are under development and should not be used in production 
       environments. They can be enabled by passing --enable-orthology to 
       the configure script. This also requires the use of #ifdef.
     */
    
#ifdef ENABLE_ORTHOLOGY    
    optstr = "d:Dfhp:t:l:o:";  // include orthology related command line options
#else
    optstr = "d:Dfhp:t:";      // exclude orthology related command line options
#endif
    
#ifdef HAVE_GETOPT_H
    //we have getopt.h so getopt_long() is available
    
    struct option long_options[] =
    {
        //don't use the getopt_long flag syntax since we share the 
        //swtich statement with the getopt() impelmentation
        /* {"distance",        required_argument, 0, 'd'}, */
        {"debug",           no_argument,       0, 'D'},
        {"foreground",      no_argument,       0, 'f'},
        {"help",            no_argument,       0, 'h'},
        {"port",            required_argument, 0, 'p'},
        {"max_threads",     required_argument, 0, 't'},
#ifdef ENABLE_ORTHOLOGY 
        {"linkage_type",    required_argument, 0, 'l'},
        {"orthology_file",  required_argument, 0, 'o'},
#endif
        {0, 0, 0, 0}
    };
    
    // passed to getopt_long, but we do not use (sharing while() body with getopt)
    int option_index;
    
    //setup while loop using getopt_long
    while ((c = getopt_long(argc, argv, optstr.c_str(), long_options, &option_index)) != -1)
#else 
    // no getopt.h so no getopt_long(), use getopt() instead
    while ((c = getopt(argc, argv, optstr.c_str())) != -1)
#endif
    {
        // in the shared while loop body for getopt and getopt_long
        
        //shared switch statement for getopt() and getopt_long()
        switch (c) {
#if 0 // not currently specified from command line
            case 'd':
                argstr = optarg;
                try {
                    distanceType = utilities::from_string<unsigned int>(argstr);
                }
                catch (SpellException &e) {
                    cerr << "ERROR: Invalid distance type: " << optarg << ".\n";
                    usage = true;
                }
                if (linkageType > DISTANCE_MAX) {
                    cerr << "ERROR: Invalid distance type: " << distanceType << ".\n";
                    usage = true;
                }
                break;
#endif
            case 'D':
                debug = true;
                break;
            case 'h':
                printUsage();
                return 0;
            case 'f':
                daemonize = false;
                break;
            case 'p':
                argstr = optarg;
                try {
                    port = utilities::from_string<unsigned int>(argstr);
                }
                catch (SpellException &e) {
                    cerr << "ERROR: Invalid port: " << optarg << ".\n";
                    usage = true;
                }
                break;
            case 't':
                argstr = optarg;
                try {
                    maxThreads = utilities::from_string<unsigned int>(argstr);
                }
                catch (SpellException &e) {
                    cerr << "ERROR: Invalid number of threads: " << optarg << ".\n";
                    usage = true;
                }
                break;
#ifdef ENABLE_ORTHOLOGY 
            case 'l':
                argstr = optarg;
                try {
                    linkageType = utilities::from_string<unsigned int>(argstr);
                }
                catch (SpellException &e) {
                    cerr << "ERROR: Invalid linkage type: " << optarg << ".\n";
                    usage = true;
                }
                if (linkageType > LINKAGE_MAX) {
                    cerr << "ERROR: Invalid linkage type: " << linkageType << ".\n";
                    usage = true;
                }
                break;
            case 'o':
                orthologyFile = optarg;
                break;
#endif
            case '?':
                // getopt_long or getopt already printed an error message.
                usage = true;
                break;
            default:
                usage = true;
        }
    }
    
    //by default getopt/getopt_long permutes argv so the non-options are at the end
    //we'll increment argv to skip all the options we've already parsed. 
    //(and skip over the executable name element in argv[0])
    argc -= optind;
    argv += optind;

    // after parsing the command line options there should be one argument left
    // if this argument is not present or we had set the usage flag then 
    // print the usage and return with error
    if (usage || argc != 1) {
        cerr << endl;
        printUsage();
        return EINVAL;
    }
 
    //   END COMMAND LINE PARSING
        
        
    // read in the configuration file
    cout << "CONFIG FILE: " << argv[0] << endl;
    try {
        confFile = ConfigFile(argv[0]);
    }
    catch (SpellException &e) {
        cerr << "ERROR loading config file:\n";
        cerr << "\t" << e.what() << endl;
        return 1;
    }
    
    // load each organism in the config file
    for (unsigned int i = 0; i < confFile.getNumOrganisms(); i++) {
        cout << "Loading " << confFile.getOrgConfig(i).getOrgID() << "... " << flush;
        try {
            // get this organism's configuration information
            OrgConfig orgConf = confFile.getOrgConfig(i);
            string orgID = orgConf.getOrgID();
            
            // add the Organism to the homology
            homology.addOrganism(orgID, orgConf.getGeneFile());
            cout << homology.getOrganism(orgID)->getNumGenes() << " genes loaded\n";
            
            // load the datasets for the organism
            homology.getOrganism(orgID)->loadDatasets(orgConf.getDatasetListFile(), 
                                                      orgConf.getDataDir());
            
            // add orthology information (empty string, "", indicates no orthology info)
            homology.addGeneFamilies(orthologyFile);
        }
        catch (SpellException &e) {
            cerr << endl << "Error loading " << confFile.getOrgConfig(i).getOrgID() <<":\n";
            cerr << e.what() << endl;
            return 1;
        }
        
    }
 
    // create a Searcher object
    Searcher search(&homology, maxThreads);


    // create a SearchServer and listen for requests, this will not return
    try {
        SearchServer server(search, port, distType, linkageType);
        server.setDebug(debug);
        server.listenForRequest(daemonize);    
    }
    catch (SpellException &e) {
        // problem setting up the SearchServer, print the cause of the error
        // and exit
        cerr << e.what() << endl;
        return 1;
    }

    // not reached
    return 0;
}



static void printUsage()
{

    cerr << "USAGE INFORMATION:\n";
    
#ifdef HAVE_GETOPT_H
// system has getopt_long()
    cerr << "    " << PACKAGE_NAME << " [options] <configuration_file>\n\n"
         << "OPTIONS:\n"
         << "    --help (-h):           print this information and exit\n\n"
         << "    --debug (-D):          turn on debugging output (print queries and replies)\n"
         << "                           no effect if the server is daemonized\n"
         << "    --foreground (-f):     do not daemonize server, run in foreground\n"
         << "    --port (-p):           listening port (default: " << DEFAULT_PORT <<")\n"
         << "    --max_threads (-t):    maximum number of threads to spawn during\n"
         << "                           multi-threaded portions of a search (default: " << DEFAULT_MAX_THREADS << ")\n\n";
         
#ifdef ENABLE_ORTHOLOGY
    cerr << "GENE ORTHOLOGY OPTIONS (under development):\n"
         << "    --orthology_file (-o): the file containing orthology information\n"
         << "    --linkage_type (-l):   linkage type for calculating correlation between gene\n"
         << "                           families(0=min, 1=max, 2=mean, 3=median, 4=sig. med)\n"
         << "                           (default: " << DEFAULT_LINKAGE_TYPE << ")\n";
#endif
//end HAVE_GETOPT_H
#else
//no getopt_long() support

    cerr << "    spell++ [options] <configuration_file>\n\n"
         << "OPTIONS:\n"
         << "    -h:   print this information and exit\n\n"
         << "    -D:   turn on debugging output (print queries and replies)\n"
         << "          no effect if the server is daemonized\n"
         << "    -f:   do not daemonize server, run in foreground\n"
         << "    -p:   listening port (default: " << DEFAULT_PORT <<")\n"
         << "    -t:   maximum number of threads to spawn during multi-threaded portions of\n"
         << "          a search (default: " << DEFAULT_MAX_THREADS << ")\n";
#ifdef ENABLE_ORTHOLOGY
    cerr << "GENE ORTHOLOGY OPTIONS (under development):\n"
         << "    -o:   file containing orthology information\n"
         << "    -l:   linkage type for calculating correlation between gene families\n"
         << "          (0=min, 1=max, 2=mean, 3=median, 4=sig. med)\n";
#endif

//end no getopt_long() support    
#endif 

}



