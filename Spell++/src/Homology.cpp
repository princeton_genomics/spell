/*
 * Homology.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <iostream>

#include <boost/unordered_set.hpp>

#include "Homology.h"
#include "SpellException.h"


using namespace std;

Homology::Homology()
{
    usingOrthology = false;
}

Homology::~Homology()
{
    for (organism_iterator it = organisms.begin();
         it != organisms.end(); ++it) {
        delete it->second;
    }
    
    for (unsigned int i = 0; i < families.size(); i++) {
        delete families[i];
    }
}

void Homology::addOrganism(std::string id, std::string geneFileName)
{
    organisms[id] = new Organism(id, geneFileName);
}

Organism *Homology::getOrganism(std::string id)
{
    if (organisms.find(id) != organisms.end()) {
        return organisms[id];
    }
    return NULL;
}

void Homology::makeDefaultFamilies()
{
    for (organism_iterator it = organisms.begin(); it != organisms.end(); ++it) {
        
        Organism *org = it->second;
        
        cout << "Creating default families for orphan genes in " 
             << org->getName() << "..." << std::flush;
        
        std::vector<GeneFamily*> newFamilies = org->makeDefaultFamilies();
        
        for (unsigned int i = 0; i < newFamilies.size(); i++) {
            newFamilies[i]->setID(families.size());
            families.push_back(newFamilies[i]);
        }
        
        cout << newFamilies.size() << " families created.\n\n";
        
        
    }
}

void Homology::addGeneFamilies(std::string familyFileName)
{
    std::string line;
    

    if (familyFileName.empty()) {
        cout << endl << "No orthology file recieved.\n"
             << "Assuming that no gene family information is desired\n";
        usingOrthology = false;
    }
    else {
        
        ifstream familyFileStream(familyFileName.c_str());
    
        if(!familyFileStream.is_open() || !familyFileStream.good()) {
            string mesg = "Error opening gene family file ";
            mesg = mesg + familyFileName + "\n";
            throw SpellException(mesg); 
        }
        

        while (getline(familyFileStream, line)) {
            vector<std::string> tokens;
            
            utilities::tokenize(line, tokens, "\t");
            GeneFamily *family = new GeneFamily();
            for (unsigned int i = 0; i < tokens.size(); i++) {
                vector<string> subparts;
                utilities::tokenize(tokens[i], subparts, "\\|");
                
                organism_iterator it = organisms.find(subparts[0]);
                if (it != organisms.end()) {
                    Gene *g = it->second->getGeneByName(subparts[1]);
                    if (g == NULL) {
                        transform( subparts[1].begin(), subparts[1].end(), subparts[1].begin(), ::toupper);
                        g = it->second->getGeneByName(subparts[1]);
                    }
                    
                    if (g == NULL) {
                        cout << "WARNING: Unable to find match for " << tokens[i] << std::endl;
                    }
                    else {
                        //Add the gene to the family, and link the family to the gene and organism
                        family->addGene(g);
                        g->setFamily(family);
                        it->second->addFamily(family);
                    }
                }
            }
            
            //If the family is not empty after adding all valid genes...
            if (family->size() != 0 ) {
                //Give the family an index and store it
                family->setID(families.size());
                families.push_back(family);
            }
        }
        
        for (organism_iterator it = organisms.begin(); it != organisms.end(); ++it) {
            //Also indicate that this organism is using orthology information
            it->second->setUsingOrthology(true);
        }
        // set homology usingOrthology flag
        usingOrthology = true;
    }
    

    
    //Each gene must belong to exactly one gene family, so find all orphan genes and make the families
    makeDefaultFamilies();
}



std::vector<ExpressionDataset*> Homology::getDatasetsByID(std::vector<int> ids)
{
    vector<ExpressionDataset*> dsets;
    ExpressionDataset *d;
    Organism *org;
    
    for (unsigned int i = 0; i < ids.size(); i++) {
        for (organism_iterator it = organisms.begin(); it != organisms.end(); ++it) {
            org = it->second;
            d = org->getDatasetByID(ids[i]);
            if (d != NULL) {
                dsets.push_back(d);
            }
        }
    }
    
    return dsets;
}


std::vector<GeneFamily*> Homology::getGeneFamiliesByGeneID(std::vector<int> ids)
{
    boost::unordered_set<GeneFamily*> families;
    vector<GeneFamily*> familyVec;
    Organism *org;
    Gene *gene;
    
 
    for (organism_iterator it = organisms.begin(); it != organisms.end(); ++it) {
        org = it->second;
        for (unsigned int i = 0; i < ids.size(); i++) {
            gene = org->getGeneByID(ids[i]);
            if (gene != NULL) {
                families.insert(gene->getFamily());
            }
        }
    }
    
    for (boost::unordered_set<GeneFamily*>::iterator it = families.begin(); 
         it != families.end(); ++it) {
        familyVec.push_back(*it);
    }
    
    return familyVec;
}

GeneFamily *Homology::findFamily(std::string geneName)
{
    Gene *g = NULL;
    Organism *org;
    
    for (organism_iterator it = organisms.begin(); it != organisms.end(); ++it) {
        org = it->second;
        if (org->getGeneByName(geneName)) {
            if (g != NULL) {
                cerr << "WARNING: " << geneName << " maps to multiple genes, including " << g->getOrganism()->getName()
                << "|" << g->getName() << " and " << org->getName() << "|" << org->getGeneByName(geneName)->getName() << endl;
            }
            else {
                g = org->getGeneByName(geneName);   
            }
        }
    }
    if ( g != NULL) {
        return g->getFamily();
    }
    return NULL;
}


std::vector<GeneFamily*> Homology::findAllFamilies(std::string geneName)
{
    Gene *g;
    Organism *org;
    vector<GeneFamily*> families;
    
    for (organism_iterator it = organisms.begin(); it != organisms.end(); ++it) {
        org = it->second;
        g = org->getGeneByName(geneName);
        if (g != NULL) {
            families.push_back(g->getFamily());
        }
    }
    return families;
}

std::vector<ExpressionDataset*> Homology::getDatasets()
{
    Organism *org;
    vector<ExpressionDataset*> allDsets;
    
    for (organism_iterator it = organisms.begin(); it != organisms.end(); ++it) {
        org = it->second;
        
        vector<ExpressionDataset*> orgDsets = org->getDatasetVector();
        for (int i = 0; i < orgDsets.size(); i++) {
            allDsets.push_back(orgDsets[i]);
        }
    }
    
    return allDsets;
}
