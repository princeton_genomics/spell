/*
 * ExpressionDataset.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef EXPRESSIONDATASET_H
#define EXPRESSIONDATASET_H

#include <string>
#include <map>
#include <vector>
#include <limits>
#include <boost/unordered_map.hpp>

#include "Gene.h"

class Organism;

#define LINKAGE_MAX 4

/// enum constants used to specify the linkgage statistics type
namespace linkage_type {
    enum  {MAX, MIN, MEAN, MEDIAN, SIGNIFICANT_MEDIAN};
}


#define DISTANCE_MAX 3

/// enum constants used to specify a distance measurement type
namespace distance_type {
    enum  {PEARSON, ZSCORE, ZSCORE2, SIGNIFICANCE};
}


/**
 * contains expression levels for genes over a number of conditions and some 
 * assoiciated statistics for a single microarray expression dataset
 *
 */
class ExpressionDataset {
private:
    /// reference to the Organism that this ExpressionDataset is for
    Organism *org;
    
    /// map a gene id to the data
    boost::unordered_map<int, std::vector<float> > data;
    
    /// the conditions in the ExpressionDataset
    std::vector<std::string> conds;
    
    /// number of conditions in the ExpressionDataset
    unsigned int numConds;
    
    /// filename for this ExpressionDataset
    std::string filename;
    
    /// integer id for the ExpressionDataset, this is how it is referenced
    /// in queries from the front end
    int id;
    
    /// declaration for a costant "ALPHA". The C++ standard requires that
    /// this gets defined in the .cpp file, since you can't initialize static
    /// const floats in a class definition.
    static const float ALPHA;
    
    /// zscore mean
    float z_mean;
    
    /// zscore standard deviation
    float z_std;
    
    /// min-linkage mean
    float zmin_mean;
    
    /// min-linkage standard deviation
    float zmin_std;
    
    /// max-linkage mean
    float zmax_mean;
    
    /// max-linkage standard deviation
    float zmax_std;
    
    /// mean-linkage mean
    float zmean_mean;
    
    /// mean-linkage standard deviation
    float zmean_std;
    
    /// median-linkage mean
    float zmedian_mean;
    
    /// median-linkage stdandard deviation
    float zmedian_std;
    
    /// significant median-linkage mean
    float zsigmed_mean;
    
    /// significant median-linkage standard deviation
    float zsigmed_std;
    
    /**
     * load the stats file for this ExpressionDataset if it has already been
     * generated. If there is a problem reading the stats file or it doesn't exist
     * then create and save the stats
     *
     */
    void loadOrCreateStatFile();
    
    /**
     * recalculates and saves the stats file
     *
     */
    void recalculateAndSaveStatFile();
    
    /**
     * recalculate the stats in a less memory intensive way
     *
     * @note note yet implemented
     * @throws SpellException
     */
    void recalculateMemoryLimitedStats();
    
    
    /**
     * calculate he Fisher z-transformation of the correlation
     *
     * @param correlation
     * @returns float Fisher z-tranformation of correlation
     */
    float fisherZtransform(float correlation);
    
    /**
     * calculate the correlation between the z scores of two gene families
     *
     * @param gfx a pointer to a gene family
     * @param gfy a pointer to a gene family
     * @param linkageType one of linkage_type indicating linkage statistics type 
     *         to use for each family
     * @returns the zscore correlation between the two families using the specified
     *          linkage type
     */
    float zscoreCorrelation(const GeneFamily *gfx, const GeneFamily *gfy, int linkageType);
    
    /**
     * calculate the correlation between the z scores of two genes
     *
     * @param gx pointer to a Gene
     * @param gy pointer to a Gene
     * @returns the zscore correlation between gx and gy
     *
     */
    float zscoreCorrelation(const Gene *gx, const Gene *gy);
    
    /**
     * calculate the significance of a correlation for a certain distance type
     *
     * @param corr a float correlation
     * @param distType one of distance_type 
     *
     */
    float correlationSignificance(float corr, int distType);
    
    /**
     * calculate the pearson correlation between two genes (by Gene id)
     *
     * @param gx int gene id
     * @param gy int gene id
     * @returns pearson correlation
     *
     */
    float pearsonCorrelation(int gx, int gy);
    
    /**
     * calculate pearson correlation between two genes
     *
     * @param gx pointer to a Gene object
     * @param gy pointer to a Gene object
     * @returns pearson correlation as a float
     *
     */
    float pearsonCorrelation(const Gene *gx, const Gene *gy)
    {
        return pearsonCorrelation(gx->getID(), gy->getID());
    }
    
    /**
     * calculate pearson correlation between two gene families
     *
     * @param gfx pointer to a GeneFamily object
     * @param gfy pointer to a GeneFamily object
     * @param linkageType one of linkage_type indicating linkage statistics type
     * @returns pearson correlation as a float
     *
     */
    float pearsonCorrelation(const GeneFamily *gfx, const GeneFamily *gfy, 
                             int linkageType)
    {
        return linkageValue(allPearsonCorrelations(gfx, gfy), linkageType, 
                            distance_type::PEARSON);
    }
    
    /**
     * calculate the Pearson correlation between each pair of genes in two gene families
     *
     * @param gfx a poiter to a Gene
     * @param gfy a pointer to a GeneFamily
     * @returns a vector of float correlations for each pair
     *
     */
    std::vector<float> allPearsonCorrelations(const GeneFamily *gfx, const GeneFamily *gfy);
    
    /**
     * calculate the Pearson correlation between a single Gene and each Gene in
     * a GeneFamily
     *
     * @param gx a pointer to a Gene
     * @param gfy a pointer to a GeneFamily
     * @returns a vector of float correlations for each pair
     *
     **/
    std::vector<float> allPearsonCorrelations(const Gene *gx, const GeneFamily *gfy);
    
    /**
     * compute a linkage value for a vector of correlations
     * (expresion correlations between genes in two gene families)
     *
     * @param corrs a vector of float correlations
     * @param linkageType one of linkage_type, the type of linkage statistics to sue
     * @param distType one of distance_type, the distance metric
     * @returns a float linkage value
     */
    float linkageValue(std::vector<float> corrs, int linkageType, int distType);
    
    
    /**
     * calculate the linkage z score for a vector of correlation values
     * (expresion correlations between genes in two gene families)
     *
     * @param corrs a vector of floating point correlation values
     * @param linkageType
     * @returns float linkage z score
     *
     */
    float linkageZscore(std::vector<float> corrs, int linkageType);
    
    /**
     * compute the mean of an array
     *
     * @param array and array of floats
     * @param size the size of the array
     * @returns the mean of the values in array[]
     *
     */
    float mean(float array[], int size);
    
    /**
     * compute the mean of a vector of floats
     *
     * @param vec a vector of float values
     * @returns the mean of the values in vec
     *
     */
    float mean(std::vector<float> vec);
    
    /**
     * compute the median of a vector of floats
     *
     * @param vec a vector of float values
     * @returns the median of vec
     *
     */
    float median(std::vector<float> vec);
    
    /**
     * compute the standard deviation of an array of floats if the mean is known
     *
     * @param array an array of float values
     * @param size the number of values in the array
     * @param mean of the values
     * @returns the standard deviation of the values
     *
     */
    float stddev(float array[], int size, float mean);

    /**
     * compute the standard deviation of an array of floats. This function
     * computes the mean 
     *
     * @param array an array of float values
     * @param size the number of values in the array
     * @returns the standard deviation of the values
     *
     */
    float stddev(float array[], int size);
    
    /**
     * compute the standard deviation of a vector of floats if the mean is known
     *
     * @param vec a vector of float values
     * @param mean of the values
     * @returns the standard deviation of the values
     *
     */
    float stddev(std::vector<float> vec, float mean);

    /**
     * compute the standard deviation of a vector of floats. This function
     * computes the mean 
     *
     * @param vec a vector of float values
     * @returns the standard deviation of the values
     *
     */
    float stddev(std::vector<float>);
    
public:
    
    /**
     * construct a new ExpressionDataset
     *
     * @param org a pointer to an Organism object. This is the organism for which
     *        the dataset contains expression information.
     * @param filename the std::string filename of the expression file
     * @param id an integer ID used by the front end to reference this dataset
     *
     */
    ExpressionDataset(Organism *org, std::string filename, int id);
    
    /**
     * compute the score for a pair of gene families
     *
     * @param gfx a pointer to a GeneFamily
     * @param gfy a pointer to a GeneFamily
     * @param distType the distance measurement used
     * @param linkageType the type of linkage used for correlation between genes in a gene family
     * @returns float score
     *
     */
    float score(const GeneFamily *gfx, const GeneFamily *gfy, int distType, int linkageType);
    
    
    /**
     * compute the score for a pair of genes
     *
     * @param gx a pointer to a Gene
     * @param gy a pointer to a Gene
     * @param distType the distance measurement used
     * @returns the float score
     */
    float score(const Gene *gx, const Gene *gy, int distType); 
    

    /**
     * compute the score for a single gene
     *
     * @param g a pointer to a Gene
     * @returns the float score
     */
    float score(const Gene *g); 
    

    /**
     * get the ID for this ExpressionDataset
     *
     * @returns the integer ID
     */
    int getID()
    {
        return id;
    }
    
    /**
     * get the filename for this expression dataset
     *
     * @returns a std::string filename
     */
    std::string getFilename()
    {
        return filename;
    }
    
    /**
     * get the number of conditions this expression dataset
     *
     * @returns an int count of conditions
     */
    int getNumConds()
    {
        return numConds;
    }
    
    /**
     * return a pointer to an Organism that this expression dataset belongs to
     *
     * @returns an Organism*
     */
    Organism *getOrganism()
    {
        return org;
    }
    
};



#endif
