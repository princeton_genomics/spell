/*
 * Homology.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HOMOLOGY_H
#define HOMOLOGY_H

#include <string>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

#include "Organism.h"

typedef boost::unordered_map<std::string, Organism*>::const_iterator organism_const_iterator;
typedef boost::unordered_map<std::string, Organism*>::iterator organism_iterator;

/**
 * Homology class allows us to load multiple Organisms and store GeneFamily information
 *
 */
class Homology {
private:
    /// all organisms that have been loaded
    boost::unordered_map<std::string, Organism*> organisms;
    
    /// all gene families for this Homology
    std::vector<GeneFamily*> families;
    
    /// true if we have loaded orthology information, false otherwise
    bool usingOrthology;
    
    /**
     * made default families for any Genes that don't have GeneFamily information
     *
     */
    void makeDefaultFamilies();
    
public:
    /**
     * default constructor, creates an empty Homology
     */
    Homology();
    
    /**
     * destructor, deletes all Organisms and GeneFamilies
     *
     */
    ~Homology();
    
    /**
     * add a new Organism to the Homology.  This creates a new Organism object
     * and loads all of the Genes for it, and adds the Organism to this Homology
     *
     * @param id a string identifier for the Organism
     * @param geneFileName string containing a path to the gene file for this Organism
     */
    void addOrganism(std::string id, std::string geneFileName);
    
    /**
     * Add GeneFamily information from a file or make default families. If the
     * empty string is passed as a file name then a warning will be printed and 
     * we will make default families for all genes. If the file is valid any 
     * Gene without a family after loading all family information will be given 
     * a default family where it is the only member. 
     *
     * @param familyFileName path to the file containing the GeneFamily information
     */
    void addGeneFamilies(std::string familyFileName);
    
    /**
     * get an Organism with a given string identifier
     *
     * @param id the std::string identifier for the Organism
     * @returns a pointer to the Organism if found, NULL otherwise
     */
    Organism* getOrganism(std::string id);
    
    /**
     * get ExpressionDatasets corresponding to a vector of dataset IDs. This function
     * iterates over every Organism to see if it has the dataset
     *
     * @param ids a vector of integer IDs for the ExpressionDatasets we are interested in
     * @returns a vector of ExpressionDataset pointers for all of the datasets that were found.
     */
    std::vector<ExpressionDataset*> getDatasetsByID(std::vector<int> ids);
    
    /**
     * get gene families for a given set of GeneFamily IDs
     *
     * @param ids a vector of integer GeneFamily IDs
     * @returns a vector of GeneFamily pointers
     */
    std::vector<GeneFamily*> getGeneFamiliesByGeneID(std::vector<int> ids);

    /**
     * get all gene families for this Homology
     *
     * @returns a vector of all GeneFamily pointers
     */
    std::vector<GeneFamily*> getFamilies()
    {
        return families;
    }
    
    /**
     * get all datasets loaded for this Homology
     *
     */
    std::vector<ExpressionDataset*> getDatasets();
    
    /**
     * get the GeneFamily for a given gene name
     *
     * @param geneName a string gene name
     *
     * @returns a pointer to a GeneFamily
     */
    GeneFamily *findFamily(std::string geneName);
    
    
    std::vector<GeneFamily*> findAllFamilies(std::string geneName);


    /**
     * indicates if we have loaded GeneFamily information for this Homology
     *
     * @returns true if we've loaded gene family information, false otherwise
     */
    bool usesOrthology() 
    {
        return usingOrthology;
    }
    

};

#endif
