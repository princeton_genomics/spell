/*
 * GeneFamily.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef GENEFAMILY_H
#define GENEFAMILY_H

#include <string>
#include <vector>
#include <boost/unordered_set.hpp>

#include "Gene.h"

/// contains a typedef const_iterator for the const_iterator type for the 
/// container holding pointers to all the Genes that belong to this GeneFamily
namespace gene_family {
    /// a cost_iterator for the container holding pointers to all 
    /// Genes that belong to this GeneFamily
    typedef boost::unordered_set<Gene*>::const_iterator const_iterator;
}

/**
 * The GeneFamily allows us to group orthological genes. The GeneFamily contains
 * a hash set of Gene pointers.
 *
 */
class GeneFamily {

private:
    
    /// Unique integer identifier for this GeneFamily
    int id;
    
    /// Hash set of pointers to Gene objects that belong to this GeneFamily
    boost::unordered_set<Gene*> genes;
    
public:
    
    /**
     * constructor to create an empty GeneFamily
     *
     * @param id GeneFamily ID
     *
     */
    GeneFamily(int id = -1) : id(id) {}
    
    /**
     * get the ID for this family
     *
     * @returns GeneFamily::id
     */
    int getID()
    {
        return id;
    }
    
    /**
     *  Add a Gene to this GeneFamily. This adds a pointer to a Gene to
     *  GeneFamily::genes.
     *
     *  @param g A pointer to a Gene object
     */
    void addGene(Gene *g)
    {
        genes.insert(g);
    }
    
    /**
     *  Convert the GeneFamily into a text representation.
     *
     *  @returns a std::string containing a text representation of the family
     */
    std::string toString();
    
    /**
     *  Get the size of the family. This is the number of Genes that belong
     *  to the family.
     *
     *  @returns the number of Gene pointers in GeneFamily::genes
     */
    int size()
    {
        return genes.size();
    }
    
    /**
     *  Set the ID for this GeneFamily
     *
     *  @param id the new ID
     */
    void setID(int id)
    {
        this->id = id;
    }
    
    /**
     * Get a simple vector of the gene IDs in this family.
     *
     * @returns a vector if ints which are the gene IDs.
     */
    std::vector<int> getGeneIDs() const;

    /**
     * Get an iterator to the genes in this family
     *
     * @returns a const_iterator GeneFamily::genes.begin()
     */
    boost::unordered_set<Gene*>::const_iterator begin() const
    {
        return genes.begin();
    }
    
    /**
     * Get an end iterator to the genes in this family. This is useful for a 
     * stopping condition in a loop that iterates over all genes in the family.
     *
     * @ returns GeneFamily::genes.end()
     */
    boost::unordered_set<Gene*>::const_iterator end() const
    {
        return genes.end();
    }
    
    
};

#endif
