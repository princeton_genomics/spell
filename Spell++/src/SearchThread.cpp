/*
 * SearchThread.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <sstream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>

#include "SearchThread.h"
#include "SpellException.h"
#include "QueryParser.h"

//for distance_types and linkage_types
#include "ExpressionDataset.h"

using namespace std;

SearchThread::~SearchThread()
{
    close(connection);
}


void SearchThread::execute()
{    
    string message;
    int maxGenesReturned;
    QueryParser qp;
    
    
    try {
        message = recvQuery();
    }
    catch (TimeOutException &e) {
        // let the client know we timed out waiting for it to send the 
        // complete message just in case it is still listening
        replyWithError(error_codes::TIME_OUT, e.what());
        close(connection);
        return;
    }
    catch (SpellException &e) {
        // connection with the client failed. No point in replying, just pring
        // an error message to stderr
        cerr << e.what() << endl;
        close(connection);
        return;
    }
    
    // log the event
    if (debug) {
        cout << "[" << address << "] request: " << message << endl;
    }
    
    try {
        qp.parse(message);
    }
    catch (InvalidParameterException &e) {
        replyWithError(error_codes::INVALID_PARAMETER, e.what());
        close(connection);
        return;
    }
    catch (SpellException &e) {
        replyWithError(error_codes::PROTOCOL_ERROR, e.what());
        close(connection);
        return;
    }
    
    
    maxGenesReturned = qp.getMaxGenes();
    if (maxGenesReturned < 0) {
        maxGenesReturned = MAX_GENES_RETURNED_DEFAULT + qp.getQuerySize();
    }
    
    //try the query, if sucess reply with the result
    //otherwise reply with an error messge
    try {
        SearchResult result = searcher.querySearch(qp.getGeneIDs(), 
                                                   qp.getDatasetIDs(), 
                                                   distType,
                                                   linkageType,
                                                   maxGenesReturned);
        
        message = result.toString();
        
        if (debug) {
            cout << "reply to [" << address << "]: " << message << endl;
        }
        
        //send the message to the client, any failure here is unrecoverable
        reply(message);
        
    }
    catch (SpellException &e) {
        replyWithError(error_codes::SYS_ERR, e.what());
    }
    close(connection);

}

string SearchThread::recvQuery()
{
    // for select() and timeout
    struct timeval timeout;
    fd_set fds;
    
    char buf[READ_BUFFER_SIZE];
    int n;
    bool done = false;
    string message = "";
    

    // setup fds for select()
    FD_ZERO(&fds);
    FD_SET(connection, &fds);

    
    // read into our buffer (assuming we're recieving ASCI text) and append to 
    // message until we received all of the message (terminated by a newline)
    
    while (!done) {
        timeout.tv_sec = RECV_TIMEOUT;
        timeout.tv_usec = 0;
        
        select(connection+1, &fds, NULL, NULL, &timeout);

        if (FD_ISSET(connection, &fds)) {
            n = recv(connection, buf, READ_BUFFER_SIZE-1, 0);
            if (n > 0) {
                
                // chop off the newline, makes it easier for logging the
                // message if necessary
                if (buf[n-1] == '\n') {
                    buf[n-1] = '\0';
                    done = true;
                }
                else {
                    // no newline to cop off, but we still need to add the null
                    // termination
                    buf[n] = '\0';
                }
                
                message += buf;  
            }
            else {
                // connection with the client failed
                string errorMessage = "Connection with " + address + " failed";
                throw SpellException(errorMessage);
            }
            
            
        }
        else {
            string errorMessage = "Timed out waiting for " + address;
            throw TimeOutException(errorMessage);
        }
    }
    
    return message;
}

void SearchThread::reply(std::string mesg)
{
    //send the result back
    unsigned int bytesSent = 0;
    int ret = 0;
    
    
    // loop until the entire message is sent to the client, or the send fails
    while (bytesSent < mesg.size() && ret != -1) {
        ret = send(connection, mesg.c_str() + bytesSent, mesg.size() - bytesSent, 0);
        bytesSent += ret;
    }
    
    if (bytesSent != mesg.size()) {
        cerr << "Error communicating with client " << address << endl;
    }
    
}

void SearchThread::replyWithError(int type, std::string errMesg)
{
    ostringstream mesg;
    mesg << "!" << type << "\t" << errMesg << "\n";
    reply(mesg.str());    
}
