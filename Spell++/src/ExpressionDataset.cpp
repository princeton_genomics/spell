/*
 * ExpressionDataset.cpp
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <fstream>
#include <limits>
#include <cmath>

#include <boost/math/distributions/students_t.hpp>

#include "utilities.h"
#include "ExpressionDataset.h"
#include "SpellException.h"
#include "Organism.h"

using namespace std;

// set our static const float ALPHA defined in the ExpressionData Class
const float ExpressionDataset::ALPHA = 0.05f;

ExpressionDataset::ExpressionDataset(Organism *org, std::string filename, 
                                     int id):org(org),filename(filename),id(id)
{
    numConds = 0;

    ifstream dataset(filename.c_str());
 
    if(!dataset.is_open() || !dataset.good()) {
        string mesg = "Error opening expression dataset ";
        mesg = mesg + filename + "\n";
        throw SpellException(mesg);
    }
    
    string line;
    unsigned int offset = 2;
    vector<string> tokens;
    
    getline(dataset, line);
    utilities::tokenize(line, tokens, "\t");
        
    // convert the string read from the file to upper case for 
    // comparison. We could write a case insensitve std::string 
    // comparision function.
    transform( tokens[2].begin(), tokens[2].end(), tokens[2].begin(), ::toupper);
        
    if (tokens[2] == "GWEIGHT") {
        offset = 3;
    }
        
    for (unsigned int i = offset; i < tokens.size(); i++) {
        conds.push_back(tokens[i]);
        numConds++;
    }
        
        
    getline(dataset, line);
    utilities::tokenize(line, tokens, "\t");
    transform( tokens[0].begin(), tokens[0].end(), tokens[0].begin(), ::toupper);
    
    // if this line begins with EWEIGHT then it is ignored
    if (tokens[0] == "EWEIGHT") {
        getline(dataset, line);
    }
    
    while (!dataset.eof() && line != "") {
        utilities::tokenize(line, tokens, "\t");
        
        /* try looking up the gene */
        
        
        Gene *g = org->getGeneByName(tokens[0]);
        
        /* if it is still null, try converting the string to upper case */
        if (g == NULL) {
            std::transform( tokens[0].begin(), tokens[0].end(), tokens[0].begin(), ::toupper);
            g = org->getGeneByName(tokens[0]);
        }
            
        /* if still NULL, try the next field */
        if (g == NULL) {
            g = org->getGeneByName(tokens[1]);
        }
            
        /* still NULL so try converting to upper case */
        if (g == NULL) {
            transform( tokens[1].begin(), tokens[1].end(), tokens[1].begin(), ::toupper);
            g = org->getGeneByName(tokens[1]);
        }
            
        /* we found a gene, load in the data */
        if (g != NULL) {
            vector<float> d;

            for (unsigned int i = offset; i < tokens.size(); i++) {
                // convert the string token to a float, and push onto our 
                // data vector.
                // this will push quiet_NaN() if the conversion fails
                d.push_back(utilities::from_string<float>(tokens[i]));
            }
            
            if (d.size() > numConds) {
                SpellException e("too many conditions for " + g->getName());
                throw e;
            }
                
            while (d.size() < numConds) {
                d.push_back(numeric_limits<float>::quiet_NaN());
            }
            
            // add data for this gene
            data[g->getID()] = d;
        }
            
        // read in the next line
        getline(dataset, line);
    }
    
    loadOrCreateStatFile();
}


// Normal scoring involves correlations between two gene's expressions.
// When we have only a single gene in the query, the score is the standard
// deviation of the genes expression across the conditions in the datset.

float ExpressionDataset::score(const Gene *g)
{
    int gi = g->getID();
    if (data.find(gi) == data.end()) {
        return numeric_limits<float>::quiet_NaN();
    }
    
    vector<float> &x = data[gi];

    return stddev(x);
}

float ExpressionDataset::score(const GeneFamily *gfx, const GeneFamily *gfy, int distType, int linkageType)
{
    switch (distType) {
        case distance_type::PEARSON:
            return pearsonCorrelation(gfx, gfy, linkageType);
        case distance_type::ZSCORE:
            return zscoreCorrelation(gfx, gfy, linkageType);
        case distance_type::ZSCORE2:
            float val = zscoreCorrelation(gfx, gfy, linkageType);
            if (isnan(val)) {
                return val;
            }
            else if (val < 1) {
                return 0.0f;
            }
            else {
                return val * val;
            }
    }
    return numeric_limits<float>::quiet_NaN();
}

float ExpressionDataset::score(const Gene *gx, const Gene *gy, int distType)
{
    switch (distType) {
        case distance_type::PEARSON:
            return pearsonCorrelation(gx, gy);
        case distance_type::ZSCORE:
            return zscoreCorrelation(gx, gy);
        case distance_type::ZSCORE2:
            float val = zscoreCorrelation(gx, gy);
            if (isnan(val)) {
                return val;
            }
            else if (val < 1) {
                return 0.0f;
            }
            else {
                return val * val;
            }
    }
    return numeric_limits<float>::quiet_NaN();
    
}

void ExpressionDataset::loadOrCreateStatFile()
{
    string statFileName = filename + ".sinfo";
    
    z_mean       = numeric_limits<float>::quiet_NaN();
    z_std        = numeric_limits<float>::quiet_NaN();
    zmin_mean    = numeric_limits<float>::quiet_NaN();
    zmin_std     = numeric_limits<float>::quiet_NaN();
    zmax_mean    = numeric_limits<float>::quiet_NaN();
    zmax_std     = numeric_limits<float>::quiet_NaN();
    zmean_mean   = numeric_limits<float>::quiet_NaN();
    zmean_std    = numeric_limits<float>::quiet_NaN();
    zmedian_mean = numeric_limits<float>::quiet_NaN();
    zmedian_std  = numeric_limits<float>::quiet_NaN();
    zsigmed_mean = numeric_limits<float>::quiet_NaN();
    zsigmed_std  = numeric_limits<float>::quiet_NaN();
    
    
    try {
        string line;
        vector<string> tokens;
        ifstream statFile(statFileName.c_str());
    
        if(!statFile.is_open() || !statFile.good()) {
            throw 1;
        }
        
        getline(statFile, line);
        utilities::tokenize(line, tokens, "\t");
        if (tokens.size() != 3) {
            throw 1;
        }
        z_mean = utilities::from_string<float>(tokens[1]);
        z_std = utilities::from_string<float>(tokens[2]);
        //if (isnan(z_mean) || isnan(z_std)) {
        //    throw 1;
        //}
        
        getline(statFile, line);
        utilities::tokenize(line, tokens, "\t");
        if (tokens.size() != 3) {
            throw 1;
        }
        zmin_mean = utilities::from_string<float>(tokens[1]);
        zmin_std = utilities::from_string<float>(tokens[2]);
        //if (org->usesOrthology() && (isnan(zmin_mean) || isnan(zmin_std))) {
        //    throw 1;
        //}
        
        getline(statFile, line);
        utilities::tokenize(line, tokens, "\t");
        if (tokens.size() != 3) {
            throw 1;
        }
        zmax_mean = utilities::from_string<float>(tokens[1]);
        zmax_std = utilities::from_string<float>(tokens[2]);
        //if (org->usesOrthology() && (isnan(zmax_mean) || isnan(zmax_std))) {
        //    throw 1;
        //}
        
        getline(statFile, line);
        utilities::tokenize(line, tokens, "\t");
        if (tokens.size() != 3) {
            throw 1;
        }
        zmean_mean = utilities::from_string<float>(tokens[1]);
        zmean_std = utilities::from_string<float>(tokens[2]);
        //if (org->usesOrthology() && (isnan(zmean_mean) || isnan(zmean_std))) {
        //    throw 1;
        //}
        
        getline(statFile, line);
        utilities::tokenize(line, tokens, "\t");
        if (tokens.size() != 3) {
            throw 1;
        }
        zmedian_mean = utilities::from_string<float>(tokens[1]);
        zmedian_std = utilities::from_string<float>(tokens[2]);
        //if (org->usesOrthology() && (isnan(zmedian_mean) || isnan(zmedian_std))) {
        //    throw 1;
        //}
        
        getline(statFile, line);
        utilities::tokenize(line, tokens, "\t");
        if (tokens.size() != 3) {
            throw 1;
        }
        zsigmed_mean = utilities::from_string<float>(tokens[1]);
        zsigmed_std = utilities::from_string<float>(tokens[2]);
        //if (org->usesOrthology() && (isnan(zsigmed_mean) || isnan(zsigmed_std))) {
        //    throw 1;
        //}
    }
    catch (int i) {
        std::cout << "WARNING: Unable to read stats file: " << statFileName << endl;
        std::cout << "         Recalculating and saving stats\n";
        
        recalculateAndSaveStatFile();
    } 
    
}

void ExpressionDataset::recalculateAndSaveStatFile()
{
    string statFileName = filename + ".sinfo";
    vector<int> geneIDs;
    int maxSize;
    float *valArray;
    
    // get a vector of all gene IDs in the dataset (the keys of our unordered_map)
    for (boost::unordered_map<int, vector<float> >::iterator i = data.begin();
         i != data.end(); ++i) {
        geneIDs.push_back(i->first);
    }
    
    
    maxSize = geneIDs.size() * geneIDs.size() / 2;
    
    //Try to allocate enough memory to hold all pairwise distance values
    cout << "attempting to allocate " << setprecision(2) 
         << (float)maxSize * sizeof(float) / (1024*1024) << " MBs "
         << "to store pairwise distances\n";
    try {
        valArray = new float[maxSize];
        
        int idx = 0;
        if (isnan(z_mean) || isnan(z_std)) {
            cout << "\tCalculating per-gene zScore statistics..." << std::flush;
            
            for (vector<int>::iterator i = geneIDs.begin(); i != geneIDs.end(); ++i) {
                for (vector<int>::iterator j = i+1; j != geneIDs.end(); ++j) {
                    float val = fisherZtransform(pearsonCorrelation(*i, *j));
                    if (!isnan(val)) {
                        valArray[idx++] = val;
                    }
                }
            }
            
            z_mean = mean(valArray, idx);
            z_std = stddev(valArray, idx);
            cout << "done\n";
        }
        
        if (org->usesOrthology()) {
            vector<GeneFamily*> familyVec = org->getGeneFamilies();
            
            if (isnan(zmin_mean) || isnan(zmin_std)) {
                cout << "\tCalculating per-family min-linkage statistics..." << flush;
                idx = 0;
                for (unsigned int i = 0; i < familyVec.size(); i++) {
                    for (unsigned int j = i+1; j < familyVec.size(); j++) {
                        float val = fisherZtransform(pearsonCorrelation(familyVec[i], familyVec[j], linkage_type::MIN));
                        if (!isnan(val)) {
                            valArray[idx++] = val;
                        }
                    }
                }
                zmin_mean = mean(valArray, idx);
                zmin_std = stddev(valArray, idx);
                cout << "done\n";
            }
            
            if (isnan(zmax_mean) || isnan(zmax_std)) {
                cout << "\tCalculating per-family max-linkage statistics..." << flush;
                idx = 0;
                for (unsigned int i = 0; i < familyVec.size(); i++) {
                    for (unsigned int j = i+1; j < familyVec.size(); j++) {
                        float val = fisherZtransform(pearsonCorrelation(familyVec[i], familyVec[j], linkage_type::MAX));
                        if (!isnan(val)) {
                            valArray[idx++] = val;
                        }
                    }
                }
                zmax_mean = mean(valArray, idx);
                zmax_std = stddev(valArray, idx);
                cout << "done\n";
            }
            
            if (isnan(zmean_mean) || isnan(zmean_std)) {
                cout << "\tCalculating per-family mean-linkage statistics..." << flush;
                idx = 0;
                for (unsigned int i = 0; i < familyVec.size(); i++) {
                    for (unsigned int j = i+1; j < familyVec.size(); j++) {
                        float val = fisherZtransform(pearsonCorrelation(familyVec[i], familyVec[j], linkage_type::MEAN));
                        if (!isnan(val)) {
                            valArray[idx++] = val;
                        }
                    }
                }
                zmean_mean = mean(valArray, idx);
                zmean_std = stddev(valArray, idx);
                cout << "done\n";
            }
            
            if (isnan(zmedian_mean) || isnan(zmedian_std)) {
                cout << "\tCalculating per-family median-linkage statistics..." << flush;
                idx = 0;
                for (unsigned int i = 0; i < familyVec.size(); i++) {
                    for (unsigned int j = i+1; j < familyVec.size(); j++) {
                        float val = fisherZtransform(pearsonCorrelation(familyVec[i], 
                                                                        familyVec[j], 
                                                                        linkage_type::MEDIAN));
                        if (!isnan(val)) {
                            valArray[idx++] = val;
                        }
                    }
                }
                zmedian_mean = mean(valArray, idx);
                zmedian_std = stddev(valArray, idx);
                cout << "done\n";
            }
            
            if (isnan(zsigmed_mean) || isnan(zsigmed_std)) {
                cout << "\tCalculating per-family significant median-linkage statistics..." << flush;
                float val;
                idx = 0;
                for (unsigned int i = 0; i < familyVec.size(); i++) {
                    for (unsigned int j = i+1; j < familyVec.size(); j++) {
                        vector<float> corrs = allPearsonCorrelations(familyVec[i], familyVec[j]);
                        vector<float> sigs;
                        
                        for (unsigned int k = 0; k < corrs.size(); k++) {
                            if (correlationSignificance(corrs[k], distance_type::PEARSON) < ALPHA) {
                                sigs.push_back(corrs[k]);
                            }
                        }
                        
                        if (sigs.size() > 0) {
                            val = median(sigs);
                        }
                        else {
                            val = median(corrs);
                        }
                        
                        if (!isnan(val)) {
                            valArray[idx++] = val;
                        }
                    }
                }
                zsigmed_mean = mean(valArray, idx);
                zsigmed_std = stddev(valArray, idx, zsigmed_mean);
                cout << "done\n";
            }
            
        } // end if usingOrthology
        
        delete[] valArray;
    }
    catch (bad_alloc&) {
        cout << "WARNING: Unable to allocate enough memory to store all pairwise distances\n";
        cout << "         Reverting to slower method that consumes less RAM.\n";
        recalculateMemoryLimitedStats();
    }
    
    //Write out the results to a file.
    ofstream outfile (statFileName.c_str());
    if (outfile.is_open()) {
        outfile << "All\t" << z_mean << "\t" << z_std << endl;
        outfile << "Min\t" << zmin_mean << "\t" << zmin_std << endl;
        outfile << "Max\t" << zmax_mean << "\t" << zmax_std << endl;
        outfile << "Mean\t" << zmean_mean << "\t" << zmean_std << endl;
        outfile << "Median\t" << zmedian_mean << "\t" << zmedian_std << endl;
        outfile << "SigMed\t" << zsigmed_mean << "\t" << zsigmed_std << endl;
        outfile.close();
    }
    else {
        cerr << "ERROR: problem writing out stats file: " << statFileName
             << "\n   file not saved\n";
    }
}

void ExpressionDataset::recalculateMemoryLimitedStats()
{
    ///todo implement memory limited version of recalculate stats if needed
    SpellException e("recalculateMemoryLimitedStats() not yet implemented");
    throw e;
}

float ExpressionDataset::fisherZtransform (float correlation)
{
    if (isnan(correlation)) {
        return correlation;
    }
    else if (correlation <= -1) {
        correlation = -0.999999f;
    }
    else if (correlation >= 1) {
        correlation = 0.999999f;
    }
    
    return 0.5f * log((1 + correlation) / (1 - correlation));
}

float ExpressionDataset::zscoreCorrelation(const GeneFamily *gfx, const GeneFamily *gfy, 
                                           int linkType)
{
    vector<float> corrs;
    
    for (gene_family::const_iterator it_x = gfx->begin(); it_x != gfx->end(); ++it_x) {
        Gene *gx = *it_x;
        for (gene_family::const_iterator it_y = gfy->begin(); it_y != gfy->end(); ++it_y) {
            Gene *gy = *it_y;
            
            // calculate the correlation for this pair
            float corr = fisherZtransform(pearsonCorrelation(gx, gy));
            if (!isnan(corr)) {
                corrs.push_back(corr);
            }
        }
    }
    
    return linkageZscore(corrs, linkType);
}


float ExpressionDataset::zscoreCorrelation(const Gene *gx, const Gene *gy)
{
    return (fisherZtransform(pearsonCorrelation(gx, gy)) - z_mean) / z_std;
}


float ExpressionDataset::correlationSignificance(float corr, int distType)
{
    if (distType == distance_type::ZSCORE) {
        corr = tanh(corr);
    }
    
    //Extreme values cause the math to blow up...
    if (corr <= -1) {
        corr = -0.999999f;
    }
    else if (corr >= 1) {
        corr = 0.999999f;
    }
    
    int df = conds.size() - 2;
    float tval = corr * sqrt(df) / sqrt(1 - corr * corr);
    if (tval <= 0) {
        return 1.0f;
    }
    
    try {
        boost::math::students_t studentT(df);
        return (float)(1 - boost::math::cdf(studentT, tval));
    }
    catch (std::domain_error &e) {
        cerr << e.what() << endl;
        cerr << "ERROR thrown from boost::math::cdf\n"
             << "    corr = " << corr << endl
             << "      df = " << df << endl
             << "    tval = " << tval << endl;
        ///XXX this indicates a programming error, for now we just bail out
        exit(1);
    }
    
    return 0.0f;
}



float ExpressionDataset::pearsonCorrelation(int gx, int gy)
{
    
    if (data.find(gx) == data.end() ||  data.find(gy) == data.end()) {
        return numeric_limits<float>::quiet_NaN();
    }
    
    vector<float> &x = data[gx];
    vector<float> &y = data[gy];
    
    float sumx  = 0;
    float sumxx = 0;
    float sumy  = 0;
    float sumyy = 0;
    float sumxy = 0;
    int   count = 0;
    
    //calculate individual terms
    for (unsigned int i = 0; i < numConds; ++i) {
        if (!isnan(x[i]) && !isnan(y[i])) {
            sumx += x[i];
            sumxx += x[i] * x[i];
            sumy += y[i];
            sumyy += y[i] * y[i];
            sumxy += x[i] * y[i];
            ++count;
        }
    }
    if (count < 3 ) {
        return numeric_limits<float>::quiet_NaN();
    }
    
    float rho = (sumxy - (sumx * sumy) / count) 
                / sqrt(  (sumxx - (sumx * sumx) / count) 
                       * (sumyy - (sumy * sumy) / count));
    
    if (rho > 1.0f) {
        return 1.0f;
    }
    else if (rho < -1.0f) {
        return -1.0f;
    }
    return rho;
    
}




std::vector<float> ExpressionDataset::allPearsonCorrelations(const GeneFamily *fx, const GeneFamily *fy)
{
    vector<float> corrs;
    
    for (gene_family::const_iterator x_it = fx->begin(); x_it != fx->end(); ++x_it) {
        Gene *gx = *x_it;
        
        for (gene_family::const_iterator y_it = fy->begin(); y_it != fy->end(); ++y_it) {
            Gene *gy = *y_it;
            
            //calculate the correlation for this pair
            float corr = pearsonCorrelation(gx, gy);
            
            //add to the vector if it is valid
            if (!isnan(corr)) {
                corrs.push_back(corr);
            }
        }
    }
    
    return corrs;
}

std::vector<float> ExpressionDataset::allPearsonCorrelations(const Gene *gx, const GeneFamily *gfy)
{
    vector<float> corrs;
    
    //for each gene in the family
    for (gene_family::const_iterator it = gfy->begin(); it != gfy->end(); ++it) {
        Gene *gy = *it;
        
        //calculate the correlation and store it if it's valid
        float corr = pearsonCorrelation(gx, gy);
        if (!isnan(corr)) {
            corrs.push_back(corr);
        }
        
    }
    
    return corrs;
}

float ExpressionDataset::linkageValue(std::vector<float> corrs, int type, 
                                      int distType)
{
    if (corrs.size() == 0) {
        return numeric_limits<float>::quiet_NaN();
    }
    else if (corrs.size() == 1) {
        return corrs[0];
    }
    
    if (type == linkage_type::MEAN) {
        return mean(corrs);
    }
    
    if (type == linkage_type::SIGNIFICANT_MEDIAN) {
        vector<float> sigs;
        
        for (unsigned int i = 0; i < corrs.size(); i++) {
            if (correlationSignificance(corrs[i], distType) < ALPHA) {
                sigs.push_back(corrs[i]);
            }
        }
        
        if (sigs.size() == 0) {
            type = linkage_type::MEDIAN;
        }
        else if (sigs.size() == 1) {
            return sigs[0];
        }
        else {
            corrs = sigs;
            type = linkage_type::MEDIAN;
        }
    }
    
    //otherwise for MIN, MAX, MEDIAN sort the vector
    sort(corrs.begin(), corrs.end());
    
    if (type == linkage_type::MIN) {
        return corrs[0];
    }
    else if (type == linkage_type::MAX) {
        return corrs[corrs.size() - 1];
    }
    else if (type == linkage_type::MEDIAN) {
        return corrs[corrs.size() / 2];
    }
    
    // should not occur, return NaN if type was invalid
    return numeric_limits<float>::quiet_NaN();
}

float ExpressionDataset::linkageZscore(std::vector<float> corrs, int linkageType)
{
    float val = linkageValue(corrs, linkageType, distance_type::ZSCORE);

    switch (linkageType) {
        case linkage_type::MIN:
            val = (val - zmin_mean) / zmin_std;
            break;
            
        case linkage_type::MAX:
            val = (val - zmax_mean) / zmax_std;
            break;
            
        case linkage_type::MEAN:
            val = (val - zmean_mean) / zmean_std;
            break;
            
        case linkage_type::MEDIAN:
            val = (val - zmedian_mean) / zmedian_std;
            break;
            
        case linkage_type::SIGNIFICANT_MEDIAN:
            val = (val - zsigmed_mean) / zsigmed_std;
            break;
    }

    return val;
}


float ExpressionDataset::mean(float array[], int size)
{
    float sum = 0;
    int count = 0;
    for (int i = 0; i < size; i++) {
        if (!isnan(array[i])) {
            sum += array[i];
            ++count;
        }
    }
    
    if (count == 0) {
        return numeric_limits<float>::quiet_NaN();
    } 
    return sum / (float)count;
}

float ExpressionDataset::mean(std::vector<float> vec)
{
    float sum = 0;
    int count = 0;
    int size = vec.size();
    
    for (int i = 0; i < size; i++) {
        if (!isnan(vec[i])) {
            sum += vec[i];
            ++count;
        }
    }
    
    if (count == 0) {
        return numeric_limits<float>::quiet_NaN();
    } 
    return sum / (float)count;
}

float ExpressionDataset::median(std::vector<float> vec)
{
    if (vec.size() == 0) {
        return numeric_limits<float>::quiet_NaN();
    }
    else if (vec.size() == 1) {
        return vec[0];
    }
    
    sort(vec.begin(), vec.end());
    return vec[vec.size() / 2];
}

float ExpressionDataset::stddev(float array[], int size, float mean) {
    float sum = 0;
    int count = 0;
    
    for (int i = 0; i < size; i++) {
        float val = array[i];
        if (!isnan(val)) {
            val = val - mean;
            val = val * val;
            sum += val;
            ++count;
        }
    }
    
    if (count == 0) {
        return numeric_limits<float>::quiet_NaN();
    }
    return sqrt(sum / (float)count);
}

float ExpressionDataset::stddev (float array[], int size) {
    return stddev(array, size, mean(array, size));
}

float ExpressionDataset::stddev(std::vector<float> vec, float mean)
{
    float sum = 0;
    int count = 0;
    int size = vec.size();
    
    for (int i = 0; i < size; i++) {
        float val = vec[i];
        if (!isnan(vec[i])) {
            val = val - mean;
            val = val * val;
            sum += val;
            ++count;
        }
    }
    
    if (count == 0) {
        return numeric_limits<float>::quiet_NaN();
    } 
    return sum / (float)count;
}

float ExpressionDataset::stddev (std::vector<float> vec) {
    return stddev(vec, mean(vec));
}

