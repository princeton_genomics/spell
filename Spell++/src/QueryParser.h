/*
 * QueryParser.h
 * Spell++
 *
 * Copyright (c) 2010 The Jackson Laboratory
 * 
 * This software was developed by the Hibbs Lab at The Jackson
 * Laboratory (see http://cbfg.jax.org).
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef QUERY_PARSER_H
#define QUERY_PARSER_H

#include <string>
#include <vector>

/**
 * validate and parse 
 *
 *
 */
class QueryParser {
    
private:
    /// dataset IDs specified in query
    std::vector<int> datasetIDs;
    
    /// query Gene IDs
    std::vector<int> geneIDs;
    
    // optional parameters
    
    /// maximum number of genes to return, 0 = all
    int maxGenes;
    
public:
    /**
     * default constructor
     *
     */
    QueryParser(){};
    
    /**
     * construct a QueryParser and parse a message. This is functionally
     * equivalent to QueryParser qp; qp.parse(message); 
     *
     * @param message the message recieved from the Spell front end
     *
     */
    QueryParser(std::string message)
    {
        parse(message);
    }
    
    /**
     * parse a new message. This method will clear out datasetIDs and geneIDs
     * so a single QueryParser object could be safely reused
     *
     * @param message the message recieved from the Spell front end
     */
    void parse(std::string message);
    
    /**
     * get the Gene IDs from a parsed message
     *
     * @returns a vector of int Gene IDs
     *
     */
    std::vector<int> getGeneIDs()
    {
        return geneIDs;
    }
    
    /**
     * get the dataset IDs from a parsed message
     *
     * @returns a vector of int ExpressionDataset IDs
     *
     */
    std::vector<int> getDatasetIDs()
    {
        return datasetIDs;
    }
    
    /**
     * get the MAX_GENES parameter value
     *
     * @returns the value for the MAX_GENES parmeter passed from the client or 
     *          -1 if the parameter was not set
     *
     */
    int getMaxGenes()
    {
        return maxGenes;
    }
    
    /**
     * get the number of query genes
     *
     * @returns the number of query genes
     */
    int getQuerySize()
    {
        return geneIDs.size();
    }
    
};

#endif
