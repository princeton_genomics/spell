#! /usr/bin/python

# add_IDs.py
# Created 8-Feb-2011
# Author: Al Simons, Hibbs Lab, The Jackson Laboratory al.simons@jax.org

# Large data tables load more quickly into MySQL using "load data infile"
# Than they do programmatically from the web interface.  However, if the 
# web interface wants "data<tab>data", load data infile wants
# ID<tab>data<tab>data

# This program adds the ID to the rows, and writes the updated rows to stdout.
# The input data is taken from the file specified as the first (and
# only) argument on the command line.

import sys

def usage():
    print >> sys.stderr, 'usage:', sys.argv[0], 'input-file'
    sys.exit()

if len(sys.argv) != 2:
    usage()

counter = 0
for line in open(sys.argv[1]):
    counter += 1
    sys.stdout.write(str(counter) + '\t')
    sys.stdout.write(line)
